﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    public partial class CustomMessageBox : Form
    {
        JointEditDoc m_JointEditDoc;

        public CustomMessageBox(JointEditDoc GetDocument)
        {
            InitializeComponent();
            m_JointEditDoc = GetDocument;
            radioButton2.Checked = true;
        }

        private void CustomMessageBox_Load(object sender, EventArgs e)
        {
            label1.Text = Yugamiru.Properties.Resources.CUSTOM_MB1;
            label2.Text = Yugamiru.Properties.Resources.CUSTOM_MB2;
            radioButton1.Text = Yugamiru.Properties.Resources.CUSTOM_MB3;
            radioButton2.Text = Yugamiru.Properties.Resources.CUSTOM_MB4;
            button_Ok.Text = Yugamiru.Properties.Resources.CUSTOM_OK;
            button_Cancel.Text = Yugamiru.Properties.Resources.OPENRESULT_CANCEL;
        }

        private void button_Ok_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true) // Add as new record
            {
                m_JointEditDoc.SetUpdateFlag(Constants.ADD_NEW_RECORD);
            }
            if(radioButton2.Checked == true) // Update existing record
            {
                m_JointEditDoc.SetUpdateFlag(Constants.UPDATE_EXISTING_RECORD);
            }
            this.Close();           
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            m_JointEditDoc.SetUpdateFlag(Constants.DO_NONE);
            this.Close();
        }
    }
}
