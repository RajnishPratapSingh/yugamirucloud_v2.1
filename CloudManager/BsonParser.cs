﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Data.SQLite;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace CloudManager
{
    public static class BsonParser
    {
        //var outputs = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty), new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(input)));


        //outputs =  Regex.Replace(outputs, "[^a-zA-Z0-9]+", " ");

        public static DataTable PatientDetailsBSONtoDT(string bsonstring)
        {
        
            //Read one row from PatientDetails table with additional JSON columns
            DataTable tablesPatientDetails = ColumnNamesPatientDetails(bsonstring);
            return tablesPatientDetails;
        }
        public static DataTable FrontBodyPositionKneeDownBSONtoDT(string bsonstring)
        {

            //Read one row from FrontBodyPositionKneedown table with additional JSON columns
            DataTable tablesPatientDetails = ColumnNamesFrontBodyPositionKneedown(bsonstring);
            return tablesPatientDetails;
        }
        public static DataTable FrontBodyPositionStandingBSONtoDT(string bsonstring)
        {

            //Read one row from FrontBodyPositionStanding table with additional JSON columns
            //DataTable tablesPatientDetails = ColumnNamesPatientDetails(bsonstring);
            DataTable tablesPatientDetails = ColumnNamesFrontBodyPositionStanding(bsonstring);
            return tablesPatientDetails;
        }
        public static DataTable SideBodyPositionBSONtoDT(string bsonstring)
        {

            //Read one row from sideBodyPosition table with additional JSON columns
            //DataTable tablesPatientDetails = ColumnNamesPatientDetails(bsonstring);
            DataTable tablesPatientDetails = ColumnNamesSideBodyPosition(bsonstring);
            return tablesPatientDetails;
        }

        public static DataTable ColumnNamesPatientDetails(string bsonData)
        {
            List<string> lstCols = new List<string>();
            string sqliteDBPath = GlobalItems.sqliteDBPath;//@"E:\GSPORT\SQLDBTest\cpdb.sqlite";
            SQLiteConnection AppDBCon = new SQLiteConnection("Data Source=" + sqliteDBPath);
            try
            {
                SQLiteCommand cmd;
                AppDBCon.Open();
                cmd = AppDBCon.CreateCommand();
                //cmd.CommandText = "SELECT * FROM SideBodyPosition ORDER BY ROWID ASC LIMIT 1;";//patientdetails
                //cmd.CommandText = "SELECT * FROM patientdetails ORDER BY ROWID ASC LIMIT 1;";//
                //cmd.CommandText = "SELECT * FROM FrontBodyPositionStanding ORDER BY ROWID ASC LIMIT 1;";//FrontBodyPositionKneedown
                cmd.CommandText = "SELECT * FROM patientdetails ORDER BY ROWID ASC LIMIT 1;";//
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                DataTable dt = new DataTable();
                //dt.TableName = "SideBodyPosition";
                //dt.TableName = "FrontBodyPositionStanding";
                ad.Fill(dt); //fill the datasource
                //close connection
                AppDBCon.Close();


                //now iterate and add column names in list
                //lstCols.Add("Status");
                //lstCols.Add("TableName");
                //lstCols.Add("ComputerId");
                //lstCols.Add("ActivationKey");

                //foreach (DataColumn cl in dt.Columns)
                //{
                //    lstCols.Add(cl.ColumnName);
                //}

                DataTable tblPatientDetails= SpliterPatientDetails( bsonData);
                //DataTable SideBodyPosition = SpliterSideBodyPosition(lstCols);
                //DataTable tblFrontBodyPositionStanding = SpliterFrontBodyPositionStanding(lstCols);
                //DataTable tblSpliterFrontBodyPositionKneedown = SpliterFrontBodyPositionKneedown(lstCols);
                return tblPatientDetails;

            }
            catch (SQLiteException dbex)
            {

            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public static DataTable ColumnNamesSideBodyPosition(string bsonData)
        {
            List<string> lstCols = new List<string>();
            //string sqliteDBPath = @"E:\GSPORT\SQLDBTest\cpdb.sqlite";
            SQLiteConnection AppDBCon = new SQLiteConnection("Data Source=" +GlobalItems.sqliteDBPath);
            try
            {
                SQLiteCommand cmd;
                AppDBCon.Open();
                cmd = AppDBCon.CreateCommand();
                cmd.CommandText = "SELECT * FROM SideBodyPosition ORDER BY ROWID ASC LIMIT 1;";//patientdetails
                
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                DataTable dt = new DataTable();
                //dt.TableName = "SideBodyPosition";
                //dt.TableName = "SideBodyPosition";
                ad.Fill(dt); //fill the datasource
                //close connection
                AppDBCon.Close();


                //now iterate and add column names in list
                lstCols.Add("Status");
                lstCols.Add("TableName");
                lstCols.Add("ComputerId");
                lstCols.Add("ActivationKey");

                foreach (DataColumn cl in dt.Columns)
                {
                    lstCols.Add(cl.ColumnName);
                }

                //DataTable tblPatientDetails = SpliterPatientDetails(lstCols);
                DataTable SideBodyPosition = SpliterSideBodyPosition(lstCols,bsonData);
                //DataTable tblFrontBodyPositionStanding = SpliterFrontBodyPositionStanding(lstCols);
                //DataTable tblSpliterFrontBodyPositionKneedown = SpliterFrontBodyPositionKneedown(lstCols);
                return SideBodyPosition;

            }
            catch (SQLiteException dbex)
            {

            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public static DataTable ColumnNamesFrontBodyPositionStanding(string bsonData)
        {
            List<string> lstCols = new List<string>();
            //string sqliteDBPath = @"E:\GSPORT\SQLDBTest\cpdb.sqlite";
            SQLiteConnection AppDBCon = new SQLiteConnection("Data Source=" + GlobalItems.sqliteDBPath);
            try
            {
                SQLiteCommand cmd;
                AppDBCon.Open();
                cmd = AppDBCon.CreateCommand();
                //cmd.CommandText = "SELECT * FROM SideBodyPosition ORDER BY ROWID ASC LIMIT 1;";//patientdetails
                //cmd.CommandText = "SELECT * FROM patientdetails ORDER BY ROWID ASC LIMIT 1;";//
                cmd.CommandText = "SELECT * FROM FrontBodyPositionStanding ORDER BY ROWID ASC LIMIT 1;";//FrontBodyPositionKneedown
                //cmd.CommandText = "SELECT * FROM patientdetails ORDER BY ROWID ASC LIMIT 1;";//
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                DataTable dt = new DataTable();
                //dt.TableName = "SideBodyPosition";
                //dt.TableName = "FrontBodyPositionStanding";
                ad.Fill(dt); //fill the datasource
                //close connection
                AppDBCon.Close();


                //now iterate and add column names in list
                lstCols.Add("Status");
                lstCols.Add("TableName");
                lstCols.Add("ComputerId");
                lstCols.Add("ActivationKey");

                foreach (DataColumn cl in dt.Columns)
                {
                    lstCols.Add(cl.ColumnName);
                }

                //DataTable tblPatientDetails = SpliterPatientDetails(lstCols);
                //DataTable SideBodyPosition = SpliterSideBodyPosition(lstCols);
                DataTable tblFrontBodyPositionStanding = SpliterFrontBodyPositionStanding(lstCols,bsonData);
                //DataTable tblSpliterFrontBodyPositionKneedown = SpliterFrontBodyPositionKneedown(lstCols);
                return tblFrontBodyPositionStanding;

            }
            catch (SQLiteException dbex)
            {

            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public static DataTable ColumnNamesFrontBodyPositionKneedown(string bsonData)
        {
            List<string> lstCols = new List<string>();
            //string sqliteDBPath = @"E:\GSPORT\SQLDBTest\cpdb.sqlite";
            SQLiteConnection AppDBCon = new SQLiteConnection("Data Source=" + GlobalItems.sqliteDBPath);
            try
            {
                SQLiteCommand cmd;
                AppDBCon.Open();
                cmd = AppDBCon.CreateCommand();
                //cmd.CommandText = "SELECT * FROM SideBodyPosition ORDER BY ROWID ASC LIMIT 1;";//patientdetails
                //cmd.CommandText = "SELECT * FROM patientdetails ORDER BY ROWID ASC LIMIT 1;";//
                cmd.CommandText = "SELECT * FROM FrontBodyPositionKneedown ORDER BY ROWID ASC LIMIT 1;";//FrontBodyPositionKneedown
                //cmd.CommandText = "SELECT * FROM patientdetails ORDER BY ROWID ASC LIMIT 1;";//
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                DataTable dt = new DataTable();
                //dt.TableName = "SideBodyPosition";
                //dt.TableName = "FrontBodyPositionKneedown";
                ad.Fill(dt); //fill the datasource
                //close connection
                AppDBCon.Close();


                //now iterate and add column names in list
                lstCols.Add("Status");
                lstCols.Add("TableName");
                lstCols.Add("ComputerId");
                lstCols.Add("ActivationKey");

                foreach (DataColumn cl in dt.Columns)
                {
                    lstCols.Add(cl.ColumnName);
                }

               // DataTable tblPatientDetails = SpliterPatientDetails(lstCols);
                //DataTable SideBodyPosition = SpliterSideBodyPosition(lstCols);
                //DataTable dtFrontBodyPositionKneedown = SpliterFrontBodyPositionStanding(lstCols);
                DataTable tblSpliterFrontBodyPositionKneedown = SpliterFrontBodyPositionKneedown(lstCols,bsonData);
                return tblSpliterFrontBodyPositionKneedown;

            }
            catch (SQLiteException dbex)
            {

            }
            catch (Exception ex)
            {

            }
            return null;
        }
       

        public static DataTable SpliterSideBodyPosition(List<string> bsoncols, string bsonData)
        {
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\sidebodyposition.bson";
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\FrontBodyPositionStanding.bson";
            string content = bsonData;//File.ReadAllText(filepath);

            //remove additional contents
            string bson1 = content.Replace("\a�\0\0\u00030\0��\0\0\u0002", "");//\a\0\0\u00030\0\0\0\u0002
            bson1 = bson1.Replace("\a�\0\0\u00030\0��\0\0\u0002", "");
            bson1 = bson1.Replace("\0\u0004\0\0\0", "");
            bson1 = bson1.Replace("\0\0\u0002", "");
            bson1 = bson1.Replace("\0��", "");
            bson1 = bson1.Replace("\0\u0011\0\0\0", "");
            bson1 = bson1.Replace("\0\u0004\0\0\0", "");
            bson1 = bson1.Replace("\0\u0002", "");
            bson1 = bson1.Replace("\0\u001d\0\0\0 ", "");
            bson1 = bson1.Replace("\0\u001b\0\0\0", "");
            bson1 = bson1.Replace("\0\v�\0\0", "").Replace("\0�", "").Replace("�", "").Replace("\0#", "").Replace("\0?", "");
            bson1 = bson1.Replace("\0\v�", "");

           



            string strColVal = bson1.Replace("\0\u001d", "");
            List<string> ar = new List<string>();
       
            string[] checksp = strColVal.Split(bsoncols.ToArray(), StringSplitOptions.None);

            string[] checktp = strColVal.Split(checksp, StringSplitOptions.None);

            DataTable dtBSONValues = new DataTable();
            foreach(string cl in checktp)
            {
                if(cl.Trim().Length==0)
                {

                }
                else
                {
                    dtBSONValues.Columns.Add(cl);
                }
            }
            
            List<string> lstVals = new List<string>();
            foreach(string cel in checksp )
            {
                if(cel.Trim().Length==0)
                {

                }
                else
                {
                    lstVals.Add(cel);
                }
            }
            dtBSONValues.Rows.Add(lstVals.ToArray());
            return dtBSONValues;

        }
        public static DataTable SpliterFrontBodyPositionStanding(List<string> bsoncols, string bsonData)
        {
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\sidebodyposition.bson";
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\FrontBodyPositionStanding.bson";
            string content = bsonData;//File.ReadAllText(filepath);

            //remove additional contents
            string bson1 = content.Replace("\a�\0\0\u00030\0��\0\0\u0002", "");//\a\0\0\u00030\0\0\0\u0002
            bson1 = bson1.Replace("\a�\0\0\u00030\0��\0\0\u0002", "");
            bson1 = bson1.Replace("\0\u0004\0\0\0", "");
            bson1 = bson1.Replace("\0\0\u0002", "");
            bson1 = bson1.Replace("\0��", "");
            bson1 = bson1.Replace("\0\u0011\0\0\0", "");
            bson1 = bson1.Replace("\0\u0004\0\0\0", "");
            bson1 = bson1.Replace("\0\u0002", "");
            bson1 = bson1.Replace("\0\u001d\0\0\0 ", "");
            bson1 = bson1.Replace("\0\u001b\0\0\0", "");
            bson1 = bson1.Replace("\0\v�\0\0", "").Replace("\0�", "").Replace("�", "").Replace("\0#", "").Replace("\0?", "");
            bson1 = bson1.Replace("\0\v�", "");
            //================frontBodyPositionStanding=========
            bson1 = bson1.Replace("Y\0\0\u00030\0Q", "");
            bson1 = bson1.Replace("\0\u001a\0\0\0", "");
            bson1 = bson1.Replace("\0\u0005\0\0\0", "");
            bson1 = bson1.Replace("\0\u0005\0\0\0", "");
            //bson1 = bson1.Replace("", "");
            //bson1 = bson1.Replace("", "");
            //bson1 = bson1.Replace("", "");
            //==================================================


            string strColVal = bson1.Replace("\0\u001d", "");
            List<string> ar = new List<string>();

            string[] checksp = strColVal.Split(bsoncols.ToArray(), StringSplitOptions.None);

            string[] checktp = strColVal.Split(checksp, StringSplitOptions.None);

            DataTable dtBSONValues = new DataTable();
            foreach (string cl in checktp)
            {
                if (cl.Trim().Length == 0)
                {

                }
                else
                {
                    dtBSONValues.Columns.Add(cl);
                }
            }

            List<string> lstVals = new List<string>();
            foreach (string cel in checksp)
            {
                if (cel.Trim().Length == 0)
                {

                }
                else
                {
                    lstVals.Add(cel);
                }
            }
            dtBSONValues.Rows.Add(lstVals.ToArray());
            return dtBSONValues;

        }
        public static DataTable SpliterFrontBodyPositionStanding( string bsonData)
        {
            //=====================STANDING
            string content = bsonData;
            string[] uniqueId = { "UniqueId" };
            string[] discardBeforeUnique = content.Split(uniqueId, StringSplitOptions.None);

            string withUniqTillEnd = uniqueId[0] + " " + discardBeforeUnique[2];//[1];// Image is there


            string[] ImageBytes = { "ImageBytes" };

            string[] startToTillBeforeImage = withUniqTillEnd.Split(ImageBytes, StringSplitOptions.None);

            string firstHalf = startToTillBeforeImage[0];
            string secondHalf = "ImageBytes " + startToTillBeforeImage[1];

            //Now get AfterImageData
            string[] kpd = { "KneePositionDetected" };
            string[] withImageData = secondHalf.Split(kpd, StringSplitOptions.None);
            string afterImageTillEnd = "KneePositionDetected" + withImageData[1];

            string f1data = firstHalf;
            string imageData9 = "ImageBytes " + (content.Split(ImageBytes, StringSplitOptions.None)[1]).Split(kpd, StringSplitOptions.None)[0];
            string s2data = afterImageTillEnd;


            imageData9 = imageData9.Replace("\0\0\0\u0001\0", "");
            imageData9 = imageData9.Replace("\0\u0002", "");
            //sumit v2.0
            string[] starttag = { "thiscannotbeacoincedenceatstart" };
            string[] endtag = { "atendthisisalsonotacoincedence" };
            string imagedatahalfclean = imageData9.Split(starttag, StringSplitOptions.None)[1];
            string imagedataallcleaned = imagedatahalfclean.Split(endtag, StringSplitOptions.None)[0];
            imageData9 = imagedataallcleaned;
            //v2.0 end
            //Clean non image Data
            var outputsFirst = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
                  Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                      new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(f1data)));
            outputsFirst = Regex.Replace(outputsFirst, "[^a-zA-Z0-9]+", " ");

            var outputSecond = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
              Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                  new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(s2data)));
            outputSecond = Regex.Replace(outputSecond, "[^a-zA-Z0-9]+", " ");


            List<string> gather = new List<string>();
            foreach (string k1 in outputsFirst.Split(' '))
            {
                if (k1.Length > 0)
                    gather.Add(k1);
            }

            gather.Add("ImageBytes");//.Split(' ')[0]);
            gather.Add(imageData9);//.Split(' ')[1]);

            foreach (string k2 in outputSecond.Split(' '))
            {
                if (k2.Length > 0)
                    gather.Add(k2);
            }
            List<string> columnList = new List<string>();
            List<string> valuesList = new List<string>();

            for (int i = 0; i < gather.Count - 1; i += 2)
            {
                if (gather[i] == "Status" || gather[i] == "TableName" || gather[i] == "Computerid" || gather[i] == "Activationkey")
                {
                    continue;
                }
                columnList.Add(gather[i]);
                valuesList.Add(gather[i + 1]);
            }
            DataTable dtServerKneeDown = new DataTable();
            foreach (string cl in columnList)
            {
                dtServerKneeDown.Columns.Add(cl);
            }
            dtServerKneeDown.Rows.Add(valuesList.ToArray());
            string imagebytesone = dtServerKneeDown.Rows[0]["ImageBytes"].ToString();
            //non image data cleaned


            //============TESTING for imagedata
            //File.WriteAllText("ImgBytFrmProg", imagebytesone,Encoding.UTF8);

            //string t1 = File.ReadAllText("ImgBytFrmProg", Encoding.Convert(Encoding.UTF8,Encoding.ASCII);
            //===================================


            return dtServerKneeDown;

        }
        public static DataTable SpliterFrontBodyPositionKneedown(List<string> bsoncols, string bsonData)
        {
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\sidebodyposition.bson";
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\FrontBodyPositionKneedown.bson";
            string content = bsonData;//File.ReadAllText(filepath);
            string[] uniqueId = { "UniqueId" };
            string[] discardBeforeUnique = content.Split(uniqueId, StringSplitOptions.None);

            string withUniqTillEnd = uniqueId[0] + " " + discardBeforeUnique[1];// Image is there


            string[] ImageBytes = { "ImageBytes" };

            string[] startToTillBeforeImage = withUniqTillEnd.Split(ImageBytes, StringSplitOptions.None);

            string firstHalf = startToTillBeforeImage[0];
            string secondHalf = "ImageBytes " + startToTillBeforeImage[1];

            //Now get AfterImageData
            string[] kpd = { "KneePositionDetected" };
            string[] withImageData = secondHalf.Split(kpd, StringSplitOptions.None);
            string afterImageTillEnd = "KneePositionDetected" + withImageData[1];

            string f1data = firstHalf;
            string imageData9 ="ImageBytes "+ (content.Split(ImageBytes, StringSplitOptions.None)[1]).Split(kpd, StringSplitOptions.None)[0];
            string s2data = afterImageTillEnd;


            //Clean non image Data
            var outputsFirst = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8, 
                  Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty), 
                      new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(f1data)));          
                           outputsFirst =  Regex.Replace(outputsFirst, "[^a-zA-Z0-9]+", " ");

            var outputSecond = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
              Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                  new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(s2data)));
            outputSecond = Regex.Replace(outputSecond, "[^a-zA-Z0-9]+", " ");


            List<string> gather = new List<string>();
            foreach(string k1 in outputsFirst.Split(' '))
            {
                if (k1.Length > 0)
                    gather.Add(k1);
            }

            gather.Add(imageData9.Split(' ')[0]);
            gather.Add(imageData9.Split(' ')[1]);

            foreach (string k2 in outputSecond.Split(' '))
            {
                if (k2.Length > 0)
                    gather.Add(k2);
            }
            List<string> columnList = new List<string>();
            List<string> valuesList = new List<string>();

            for(int i=0;i<gather.Count-1;i+=2)
            {
                columnList.Add(gather[i]);
                valuesList.Add(gather[i + 1]);
            }
            DataTable dtServerKneeDown = new DataTable();
            foreach(string cl in columnList)
            {
                dtServerKneeDown.Columns.Add(cl);
            }
            dtServerKneeDown.Rows.Add(valuesList.ToArray());
            string imagebytesone = dtServerKneeDown.Rows[0]["ImageBytes"].ToString();
            //non image data cleaned

            return dtServerKneeDown;

        }

        public static DataTable SpliterFrontBodyPositionKneedown(string bsonData)
        {
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\sidebodyposition.bson";
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\FrontBodyPositionKneedown.bson";
            string content = bsonData;//File.ReadAllText(filepath);
            string[] uniqueId = { "UniqueId" };
            string[] discardBeforeUnique = content.Split(uniqueId, StringSplitOptions.None);

            string withUniqTillEnd = uniqueId[0] + " " + discardBeforeUnique[1];// Image is there


            string[] ImageBytes = { "ImageBytes" };

            string[] startToTillBeforeImage = withUniqTillEnd.Split(ImageBytes, StringSplitOptions.None);

            string firstHalf = startToTillBeforeImage[0];
            string secondHalf = "ImageBytes " + startToTillBeforeImage[1];

            //Now get AfterImageData
            string[] kpd = { "KneePositionDetected" };
            string[] withImageData = secondHalf.Split(kpd, StringSplitOptions.None);
            string afterImageTillEnd = "KneePositionDetected" + withImageData[1];

            string f1data = firstHalf;
            string imageData9 = "ImageBytes " + (content.Split(ImageBytes, StringSplitOptions.None)[1]).Split(kpd, StringSplitOptions.None)[0];
            string s2data = afterImageTillEnd;


            imageData9 = imageData9.Replace("\0\0\0\u0001\0", "");
            imageData9 = imageData9.Replace("\0\u0002", "");
            //Sumit v2.0 Image data cleaning 
            string[] starttag = { "thiscannotbeacoincedenceatstart" };
            string[] endtag = { "atendthisisalsonotacoincedence" };
            string imagedatahalfclean = imageData9.Split(starttag,StringSplitOptions.None)[1];
            string imagedataallcleaned = imagedatahalfclean.Split(endtag,StringSplitOptions.None)[0];
            imageData9 = imagedataallcleaned;
            //

            //Clean non image Data
            var outputsFirst = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
                  Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                      new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(f1data)));
            outputsFirst = Regex.Replace(outputsFirst, "[^a-zA-Z0-9]+", " ");

            var outputSecond = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
              Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                  new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(s2data)));
            outputSecond = Regex.Replace(outputSecond, "[^a-zA-Z0-9]+", " ");


            List<string> gather = new List<string>();
            foreach (string k1 in outputsFirst.Split(' '))
            {
                if (k1.Length > 0)
                    gather.Add(k1);
            }

            gather.Add("ImageBytes");//imageData9.Split(' ')[0]);
            gather.Add(imageData9);//.Split(' ')[1]);

            foreach (string k2 in outputSecond.Split(' '))
            {
                if (k2.Length > 0)
                    gather.Add(k2);
            }
            List<string> columnList = new List<string>();
            List<string> valuesList = new List<string>();

            for (int i = 0; i < gather.Count - 1; i += 2)
            {
                if (gather[i] == "Status" || gather[i] == "TableName" || gather[i] == "Computerid" || gather[i] == "Activationkey")
                {
                    continue;
                }
                columnList.Add(gather[i]);
                valuesList.Add(gather[i + 1]);
            }
            DataTable dtServerKneeDown = new DataTable();
            foreach (string cl in columnList)
            {
                dtServerKneeDown.Columns.Add(cl);
            }
            dtServerKneeDown.Rows.Add(valuesList.ToArray());
            string imagebytesone = dtServerKneeDown.Rows[0]["ImageBytes"].ToString();
            //non image data cleaned

            return dtServerKneeDown;

        }
        //--Added by Rajnish for GSP-676--//
        public static Boolean IsBase64String(this String value)
        {
            if (value == null || value.Length == 0 || value.Length % 4 != 0
                || value.Contains(' ') || value.Contains('\t') || value.Contains('\r') || value.Contains('\n'))
                return false;
            var index = value.Length - 1;
            if (value[index] == '=')
                index--;
            if (value[index] == '=')
                index--;
            for (var i = 0; i <= index; i++)
                if (IsInvalid(value[i]))
                    return false;
            return true;
        }
        private static Boolean IsInvalid(char value)
        {
            var intValue = (Int32)value;
            if (intValue >= 48 && intValue <= 57)
                return false;
            if (intValue >= 65 && intValue <= 90)
                return false;
            if (intValue >= 97 && intValue <= 122)
                return false;
            return intValue != 43 && intValue != 47;
        }
        //-------------------------------//
        public static DataTable SpliterPatientDetails(string bsondata)
        {

            #region Need to replace with fail safe logic
            /*
            if(bsondata.Length==0)
            {
                throw new EvaluateException("bsondata cannot be empty");
            }

            //======================================
            var outputs = Encoding.ASCII.GetString(Encoding.Convert
                (Encoding.UTF8, Encoding.GetEncoding(Encoding.ASCII.EncodingName, 
            new EncoderReplacementFallback(String.Empty), new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(bsondata)));


            outputs = Regex.Replace(outputs, "[^a-zA-Z0-9]+", " ");
            //======================================

            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\sidebodyposition.bson";
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\PatientDetails.bson";
            string content = bsondata;//File.ReadAllText(filepath);

            //remove additional contents
            string bson1 = content.Replace("r\u0001\0\0\u0002", "").Replace("\a�\0\0\u00030\0��\0\0\u0002", "");//\a\0\0\u00030\0\0\0\u0002
            bson1 = bson1.Replace("\a�\0\0\u00030\0��\0\0\u0002", "");
            bson1 = bson1.Replace("\0\u0004\0\0\0", "");
            bson1 = bson1.Replace("\0\0\u0002", "");
            bson1 = bson1.Replace("\0��", "");
            bson1 = bson1.Replace("\0\u0011\0\0\0", "");
            bson1 = bson1.Replace("\0\u0004\0\0\0", "");
            bson1 = bson1.Replace("\0\u0002", "");
            bson1 = bson1.Replace("\0\u001d\0\0\0 ", "");
            bson1 = bson1.Replace("\0\u001b\0\0\0", "");
            bson1 = bson1.Replace("\0\v�\0\0", "").Replace("\0�", "").Replace("�", "").Replace("\0#", "").Replace("\0?", "");
            bson1 = bson1.Replace("\0\v�", "");
            //================frontBodyPositionStanding=========
            bson1 = bson1.Replace("Y\0\0\u00030\0Q", "");
            bson1 = bson1.Replace("\0\u001a\0\0\0", "");
            bson1 = bson1.Replace("\0\u0005\0\0\0", "");
            bson1 = bson1.Replace("\0\u0005\0\0\0", "");
            //=================PatientDetails==============
            bson1 = bson1.Replace("\u0001\0\0\u00030\u0001", "");
            bson1 = bson1.Replace("\0\u000f\0\0\0", "");
            bson1 = bson1.Replace("\0\u0003\0\0\0", "");
            bson1 = bson1.Replace("\0\u0001\0\0", "");
            bson1 = bson1.Replace("\0\u0016\0\0\0", "");            
            bson1 = bson1.Replace("\0\a\0\0\0", "");            
            bson1 = bson1.Replace("\0\u0003\0\0\0", "");
            bson1 = bson1.Replace("\0\0\0", "");
            //bson1 = bson1.Replace("", "");
            //bson1 = bson1.Replace("", "");
            //bson1 = bson1.Replace("", "");
            //==================================================


            string strColVal = bson1.Replace("\0\u001d", "");
            strColVal = strColVal.Replace("\u0001", "");
            strColVal = strColVal.Replace("\0\u0013", "");
            strColVal = strColVal.Replace("\0\n", "");
            //strColVal = strColVal.Replace("\0\r", "");
            strColVal = strColVal.Replace("\0\0", "");
            strColVal = strColVal.Replace("Comment", "Comment$");
            strColVal = strColVal.Replace(" ", "$");
            List<string> ar = new List<string>();
            strColVal = strColVal.Replace("Computerid", "ComputerId");
            strColVal = strColVal.Replace("Activationkey", "ActivationKey");

            //================================================================================================
            //strColVal = strColVal.Replace("-", "#");
            //var outputs = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding(Encoding.ASCII.EncodingName, 
            //    new EncoderReplacementFallback(String.Empty), new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(strColVal)));


            //outputs = Regex.Replace(outputs, "[^a-zA-Z0-9]+", " ");
            //strColVal = outputs;

            strColVal = strColVal.Replace("#", "-");
            //================================================================================================
            string[] checkspValues = strColVal.Split(bsoncols.ToArray(), StringSplitOptions.None);

            string[] checktpColNames = strColVal.Split(checkspValues, StringSplitOptions.None);

            List<string> correctColumns = new List<string>();
            foreach (string columnsMayBWrong in checktpColNames)
            {
                if (columnsMayBWrong.Contains("Month"))
                {
                    correctColumns.Add("Month");
                    continue;
                }
                //if (columnsMayBWrong.Contains("-") || columnsMayBWrong == "0" || columnsMayBWrong == "" || columnsMayBWrong.Length == 0
                //    || columnsMayBWrong == "1" || columnsMayBWrong == "2" || columnsMayBWrong == "3" || columnsMayBWrong == "4"
                //    || columnsMayBWrong == "5" || columnsMayBWrong == "6" || columnsMayBWrong == "7" || columnsMayBWrong == "8"
                //    || columnsMayBWrong == "9")
                if (columnsMayBWrong.Contains("-") || columnsMayBWrong.Contains("0") || columnsMayBWrong.Contains("") || columnsMayBWrong.Length == 0
                    || columnsMayBWrong.Contains("1") || columnsMayBWrong.Contains("2") || columnsMayBWrong.Contains("3") || columnsMayBWrong.Contains("4")
                    || columnsMayBWrong.Contains("5") || columnsMayBWrong.Contains("6") || columnsMayBWrong.Contains("7") || columnsMayBWrong.Contains("8")
                    || columnsMayBWrong.Contains("9"))

                {
                    continue;
                }
                if (columnsMayBWrong.Contains("00"))
                {
                    correctColumns.Add(columnsMayBWrong.Replace("00", "").Replace("$", " "));
                    continue;
                }
                else
                {
                }
                correctColumns.Add(columnsMayBWrong.Replace("$", " "));
            }
            List<string> correctValues = new List<string>();
            int cntColVal = 1;
            foreach (string valuesMayBWrong in checkspValues)
            {
                if(cntColVal==1)
                {
                    cntColVal++;
                    continue;
                }
                correctValues.Add(valuesMayBWrong.Replace("$", " "));
                
            }



            DataTable dtBSONValues = new DataTable();
            int k = 0;
            foreach (string cl in correctColumns)
            {
                if (cl.Trim().Length == 0 )
                {
                    continue;
                }
                else if(k<=3)
                {
                    //continue;
                }
                else
                {
                    dtBSONValues.Columns.Add(cl);
                }
                k++;
            }

            List<string> lstVals = new List<string>();

            

            //List<string> lstVals = new List<string>();
            //foreach (string cel in correctValues)
            //{
            //    //if (cel.Trim().Length == 0)
            //    //{

            //    //}
            //    //else
            //    //{
            //    //    lstVals.Add(cel);
            //    //}
            //    lstVals.Add(cel.Replace(" ", "-"));
            //}
            lstVals = correctValues;
            lstVals.RemoveAt(0);
            lstVals.RemoveAt(0);
            lstVals.RemoveAt(0);
            lstVals.RemoveAt(0);
            dtBSONValues.Rows.Add(lstVals.ToArray());

            #region commented
            //dtBSONValues.Rows[0]["MeasurementTime"] = dtBSONValues.Rows[0]["MeasurementTime"].ToString().Replace(" ", "-");
            //dtBSONValues.Rows[0]["DOB"] = dtBSONValues.Rows[0]["DOB"].ToString().Replace(" ", "-");
            //edit table values for 'time' data

            //foreach(DataColumn dc in dtBSONValues.Columns)
            //{
            //    if(dc.ColumnName.Trim() == "MeasurementTime" || dc.ColumnName.Trim()=="DOB")
            //    {
            //        continue;
            //    }
            //    else
            //    {
            //        var outputs = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8, 
            //        Encoding.GetEncoding(Encoding.ASCII.EncodingName,
            //        new EncoderReplacementFallback(String.Empty), new DecoderExceptionFallback()), 
            //        Encoding.UTF8.GetBytes(dtBSONValues.Rows[0][dc].ToString())));


            //        dtBSONValues.Rows[0][dc]=Regex.Replace(outputs, "[^a-zA-Z0-9]+", " ");
            //    }
            //}
            #endregion

    */
            #endregion

            DataTable dtRet = new DataTable();
            string content = bsondata;//File.ReadAllText(filepath);
            //===========================
            //string[] columnsName = { "UniqueId", "BenchmarkDistance","Comment","DOB","Date","Gender", "Height", "MeasurementTime","Month",
            //                        "Name", "PatientId","Year","lut"};

            string[] columnsName = { "UniqueId", "BenchmarkDistance","Comment","DOB","Date","Gender", "Height", "MeasurementTime","Month",
                                    "Name", "PatientId","Year"};


            string[] valuesOnly = content.Split(columnsName, StringSplitOptions.None);
            //===========================
            int k = 2;
            DataTable dtpd = new DataTable();
            foreach(string gp in columnsName)
            {
                dtpd.Columns.Add(gp); 
            }
            List<string> kspl = new List<string>();
            kspl.AddRange(valuesOnly);
            kspl.RemoveAt(0);
            kspl.RemoveAt(0);
            dtpd.Rows.Add(kspl.ToArray());

            for(int h=0;h<dtpd.Columns.Count;h++)
            {
                string clname = dtpd.Columns[h].ColumnName;
                //Condition modified by Rajnish for GSP-676 (&& clname != "Comment" && clname != "Name" && clname != "PatientId")
                if (clname != "DOB" && clname != "MeasurementTime" && clname != "Comment" && clname != "Name" && clname != "PatientId")
                {
                    var tmp = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
                         Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                             new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(dtpd.Rows[0][clname].ToString())));
                    dtpd.Rows[0][clname] = Regex.Replace(tmp.Trim(), "[^a-zA-Z0-9]+", " ");
                }
                else if (clname == "Comment" || clname == "Name" || clname == "PatientId")
                {
                    //--else if case Added by Rajnish for GSP-676--//
                    var tmp1 = string.Empty;
                    tmp1 = dtpd.Rows[0][clname].ToString();
                    tmp1 = Regex.Replace(tmp1.Trim(), "[^a-zA-Z0-9+*/=]+", " ");

                    try
                    {
                        string[] tmp2 = tmp1.Split(' ');
                        foreach (string base64data in tmp2)
                        {
                            if (base64data.Length > 0)
                            {
                                bool IsValidBase64 = IsBase64String(base64data);
                                if (IsValidBase64)
                                {
                                    byte[] data = Convert.FromBase64String(base64data);
                                    tmp1 = System.Text.Encoding.UTF8.GetString(data);
                                    dtpd.Rows[0][clname] = tmp1;
                                    dtpd.Rows[0][clname] = tmp1.Replace("'","''");//Added by Rajnish For GSP-901
                                    break;
                                }
                            }
                            else
                            {
                                dtpd.Rows[0][clname] = "";
                            }
                        }
                    }
                    catch (Exception ex) { ex.ToString(); }

                    //-----------------------------------------------------//
                }
                else
                {
                    string valHold = dtpd.Rows[0][clname].ToString().Replace('-', 'x').Replace(':', 'y');
                    var clearval = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
                         Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                             new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(valHold)));
                    valHold = Regex.Replace(clearval.Trim(), "[^a-zA-Z0-9]+", " ");
                    dtpd.Rows[0][clname] = valHold.Replace('x', '-').Replace('y', ':');
                }
            }

            #region obsoleteLogic
            //string[] uniqueId = { "UniqueId" };
            //string[] discardBeforeUnique = content.Split(uniqueId, StringSplitOptions.None);

            //string withUniqTillEnd = uniqueId[0] + " " + discardBeforeUnique[1];// Image is there

            ////=====================================DOB Extraction===========================
            //string[] dob = { "DOB" };
            //string[] mtime = { "MeasurementTime" };

            //string[] startToTillBeforedob = withUniqTillEnd.Split(dob, StringSplitOptions.None);

            //string firstHalf = startToTillBeforedob[0];
            //string secondHalf = "DOB " + startToTillBeforedob[1];

            ////Now get AfterImageData
            //string[] kpd = { "Date" };
            //string[] withdob = secondHalf.Split(kpd, StringSplitOptions.None);
            //string afterdobTillEnd = "Date" + withdob[1];

            //string f1data = firstHalf;
            //string dobData9 = "DOB " + (content.Split(dob, StringSplitOptions.None)[1]).Split(kpd, StringSplitOptions.None)[0];
            //string s2data = afterdobTillEnd;
            ////===============================================================


            ////=============================MeasurementTime extraction===================
            //string[] mt = { "MeasurementTime" };
            //string[] b4mt = { "Height","Month" };
            //string[] aftmt = { "Month" };
            //string[] withmt = s2data.Split(b4mt, StringSplitOptions.None);

            //string mtNameValue = withmt[1];

            //mtNameValue = mtNameValue.Replace('-', 'z').Replace(':', 'y');
            //var mtclear = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
            //     Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
            //         new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(mtNameValue)));
            //mtclear = Regex.Replace(mtclear, "[^a-zA-Z0-9]+", " ");



            ////==========================================================================





            ////Clean non image Data
            //var outputsFirst = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
            //      Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
            //          new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(f1data)));
            //outputsFirst = Regex.Replace(outputsFirst, "[^a-zA-Z0-9]+", " ");

            //var outputSecond = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
            //  Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
            //      new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(s2data)));
            //outputSecond = Regex.Replace(outputSecond, "[^a-zA-Z0-9]+", " ");


            //List<string> gather = new List<string>();
            //foreach (string k1 in outputsFirst.Split(' '))
            //{
            //    if (k1.Length > 0)
            //        gather.Add(k1);
            //}

            //gather.Add(dobData9.Split(' ')[0]);
            //gather.Add(dobData9.Split(' ')[1]);

            //foreach (string k2 in outputSecond.Split(' '))
            //{
            //    if (k2.Length > 0)
            //        gather.Add(k2);
            //}
            //List<string> columnList = new List<string>();
            //List<string> valuesList = new List<string>();

            //for (int i = 0; i < gather.Count - 1; i += 2)
            //{
            //    columnList.Add(gather[i]);
            //    valuesList.Add(gather[i + 1]);
            //}
            //DataTable dtServerKneeDown = new DataTable();
            //foreach (string cl in columnList)
            //{
            //    dtServerKneeDown.Columns.Add(cl);
            //}
            //dtServerKneeDown.Rows.Add(valuesList.ToArray());
            //string imagebytesone = dtServerKneeDown.Rows[0]["DOB"].ToString();
            ////non image data cleaned

            //return dtServerKneeDown;

            #endregion



            return dtpd;

        }

        public static DataTable SpliterSideBodyPosition(string bsonData)
        {
            //=====================STANDING
            string content = bsonData;
            string[] uniqueId = { "UniqueId" };
            string[] discardBeforeUnique = content.Split(uniqueId, StringSplitOptions.None);

            string withUniqTillEnd = uniqueId[0] + " " + discardBeforeUnique[1];// Image is there


            string[] ImageBytes = { "ImageBytes" };

            string[] startToTillBeforeImage = withUniqTillEnd.Split(ImageBytes, StringSplitOptions.None);

            string firstHalf = startToTillBeforeImage[0];
            string secondHalf = "ImageBytes " + startToTillBeforeImage[1];

            //Now get AfterImageData
            string[] kpd = { "KneeLeftBeltX" };
            string[] withImageData = secondHalf.Split(kpd, StringSplitOptions.None);
            string afterImageTillEnd = "KneeLeftBeltX" + withImageData[1];

            string f1data = firstHalf;
            string imageData9 = "ImageBytes " + (content.Split(ImageBytes, StringSplitOptions.None)[1]).Split(kpd, StringSplitOptions.None)[0];
            string s2data = afterImageTillEnd;

            imageData9 = imageData9.Replace("\0\0\0\u0001\0", "");
            imageData9 = imageData9.Replace("\0\u0002", "");

            //Sumit v2.0
            string[] starttag = { "thiscannotbeacoincedenceatstart" };
            string[] endtag = { "atendthisisalsonotacoincedence" };
            string imagedatahalfclean = imageData9.Split(starttag, StringSplitOptions.None)[1];
            string imagedataallcleaned = imagedatahalfclean.Split(endtag, StringSplitOptions.None)[0];
            imageData9 = imagedataallcleaned;
            //end v2.0

            //Clean non image Data
            var outputsFirst = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
                  Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                      new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(f1data)));
            outputsFirst = Regex.Replace(outputsFirst, "[^a-zA-Z0-9]+", " ");

            var outputSecond = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
              Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                  new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(s2data)));
            outputSecond = Regex.Replace(outputSecond, "[^a-zA-Z0-9]+", " ");


            List<string> gather = new List<string>();
            foreach (string k1 in outputsFirst.Split(' '))
            {
                if (k1.Length > 0)
                    gather.Add(k1);
            }

            gather.Add("ImageBytes");//imageData9.Split(' ')[0]);
            gather.Add(imageData9);//.Split(' ')[1]);

            foreach (string k2 in outputSecond.Split(' '))
            {
                if (k2.Length > 0)
                    gather.Add(k2);
            }
            List<string> columnList = new List<string>();
            List<string> valuesList = new List<string>();

            for (int i = 0; i < gather.Count - 1; i += 2)
            {
                if(gather[i]=="Status" || gather[i] == "TableName" || gather[i] == "Computerid" || gather[i] == "Activationkey")
                {
                    continue;
                }
                columnList.Add(gather[i]);
                valuesList.Add(gather[i + 1]);
            }
            DataTable dtServerSideBody = new DataTable();
            foreach (string cl in columnList)
            {
                dtServerSideBody.Columns.Add(cl);
            }
            dtServerSideBody.Rows.Add(valuesList.ToArray());
            string imagebytesone = dtServerSideBody.Rows[0]["ImageBytes"].ToString();
            //non image data cleaned


            //============TESTING for imagedata
            //File.WriteAllText("ImgBytFrmProg", imagebytesone,Encoding.UTF8);

            //string t1 = File.ReadAllText("ImgBytFrmProg", Encoding.Convert(Encoding.UTF8,Encoding.ASCII);
            //===================================


            return dtServerSideBody;

        }

    }
}
