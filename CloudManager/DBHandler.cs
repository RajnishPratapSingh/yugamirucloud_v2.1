﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace CloudManager
{
    public static class DBHandler
    {

        public static string QryConverter(DataTable dtToConvert, string tableName)
        {
            string qrytemplate = "INSERT OR REPLACE into " + tableName + "(col#) values(val#)";
            if(dtToConvert==null || dtToConvert.Rows.Count==0 || !dtToConvert.Columns.Contains("uniqueid"))
            {
                throw new FormatException("table is not in desired format");
            }
            string cols = string.Empty;
            string vals = string.Empty;
            foreach(DataRow dr in dtToConvert.Rows)
            {
                foreach (DataColumn cl in dtToConvert.Columns)
                {
                    cols = cols + "," + cl.ColumnName;
                    vals = vals+ ",'" + dr[cl.ColumnName].ToString().Trim()+"'";
                }
                cols = cols.Trim(',');
                vals = vals.Replace("\0\r","").Trim(',');
            }

            string finalQry = qrytemplate.Replace("col#", cols).Replace("val#", vals) + ";";

            return finalQry;
        }

        public static bool ExecQry(string qry,string UniqueId,Tables tableNme)
        {
            try
            {
                if (UniqueId.Trim() == "0")
                {
                    throw new Exception("Invalid command: ID cannot be 0");
                }
                SQLiteConnection sqlite = new SQLiteConnection("Data Source=" + GlobalItems.sqliteDBPath);
                SQLiteCommand cmd = new SQLiteCommand();
                if (tableNme != Tables.PatientDetails)
                {
                    //delete existing
                    cmd = sqlite.CreateCommand();
                    cmd.CommandText = "delete from " + tableNme.ToString() + " where uniqueid='" + UniqueId+"'";
                    //cmd = sqlite.CreateCommand();
                    //cmd.CommandText = qry;
                    sqlite.Open();
                    cmd.ExecuteNonQuery();
                    sqlite.Close();
                }
                //insert new
                sqlite.Open();
                //cmd = new SQLiteCommand();
                cmd = sqlite.CreateCommand();
                cmd.CommandText = qry;
                cmd.ExecuteNonQuery();
                sqlite.Close();
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
                //MessageBox.Show(ex.Message);
                throw ex;
            }
            return true;
        }

        public static bool ResetLut(string id)
        {
            #region resetting
            string updateQry = "update PatientDetails set lut='' where uniqueid=" + id + ";";
            SQLiteConnection sqlite = new SQLiteConnection("Data Source=" + GlobalItems.sqliteDBPath);
            SQLiteCommand cmd = new SQLiteCommand();
            cmd=sqlite.CreateCommand();
            //delete existing
            cmd.CommandText = updateQry;            
            sqlite.Open();
            cmd.ExecuteNonQuery();
            sqlite.Close();
            #endregion
            return true;
        }

        /// <summary>
        /// add some string values to imagesbytes data (at data start and at data end) fixed to 'thiscannotbeacoincedenceatstart' and 'atendthisisalsonotacoincedence'
        /// both string one for start another for end is fixed, these will be appended with Imagebytes data
        /// </summary>
        /// <param name="dt">table must have one row only</param>
        /// <returns></returns>
        public static DataTable AddToImageBytesBeforeSend(DataTable dt)
        {
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.ColumnName.ToUpper() == "IMAGEBYTES")
                    {
                        dr[dc] = "thiscannotbeacoincedenceatstart" + dr[dc].ToString() + "atendthisisalsonotacoincedence";
                        break;
                    }
                }
            }

            return dt;
        }

        
    }
}
