﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Yugamiru
{
    public class RealValueRangeDefinitionMgr
    {
        int m_iCount;
        RealValueRangeDefinitionElement[] m_pElement;
        SymbolFunc m_SymbolFunc = new SymbolFunc();

        // ’lˆæƒVƒ“ƒ{ƒ‹’è‹`ƒ}ƒXƒ^[ƒf[ƒ^.
        public RealValueRangeDefinitionMgr()
        {
            m_iCount = 0;
            m_pElement = new RealValueRangeDefinitionElement[m_iCount];
        }




        public RealValueRangeDefinitionMgr(RealValueRangeDefinitionMgr rSrc)
        {
            m_iCount = rSrc.m_iCount;
            m_pElement = new RealValueRangeDefinitionElement[m_iCount];

            if (rSrc.m_pElement != null)
            {
                RealValueRangeDefinitionElement[] m_pElement;
                m_pElement = new RealValueRangeDefinitionElement[m_iCount];
                int i = 0;
                for (i = 0; i < m_iCount; i++)
                {
                    m_pElement[i] = rSrc.m_pElement[i];
                }
            }
        }

        ~RealValueRangeDefinitionMgr()
        {

        }
        /*
         &CRealValueRangeDefinitionMgr::operator=( const CRealValueRangeDefinitionMgr &rSrc )
        {
            if (m_pElement != NULL)
            {
                delete[] m_pElement;
                m_pElement = NULL;
            }
            m_iCount = rSrc.m_iCount;
            if (rSrc.m_pElement != NULL)
            {
                m_pElement = new CRealValueRangeDefinitionElement[m_iCount];
                int i = 0;
                for (i = 0; i < m_iCount; i++)
                {
                    m_pElement[i] = rSrc.m_pElement[i];
                }
            }
            return *this;
        }
        */

        public bool ReadFromFile(string lpszFolderName, string lpszFileName, string pchErrorFilePath /* = NULL */ )
        {
            char[] buf = new char[1024];
            List<RealValueRangeDefinitionElement> listElement = new List<RealValueRangeDefinitionElement>();

            if (m_pElement != null)
            {
                /* delete[] m_pElement;
                 m_pElement = NULL;*/
                m_pElement = null;
            }
            m_iCount = 0;

            DualSourceTextReader DualSourceTextReader = new DualSourceTextReader();
            int i = DualSourceTextReader.Open(lpszFolderName, lpszFileName);
            if (i == 0)
            {
                return false;
            }

            string strError = string.Empty;
            int iLineNo = 1;
            //i = DualSourceTextReader.IsEOF();
            while (!(DualSourceTextReader.IsEOF()))
            {
                buf[0] = '\0';
                DualSourceTextReader.Read(ref buf,1024);
                RealValueRangeDefinitionElement Element = new RealValueRangeDefinitionElement();
                int iErrorCode = Element.ReadFromString(buf);
                if (iErrorCode == Constants.READFROMSTRING_SYNTAX_OK)
                {
                    listElement.Add(Element);
                }
                else
                {
                  m_SymbolFunc.OutputReadErrorString(ref strError, iLineNo, iErrorCode);
                }
                iLineNo++;
            }

            // d•¡’è‹`ƒ`ƒFƒbƒN.
            //listElement.sort(cmpfunc());
            listElement.OrderBy(t => t.m_strRangeSymbol); //lambda
            /*   list<CRealValueRangeDefinitionElement>::iterator index;*/
            string strOldSymbol = string.Empty;
            bool bFoundMultipleDefinition = false;

            foreach (var element in listElement)
            {
                if (strOldSymbol == element.m_strRangeSymbol)
                {
                    strOldSymbol = string.Format("MultipleDefinition %s\n", strOldSymbol);
                }
                strOldSymbol = element.m_strRangeSymbol;
            }
            /* for (index = listElement.begin(); index != listElement.end(); index++)
              for(int j = 0;j<listElement.Count;j++)
              {
                  if (index->m_strRangeSymbol == strOldSymbol)
                  {
                      CString strTmp;
                      strTmp.Format("MultipleDefinition %s\n", strOldSymbol);
                      strError += strTmp;
                  }
                  strOldSymbol = index->m_strRangeSymbol;
              }*/

            if (pchErrorFilePath != null)
            {
                /*  FILE* fp = ::fopen(pchErrorFilePath, "wt");

                  ::fwrite((const char*)strError, 1, strError.GetLength(), fp );

                  ::fclose(fp);
                  fp = NULL;*/
                StreamWriter fp = new StreamWriter(pchErrorFilePath);
                fp.Dispose();

            }

            // €–Ú”•ª‚¾‚¯ƒƒ‚ƒŠŠm•Û.
            int iSize = listElement.Count;
            if (iSize <= 0)
            {
                return true;
            }
            m_pElement = new RealValueRangeDefinitionElement[iSize];
            if (m_pElement == null)
            {
                return false;
            }
            m_iCount = iSize;

            /*  int i = 0;
              for (i = 0, index = listElement.begin(); index != listElement.end(); index++, i++)
              {
                  m_pElement[i] = (*index);
              }*/
            for (i = 0; i < listElement.Count; i++)
            {
                m_pElement[i] = listElement[i];
            }

            return true;
        }

        public int GetElementCount()
        {
            return m_iCount;
        }

        public bool IsSatisfied( string pchRangeSymbol, double dValue) 
        {

    int i = 0;
	for( i = 0; i<m_iCount; i++ ){
		if ( m_pElement[i].IsEqualRangeSymbol(pchRangeSymbol ) ){
			return ( m_pElement[i].IsSatisfied(dValue ) );
		}
}
	return false;
}

public bool GetRange(ref int iMinValueOperatorID, ref int iMaxValueOperatorID, ref double dMinValue, ref double dMaxValue, string pchRangeSymbol) 
{
	int i = 0;
	for( i = 0; i<m_iCount; i++ ){
		if ( m_pElement[i].IsEqualRangeSymbol(pchRangeSymbol ) ){
			m_pElement[i].GetRange(ref iMinValueOperatorID, ref iMaxValueOperatorID, ref dMinValue, ref dMaxValue );
			return true;
		}
	}
	return false;
}

    }
}
