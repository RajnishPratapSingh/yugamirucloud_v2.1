﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LicenseInformation
{
    public partial class Form1 : Form
    {
        //test 
        bool closingFromOk;
        System.Text.RegularExpressions.Regex r;
        public Form1()
        {
            
            r = new System.Text.RegularExpressions.Regex(@"[~`!@#$%^&*()+=|\{}':;.,<>/?[\]""_]");
            closingFromOk = false;
            InitializeComponent();
            ToolTip tooltp = new ToolTip();
            tooltp.IsBalloon = true;
            tooltp.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            tooltp.SetToolTip(LicenseKeyLblQue, "Enter the Activation Key which you would have received"+
                Environment.NewLine+"in your registered email after purchasing the yugamiru license.");
        }

        private void LicenseLbl_Click(object sender, EventArgs e)
        {

        }

        private void OkBtn_Click(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(licenseTxtbox.Text))
            {            
            
                if ((r.IsMatch(licenseTxtbox.Text)) || (r.IsMatch(InstallNameTxtBox.Text)) || (r.IsMatch(InstallByTxtBox.Text))
                    || (r.IsMatch(CommentTxtBox.Text)) || (r.IsMatch(InstallDateTxtBox.Text)))
                {
                    MessageBox.Show("Special characters are not allowed");
                    //licenseTxtbox.Text = "";
                    //InstallNameTxtBox.Text = "";
                    //InstallByTxtBox.Text = "";
                    //CommentTxtBox.Text = "";
                    //DateTime currentDt = DateTime.Now;
                    //InstallDateTxtBox.Text = currentDt.ToString("dd-MM-yyyy");
                    return;
                }
                else
                {

                    string licenseText = licenseTxtbox.Text;
                    string installNameText = InstallNameTxtBox.Text;
                    string ComputerID = WebComCation.keyRequest.GetComputerID();
                    string InstallBy = InstallByTxtBox.Text;
                    string comment = CommentTxtBox.Text;
                    string InstallDateText = InstallDateTxtBox.Text;
                    string PC_Name = PCNameTxtBox.Text;

                  bool success=  sendRecord(PC_Name, installNameText, ComputerID, InstallDateText, InstallBy, licenseText, comment);

                    if(!success)
                    {
                        MessageBox.Show("License key is invalid");
                        licenseTxtbox.Focus();
                        licenseTxtbox.SelectAll();
                        return;
                    }

                    closingFromOk = true;
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Please provide License Key");
            }
        }
        public bool sendRecord(string computer_name, string installation_name, string comp_id, string Install_date, string Install_by, string activation_key = null, string comm = null)
        {
            bool retVal = false;
            string language = "";
            string installId = "";
            string reg_user_id = "";
            string jsonQuery = "[{\"Computer_id\":\"" + comp_id +
                               "\",\"Computer_name\":\"" + computer_name +
                               "\",\"Activation_key\":\"" + activation_key +
                               "\",\"Installation_name\":\"" + installation_name +
                               "\",\"Date_of_install\":\"" + InstallDateTxtBox.Text +
                               "\",\"Install_id\":\"" + installId +
                               "\",\"reg_user_id\":\"" + reg_user_id +
                                "\",\"Installed_by\":\"" + Install_by +
                                 "\",\"Comment\":\"" + comm +
                                "\",\"Language\":\"" + language + "\"}]";
            String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
            var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);

            JsonSerializer jsonSerializer = new JsonSerializer();

            MemoryStream objBsonMemoryStream = new MemoryStream();

            Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);

            jsonSerializer.Serialize(bsonWriterObject, studentObject);
            //  text = queryString;
            byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);
                                                               // WebRequest

            //Connect to our Yugamiru Web Server
            //WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/validatelicense");
            WebRequest webRequest = WebRequest.Create("http://52.197.210.82/apioauthdata/index.php/home/installationdetails");
            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            webRequest.ContentLength = requestByte.Length;
            Stream webDataStream = null;
            try
            {
                webDataStream = webRequest.GetRequestStream();
                webDataStream.Write(requestByte, 0, requestByte.Length);
            }
            catch (Exception ex)
            {

            }
            string ed = webDataStream.ToString();

            // get the response from our stream

            WebResponse webResponse = webRequest.GetResponse();
            webDataStream = webResponse.GetResponseStream();

            // convert the result into a String
            StreamReader webResponseSReader = new StreamReader(webDataStream);
            String responseFromServer = webResponseSReader.ReadToEnd();

            #region Parsing_Response_Data
            string resData = responseFromServer;
            /*Sample:
             * "W\0\0\0\u0010Status\0�\u0001\0\0\u0002Computerid\0\u0001\0\0\0\0\u0002
             * Install_id\0\u0001\0\0\0\0\u0002Message\0\u0017\0\0\0Activation Key Invalid\0\0"             
             */
            if (resData.Contains("Invalid"))
            {
                retVal = false;
            }
            else if (resData.Contains("Successfully"))
            {
                retVal = true;
                //Writedown this self installation 
                //information data in local txt file and update the same after application activation
                try
                {                   

                  string installId_RegId=  ParseJsonInstallInfo(resData);
                    if(installId_RegId.Length>1)
                    {
                        installId = installId_RegId.Split('_')[0];
                        reg_user_id = installId_RegId.Split('_')[1];
                        jsonQuery = "[{\"Installation_name\":\"" + installation_name +
                               "\",\"Date_of_install\":\"" + InstallDateTxtBox.Text +
                               "\",\"Install_id\":\"" + installId +
                               "\",\"reg_user_id\":\"" + reg_user_id +
                                "\",\"Installed_by\":\"" + Install_by +
                                 "\",\"Comment\":\"" + comm +
                                "\",\"Language\":\"" + language + "\"}]";

                        string temp_Ins_file = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
                                                                "\\gsport\\Yugamiru cloud\\database\\TempInstalInfoJson.txt";

                        File.WriteAllText(temp_Ins_file, jsonQuery);
                    }
                    
                }
                catch (Exception ex)
                { }
            }
            #endregion
            return retVal;
        }

        string ParseJsonInstallInfo(string bsonData)
        {
            string[] knownVals = { "Status", "Computer_id", "Install_id", "reg_user_id", "Message" };
            string[] parts1 = bsonData.Split(knownVals, StringSplitOptions.None);
            List<string> lstVals = new List<string>(parts1);

            for(int i=1;i<lstVals.Count;i++)
            {
                lstVals[i] = lstVals[i].Replace("\09\0\0\0", "").Replace("\0", "")
                    .Replace("\u0002", "").Replace("\u0004", "");
                string q = lstVals[i].Replace("\09\0\0\0", "").Replace("\0", "")
                    .Replace("\u0002", "").Replace("\u0004", "");
            }
            if(lstVals.Count==6)
            {
                //string retStr = "Install_id=" + lstVals[3]+", reg_user_id=" + lstVals[4];
                string retStr = lstVals[3] + "_" + lstVals[4];
                return retStr;
            }

            return "";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
         
            PCNameTxtBox.Text = Environment.MachineName;
            DateTime currentDt = DateTime.Now;
            InstallDateTxtBox.Text = currentDt.ToString("dd-MM-yyyy");
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            DateTime date = this.dateTimePicker1.Value;

            this.InstallDateTxtBox.Text = date.ToString("dd-MM-yyyy");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if(
                MessageBox.Show(
                    "Cancelling this step will cancel the installation" +
                    " of the Yugamiru Cloud Application."
                    +Environment.NewLine +"Do you want to close and cancel the installation?","gsport",MessageBoxButtons.YesNo)==DialogResult.Yes)
            {
                closingFromOk = true;
                Environment.Exit(1);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(!closingFromOk)
            {
                btnCancel.PerformClick();
                e.Cancel = true;
                closingFromOk = false;
            }
        }

        private void licenseTxtbox_KeyDown(object sender, KeyEventArgs e)
        {
            //Keys keyData = e.KeyData;
            //string s = keyData.ToString();
        }

        private void licenseTxtbox_TextChanged(object sender, EventArgs e)
        {
            if (r.IsMatch(((TextBox)(sender)).Text))
            {
                MessageBox.Show("Special characters are not allowed");
                ((TextBox)(sender)).Text = ((TextBox)(sender)).Text.Replace(((TextBox)(sender)).Text[((TextBox)(sender)).Text.Length - 1].ToString(), "");
            }
        }
    }
}
