﻿namespace LicenseInformation
{
    partial class frmCloudSycnSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCloudSycnSettings));
            this.btnRefresh = new System.Windows.Forms.Button();
            this.flpnlPCs = new System.Windows.Forms.FlowLayoutPanel();
            this.btnSaveSync = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbSrchParam = new System.Windows.Forms.ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(12, 12);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 30);
            this.btnRefresh.TabIndex = 0;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // flpnlPCs
            // 
            this.flpnlPCs.AutoScroll = true;
            this.flpnlPCs.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flpnlPCs.Location = new System.Drawing.Point(0, 96);
            this.flpnlPCs.Name = "flpnlPCs";
            this.flpnlPCs.Size = new System.Drawing.Size(421, 400);
            this.flpnlPCs.TabIndex = 1;
            // 
            // btnSaveSync
            // 
            this.btnSaveSync.Location = new System.Drawing.Point(93, 12);
            this.btnSaveSync.Name = "btnSaveSync";
            this.btnSaveSync.Size = new System.Drawing.Size(240, 30);
            this.btnSaveSync.TabIndex = 2;
            this.btnSaveSync.Text = "Save Sync Settings";
            this.btnSaveSync.UseVisualStyleBackColor = true;
            this.btnSaveSync.Click += new System.EventHandler(this.btnSaveSync_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(339, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 30);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Search PC";
            // 
            // cmbSrchParam
            // 
            this.cmbSrchParam.FormattingEnabled = true;
            this.cmbSrchParam.Items.AddRange(new object[] {
            "PC Name",
            "Installation Name"});
            this.cmbSrchParam.Location = new System.Drawing.Point(73, 67);
            this.cmbSrchParam.Name = "cmbSrchParam";
            this.cmbSrchParam.Size = new System.Drawing.Size(134, 21);
            this.cmbSrchParam.TabIndex = 5;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(244, 67);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(162, 20);
            this.textBox1.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(221, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "=";
            // 
            // frmCloudSycnSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 496);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.cmbSrchParam);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSaveSync);
            this.Controls.Add(this.flpnlPCs);
            this.Controls.Add(this.btnRefresh);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCloudSycnSettings";
            this.Text = "Yugamiru Cloud: Data Synchronization Settings";
            this.Load += new System.EventHandler(this.frmCloudSycnSettings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.FlowLayoutPanel flpnlPCs;
        private System.Windows.Forms.Button btnSaveSync;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbSrchParam;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
    }
}