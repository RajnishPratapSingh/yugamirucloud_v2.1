﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Yugamiru.stretchDIBbits;
using System.Drawing.Printing;
using System.Drawing.Imaging;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.Drawing.Drawing2D;
using System.Diagnostics;
using System.Reflection;
using System.Threading;

namespace Yugamiru
{
    public partial class ResultView : Form
    {


        int m_ZoomFlag = 0;
        PictureBox m_MainPic = new PictureBox();
        JointEditDoc m_JointEditDoc;
        FrontBodyResultWnd m_wndStandingPosture;
        FrontBodyResultWnd m_wndKneedownPosture;
        //MyData m_BalanceLabData = new MyData();
        internal PrintPreviewDialog PrintPreviewDialog1;
        private PrintDocument document = new PrintDocument();

        FrontBodyAngle FrontBodyAngleStanding = new FrontBodyAngle();
        FrontBodyAngle FrontBodyAngleKneedown = new FrontBodyAngle();
        SideBodyAngle m_SideBodyAngle = new SideBodyAngle();
        bool m_SecondPageFlag = false;
        int m_ZoomCounter = 0;
        public bool ImportYGAFileFlag = false;

        SymbolFunc m_SymbolFunc = new SymbolFunc();
        PrintPreviewDialog printPreviewDialog;
        PrintDocument printDocument_temp;
        public static DateTime qlsGetDate; //Added By Suhana

        int NextPageNum = 1;
        string m_CL_Path = Constants.path + Yugamiru.Properties.Resources.CURRENT_LANGUAGE;

        public Font printFont =
               new Font("MS UI Gothic", 11,
               FontStyle.Regular);

        /// <summary>
        /// The backgroundworker object on which the time consuming operation 
        /// shall be executed
        /// </summary>
        /// 

        ~ResultView()
        {
            //MessageBox.Show("hi");

        }

     

        public ResultView(JointEditDoc GetDocument, MyData m_BalanceLabData)
        {
            // Declare a PrintDocument object named document.
            InitializeComponent();

            /*var watch = System.Diagnostics.Stopwatch.StartNew();
            ImportScoresheetImagesintoFolder();
            watch.Stop();
            var elapsedMs = watch.Elapsed.Seconds;
            MessageBox.Show("elapsedMs = " +elapsedMs);*/

            m_wndStandingPosture = new FrontBodyResultWnd();
            m_wndKneedownPosture = new FrontBodyResultWnd();
            m_wndStandingPosture.SetLanguage(GetDocument.GetLanguage());
            m_wndKneedownPosture.SetLanguage(GetDocument.GetLanguage());

            m_MainPic.Size = new Size(Yugamiru.Properties.Resources.Mainpic6.Size.Width,
                Yugamiru.Properties.Resources.Mainpic6.Size.Height);
            m_MainPic.BackColor = Color.Transparent;
            this.Controls.Add(m_MainPic);
            m_MainPic.Image = Yugamiru.Properties.Resources.Mainpic6;

            IDC_Mag1Btn.Image = Yugamiru.Properties.Resources.mag2_up;
            IDC_Mag2Btn.Image = Yugamiru.Properties.Resources.mag1_up;
            IDC_ResetImgBtn.Image = Yugamiru.Properties.Resources.mag3_up;

            IDC_BTN_NAMECHANGE.Image = Yugamiru.Properties.Resources.namechange_on;
            IDC_BTN_RETURNTOPMENU.Image = Yugamiru.Properties.Resources.returnstart_up;
            IDC_RemeasurementBtn.Image = Yugamiru.Properties.Resources.imagechange_up;
            IDC_EditBtn.Image = Yugamiru.Properties.Resources.jointedit_up;


            IDC_PrintBtn.Image = Yugamiru.Properties.Resources.reportprint_up;
            IDC_ScoresheetBtn.Image = Yugamiru.Properties.Resources.reportdisplay_up;
            IDC_BTN_DATASAVE.Image = Yugamiru.Properties.Resources.datasave_up;
            IDC_MeasurementEndBtn.Image = Yugamiru.Properties.Resources.startred_up;

            if (GetDocument.m_FrontStandingImageBytes != null)
                m_wndStandingPosture.SetBackgroundBitmap(1024, 1280, GetDocument.m_FrontStandingImageBytes);
            m_wndStandingPosture.SetDataVersion(GetDocument.GetDataVersion());
            m_wndStandingPosture.SetMarkerSize(GetDocument.GetMarkerSize());

            m_wndStandingPosture.SetMidLine(GetDocument.IsValidMidLine());
            m_wndStandingPosture.SetMidLineWidth(GetDocument.GetMidLineWidth());
            m_wndStandingPosture.SetMidLineColor(GetDocument.GetMidLineColor());
            m_wndStandingPosture.SetMidLineStyle(GetDocument.GetMidLineStyle());

            m_wndStandingPosture.SetCentroidLine(GetDocument.GetDisplayCentroid());
            m_wndStandingPosture.SetCentroidLineWidth(GetDocument.GetCentroidLineWidth());
            m_wndStandingPosture.SetCentroidLineColor(GetDocument.GetCentroidLineColor());
            m_wndStandingPosture.SetCentroidLineStyle(GetDocument.GetCentroidLineStyle());

            m_wndStandingPosture.SetStyleLine(GetDocument.IsValidStyleLine());
            m_wndStandingPosture.SetStyleLineStyle(GetDocument.GetStyleLineStyle());
            m_wndStandingPosture.SetStyleLineColor(GetDocument.GetStyleLineColor());
            m_wndStandingPosture.SetStyleLineWidth(GetDocument.GetStyleLineWidth());

            Font lf;
            //GetDocument.GetLogFont(lf);
            //m_wndStandingPosture.SetLogFont(lf);
            m_wndStandingPosture.SetFontColor(GetDocument.GetFontColor());
            m_wndStandingPosture.SetOutline(GetDocument.IsValidOutline());
            m_wndStandingPosture.SetOutlineColor(GetDocument.GetOutlineColor());

            int iBenchmarkDistance = GetDocument.GetBenchmarkDistance();
            SideBodyPosition SideBodyPosition = new SideBodyPosition();
            GetDocument.GetSideBodyPosition(ref SideBodyPosition);
            Point ptBenchmark1 = new Point(0, 0);
            Point ptBenchmark2 = new Point(0, 0);
            SideBodyPosition.GetBenchmark1Position(ref ptBenchmark1);
            SideBodyPosition.GetBenchmark2Position(ref ptBenchmark2);

            FrontBodyPosition FrontBodyPositionStanding = new FrontBodyPosition();
            //m_BalanceLabData.InitBodyBalance(1024, 1280, m_CalibInf, m_CalcPostureProp);
            //FrontBodyPositionStanding = m_BalanceLabData.m_FrontBodyPositionStanding;

            FrontBodyPositionStanding.SetAllPositionDetected();

            GetDocument.GetStandingFrontBodyPosition(ref GetDocument.m_FrontBodyPositionStanding); // newly added by meena for beta testing
            m_wndStandingPosture.SetFrontBodyPosition(GetDocument.m_FrontBodyPositionStanding);
            //GetDocument.SetStandingFrontBodyPosition(GetDocument.m_FrontBodyPositionStanding);

            GetDocument.CalcBodyAngleStanding(ref FrontBodyAngleStanding);
            m_wndStandingPosture.SetFrontBodyAngle(FrontBodyAngleStanding);
            FrontBodyResultData FrontBodyResultDataStanding = new FrontBodyResultData();
            GetDocument.CalcStandingFrontBodyResultData(ref FrontBodyResultDataStanding);

            m_wndStandingPosture.SetFrontBodyResultData(FrontBodyResultDataStanding);
            m_wndStandingPosture.SetBenchmark1Point(ptBenchmark1);
            m_wndStandingPosture.SetBenchmark2Point(ptBenchmark2);
            m_wndStandingPosture.SetBenchmarkDistance(iBenchmarkDistance);
            if (GetDocument.m_FrontKneedownImageBytes != null)
                m_wndKneedownPosture.SetBackgroundBitmap(1024, 1280, GetDocument.m_FrontKneedownImageBytes);
            m_wndKneedownPosture.SetDataVersion(GetDocument.GetDataVersion());
            m_wndKneedownPosture.SetMarkerSize(GetDocument.GetMarkerSize());

            m_wndKneedownPosture.SetMidLine(GetDocument.IsValidMidLine());
            m_wndKneedownPosture.SetMidLineWidth(GetDocument.GetMidLineWidth());
            m_wndKneedownPosture.SetMidLineColor(GetDocument.GetMidLineColor());
            m_wndKneedownPosture.SetMidLineStyle(GetDocument.GetMidLineStyle());

            m_wndKneedownPosture.SetCentroidLine(GetDocument.GetDisplayCentroid());
            m_wndKneedownPosture.SetCentroidLineWidth(GetDocument.GetCentroidLineWidth());
            m_wndKneedownPosture.SetCentroidLineColor(GetDocument.GetCentroidLineColor());
            m_wndKneedownPosture.SetCentroidLineStyle(GetDocument.GetCentroidLineStyle());

            m_wndKneedownPosture.SetStyleLine(GetDocument.IsValidStyleLine());
            m_wndKneedownPosture.SetStyleLineStyle(GetDocument.GetStyleLineStyle());
            m_wndKneedownPosture.SetStyleLineColor(GetDocument.GetStyleLineColor());
            m_wndKneedownPosture.SetStyleLineWidth(GetDocument.GetStyleLineWidth());

            //GetDocument()->GetLogFont(&lf);
            //m_wndKneedownPosture.SetLogFont(lf);
            m_wndKneedownPosture.SetFontColor(GetDocument.GetFontColor());
            m_wndKneedownPosture.SetOutline(GetDocument.IsValidOutline());
            m_wndKneedownPosture.SetOutlineColor(GetDocument.GetOutlineColor());

            FrontBodyPosition FrontBodyPositionKneedown = new FrontBodyPosition();
            //FrontBodyPositionKneedown = m_BalanceLabData.m_FrontBodyPositionKneedown;

            FrontBodyPositionKneedown.SetAllPositionDetected();
            GetDocument.GetKneedownFrontBodyPosition(ref GetDocument.m_FrontBodyPositionKneedown); // newly added by meena for beta testing
            m_wndKneedownPosture.SetFrontBodyPosition(GetDocument.m_FrontBodyPositionKneedown);
            //GetDocument.SetKneedownFrontBodyPosition(GetDocument.m_FrontBodyPositionKneedown);

            GetDocument.CalcBodyAngleKneedown(ref FrontBodyAngleKneedown);
            m_wndKneedownPosture.SetFrontBodyAngle(FrontBodyAngleKneedown);

            GetDocument.CalcBodyAngleSide(ref m_SideBodyAngle);

            FrontBodyResultData FrontBodyResultDataKneedown = new FrontBodyResultData();
            GetDocument.CalcKneedownFrontBodyResultData(ref FrontBodyResultDataKneedown);
            m_wndKneedownPosture.SetFrontBodyResultData(FrontBodyResultDataKneedown);
            m_wndKneedownPosture.SetBenchmark1Point(ptBenchmark1);
            m_wndKneedownPosture.SetBenchmark2Point(ptBenchmark2);
            m_wndKneedownPosture.SetBenchmarkDistance(iBenchmarkDistance);

            string strID = string.Empty;
            IDC_ID.Font = new Font("MS UI Gothic", 10, FontStyle.Regular);
            IDC_ID.Text = GetDocument.GetDataID().ToString();             // ID

            IDC_Name.Font = new Font("MS UI Gothic", 10, FontStyle.Regular);
            IDC_Name.Text = GetDocument.GetDataName();
            // –¼‘O

            IDC_Gender.Font = new Font("MS UI Gothic", 10, FontStyle.Regular);
            string str = "-";
            if (GetDocument.GetDataGender() == 1)
            {
                str = Yugamiru.Properties.Resources.MALE;
                if (str == "Male")
                    str = "M";
                else if (str == "男")
                    str = "男";
                else if (str == "남성")
                    str = "남성";
            }

            if (GetDocument.GetDataGender() == 2)
            {
                str = Yugamiru.Properties.Resources.FEMALE;
                if (str == "Female")
                    str = "F";
                else if (str == "女")
                    str = "女";
                else if (str == "여성")
                    str = "여성";
            }
            IDC_Gender.Text = str;   // «•Ê

            string sYear = string.Empty, sMonth = string.Empty, sDay = string.Empty;
            GetDocument.GetDataDoB(ref sYear, ref sMonth, ref sDay);
            IDC_DoB.Font = new Font("MS UI Gothic", 12, FontStyle.Regular);
            str = sMonth + "." + sDay + " " + sYear;
            IDC_DoB.Text = str;                               // ¶”NŒŽ“ú

            if (GetDocument.GetDataHeight() == 0)
                str = "-";  // g’·
            else
                str = GetDocument.GetDataHeight().ToString();
            IDC_Height.Font = new Font("MS UI Gothic", 12, FontStyle.Regular);
            IDC_Height.Text = str;                            // g’·					

            string strInstitutionName = string.Empty;
            GetDocument.GetInstitutionName(strInstitutionName);
            //IDC_InstitutionName, strInstitutionName);    // Ž{Ý–¼

            //((CEdit*)GetDlgItem(IDC_CommentField))->SetLimitText(300);

            string strComment = GetDocument.GetDataComment();
            if (strComment.Length == 0)
            {
                string strFixedComment = string.Empty;
                GetDocument.GetFixedComment(ref strFixedComment);
                IDC_CommentField.Text = strFixedComment;  // ’èŒ`•¶
            }
            else
            {
                IDC_CommentField.Text = strComment;   // ƒRƒƒ“ƒg—“
            }

            // ƒGƒfƒBƒbƒgƒ{ƒbƒNƒX‚ÍƒtƒHƒ“ƒg‚ðÝ’è‚µ‚È‚¨‚·.
            {
                /*     CWnd* pWnd = GetDlgItem(IDC_CommentField);
                     if (pWnd != NULL)
                     {
                         pWnd->SetFont(&m_FontEditBox);
                     }*/
            }

            // ”»’è•¶
            string strJudge = string.Empty;
            if (m_BalanceLabData.IsDetected(FrontBodyPositionStanding, FrontBodyPositionKneedown))
            {
                FrontBodyResultData temp_FrontBodyResultDataStanding = new FrontBodyResultData();
                FrontBodyResultData temp_FrontBodyResultDataKneedown = new FrontBodyResultData();
                SideBodyResultData temp_SideBodyResultData = new SideBodyResultData();
                GetDocument.CalcStandingFrontBodyResultData(ref FrontBodyResultDataStanding);
                GetDocument.CalcKneedownFrontBodyResultData(ref FrontBodyResultDataKneedown);
                GetDocument.CalcSideBodyResultData(ref temp_SideBodyResultData);
                JudgementResult JudgementResultStanding = new JudgementResult(FrontBodyResultDataStanding, temp_SideBodyResultData);
                JudgementResult JudgementResultKneedown = new JudgementResult(FrontBodyResultDataKneedown, temp_SideBodyResultData);
                JudgementMessageInfo JudgementMessageInfoStanding = new JudgementMessageInfo(JudgementResultStanding);
                JudgementMessageInfo JudgementMessageInfoKneedown = new JudgementMessageInfo(JudgementResultKneedown);
                string str1 = string.Empty;
                string str2 = string.Empty;
                //# ifdef LANG_EN
                //JudgementMessageInfoStanding.MakeString(str1, "", GetDocument.IsScoresheetDetail());
                /*                strJudge = str1;
                #else
                #if defined(PEDX)
                        JudgementMessageInfoStanding.MakeString( str1, "StandingF", GetDocument()->IsScoresheetDetail() );
                        JudgementMessageInfoKneedown.MakeString( str2, "Walking F", GetDocument()->IsScoresheetDetail() );
                #else*/
                JudgementMessageInfoStanding.MakeString(ref str1, /*"Standing:"*/Yugamiru.Properties.Resources.KEY_STAND, GetDocument.IsScoresheetDetail());
                JudgementMessageInfoKneedown.MakeString(ref str2, /*"Bending:"*/Yugamiru.Properties.Resources.KEY_BEND, GetDocument.IsScoresheetDetail());
                strJudge = str1 + "\r\n\r\n" + str2;
                /*
#endif
                strJudge = str1 + "\r\n\r\n" + str2;
#endif*/
            }
            else
            {
                strJudge = Yugamiru.Properties.Resources.MUSCLEREPORT17; //"Without specification of the joints' positions, posture evaluation is not available.";
            }
            IDC_EDIT1.Font = new Font("MS UI Gothic", 12, FontStyle.Regular);

            IDC_EDIT1.Text = strJudge;

            int iBackgroundWidth = m_wndStandingPosture.GetBackgroundWidth();
            int iBackgroundHeight = m_wndStandingPosture.GetBackgroundHeight();
            int iSrcWidth = (int)(iBackgroundWidth / GetDocument.GetImgMag());
            int iSrcHeight = (int)(iBackgroundHeight / GetDocument.GetImgMag());
            int iSrcX = iBackgroundWidth / 2 - iSrcWidth / 2;
            int iSrcY = iBackgroundHeight / 2 - iSrcHeight / 2;
            m_wndStandingPosture.SetSrcPos(iSrcX, iSrcY);
            m_wndStandingPosture.SetSrcSize(iSrcWidth, iSrcHeight);
            m_wndKneedownPosture.SetSrcPos(iSrcX, iSrcY);
            m_wndKneedownPosture.SetSrcSize(iSrcWidth, iSrcHeight);
            m_wndStandingPosture.SetArrowLength(GetDocument.GetArrowLength());
            m_wndStandingPosture.SetArrowWidth(GetDocument.GetArrowWidth());
            m_wndStandingPosture.SetArrowInvisible(GetDocument.IsArrowInvisible());
            m_wndStandingPosture.SetLabelInvisible(GetDocument.IsLabelInvisible());
            //m_wndStandingPosture.UpdateOffscreen(m_pDCOffscreen);
            m_wndKneedownPosture.SetArrowLength(GetDocument.GetArrowLength());
            m_wndKneedownPosture.SetArrowWidth(GetDocument.GetArrowWidth());
            m_wndKneedownPosture.SetArrowInvisible(GetDocument.IsArrowInvisible());
            m_wndKneedownPosture.SetLabelInvisible(GetDocument.IsLabelInvisible());
            //m_wndKneedownPosture.UpdateOffscreen();

            // ƒXƒ‰ƒCƒ_[
            /*  CSliderCtrl* pSB1 = (CSliderCtrl*)GetDlgItem(IDC_SLIDER1);
              pSB1->SetRange(int(IMG_SCALE_MIN * 100), int(IMG_SCALE_MAX * 100));
              //	pSB1->SetRange(int(IMG_SCALE_MAX*100),int(IMG_SCALE_MIN*100));
              pSB1->SetPos(int(IMG_SCALE_MAX * 100 + IMG_SCALE_MIN * 100 - GetDocument()->GetImgMag() * 100));
              pSB1->SetTic(50);
              */

            IDC_SLIDER1.SetRange((int)(Constants.IMG_SCALE_MIN * 100), (int)(Constants.IMG_SCALE_MAX * 100));
            IDC_SLIDER1.TickFrequency = 50;
            IDC_SLIDER1.Value = (int)(Constants.IMG_SCALE_MIN * 100);/*(int)(Constants.IMG_SCALE_MAX * 100 +
                Constants.IMG_SCALE_MIN * 100 - GetDocument.GetImgMag() * 100);*/


            m_wndStandingPosture.m_pbyteBits = GetDocument.m_FrontStandingImageBytes;
            m_wndStandingPosture.m_iOffscreenWidth = 304;
            m_wndStandingPosture.m_iOffscreenHeight = 380;
            m_wndStandingPosture.CreateFinalImage(ref m_StandingImage);

            m_wndKneedownPosture.m_pbyteBits = GetDocument.m_FrontKneedownImageBytes;
            m_wndKneedownPosture.m_iOffscreenWidth = 304;
            m_wndKneedownPosture.m_iOffscreenHeight = 380;
            m_wndKneedownPosture.CreateFinalImage(ref m_KneedownImage);

            m_JointEditDoc = GetDocument;


        }
        Bitmap m_StandingImage = null;
        Bitmap m_KneedownImage = null;

        public void ResultView_SizeChanged(object sender, EventArgs e)
        {
            //--to centre the picture box while resizing the form
            m_MainPic.Left = (this.ClientSize.Width - m_MainPic.Width) / 2;
            m_MainPic.Top = (this.ClientSize.Height - m_MainPic.Height) / 2;

            pictureBox_Standing.Size = new Size(304, 380);
            pictureBox_Standing.Location = new Point(290, 85 + m_MainPic.Top);
            pictureBox_Standing.Left = m_MainPic.Left + 90;

            pictureBox_KneeDown.Size = new Size(304, 380);
            pictureBox_KneeDown.Location = new Point(690, 85 + m_MainPic.Top);
            pictureBox_KneeDown.Left = pictureBox_Standing.Left + pictureBox_Standing.Width + 90;

            IDC_BTN_NAMECHANGE.Size = new Size(112, 42);
            IDC_BTN_NAMECHANGE.Location = new Point(170, 630 + m_MainPic.Top);
            IDC_BTN_NAMECHANGE.Left = m_MainPic.Left + 30;

            IDC_RemeasurementBtn.Size = new Size(112, 42); //uÄ‘ª’èvƒ{ƒ^ƒ“.
            IDC_RemeasurementBtn.Location = new Point(290, 630 + m_MainPic.Top);
            IDC_RemeasurementBtn.Left = IDC_BTN_NAMECHANGE.Left + IDC_BTN_NAMECHANGE.Width + 2;

            IDC_EditBtn.Size = new Size(112, 42);//uŠÖßˆÊ’uvƒ{ƒ^ƒ“.
            IDC_EditBtn.Location = new Point(410, 630 + m_MainPic.Top);
            IDC_EditBtn.Left = IDC_RemeasurementBtn.Left + IDC_RemeasurementBtn.Width + 2;

            IDC_ScoresheetBtn.Size = new Size(112, 42); //uƒŒƒ|[ƒgvƒ{ƒ^ƒ“.
            IDC_ScoresheetBtn.Location = new Point(530, 630 + m_MainPic.Top);
            IDC_ScoresheetBtn.Left = IDC_EditBtn.Left + IDC_EditBtn.Width + 2;

            IDC_PrintBtn.Size = new Size(112, 42); //uˆóüvƒ{ƒ^ƒ“. 
            IDC_PrintBtn.Location = new Point(650, 630 + m_MainPic.Top); //uˆóüvƒ{ƒ^ƒ“. 
            IDC_PrintBtn.Left = IDC_ScoresheetBtn.Left + IDC_ScoresheetBtn.Width + 2;

            IDC_BTN_DATASAVE.Size = new Size(112, 42);
            IDC_BTN_DATASAVE.Location = new Point(770, 630 + m_MainPic.Top);
            IDC_BTN_DATASAVE.Left = IDC_PrintBtn.Left + IDC_PrintBtn.Width + 2;

            IDC_MeasurementEndBtn.Size = new Size(112, 42); //u–ß‚évƒ{ƒ^ƒ“i‘ª’èI—¹j.
            IDC_MeasurementEndBtn.Location = new Point(890, 630 + m_MainPic.Top);
            IDC_MeasurementEndBtn.Left = IDC_BTN_DATASAVE.Left + IDC_BTN_DATASAVE.Width + 2;

            IDC_BTN_RETURNTOPMENU.Size = new Size(112, 42);
            IDC_BTN_RETURNTOPMENU.Location = new Point(1010, 630 + m_MainPic.Top);
            IDC_BTN_RETURNTOPMENU.Left = IDC_MeasurementEndBtn.Left + IDC_MeasurementEndBtn.Width + 2;
            IDC_CommentField.Size = new Size(346, 116 + 6);
            IDC_CommentField.Location = new Point(264, 502 + m_MainPic.Top);
            IDC_CommentField.Left = pictureBox_Standing.Left - 26;

            IDC_EDIT1.Size = new Size(468 + 6, 116 + 6);
            IDC_EDIT1.Location = new Point(628, 502 + m_MainPic.Top);
            IDC_EDIT1.Left = IDC_CommentField.Left + IDC_CommentField.Width + 20;

            IDC_Mag1Btn.Size = new Size(48, 48);
            IDC_Mag1Btn.Location = new Point(1110, 280 + m_MainPic.Top);
            IDC_Mag1Btn.Left = IDC_EDIT1.Left + IDC_EDIT1.Width + 10;

            panel1.Size = new Size(42, 192);
            panel1.Location = new Point(1110, 330 + m_MainPic.Top);
            panel1.Left = IDC_EDIT1.Left + IDC_EDIT1.Width + 10;

            IDC_SLIDER1.Size = new Size(38, 192);
            //IDC_SLIDER1.Location = new Point(1115, 250 - 50);

            IDC_Mag2Btn.Size = new Size(48, 48);
            IDC_Mag2Btn.Location = new Point(1110, 530 + m_MainPic.Top);
            IDC_Mag2Btn.Left = IDC_EDIT1.Left + IDC_EDIT1.Width + 10;

            IDC_ResetImgBtn.Size = new Size(48, 48);
            IDC_ResetImgBtn.Location = new Point(1110, 580 + m_MainPic.Top);
            IDC_ResetImgBtn.Left = IDC_EDIT1.Left + IDC_EDIT1.Width + 10;


            IDC_ID.Size = new Size(99 - 6, 26);
            IDC_ID.Location = new Point(270, 20 + m_MainPic.Top);

            IDC_Name.Size = new Size(163 - 6, 26);
            IDC_Name.Location = new Point(380, 20 + m_MainPic.Top);

            IDC_Gender.Size = new Size(6, 26);
            IDC_Gender.Location = new Point(530, 20 + m_MainPic.Top);

            IDC_DoB.Size = new Size(119, 26);
            IDC_DoB.Location = new Point(620, 20 + m_MainPic.Top);

            IDC_Height.Size = new Size(38, 26);
            IDC_Height.Location = new Point(720, 20 + m_MainPic.Top);

            IDC_ID.Left = (this.Width) / 2 - 420;//267,1356,61
            IDC_Name.Left = IDC_ID.Left + 108;
            IDC_Gender.Left = IDC_Name.Left + 178;
            IDC_DoB.Left = IDC_Gender.Left + 56 + 4;
            IDC_Height.Left = IDC_DoB.Left + 106;


        }

        private void pictureBox_Standing_Paint(object sender, PaintEventArgs e)
        {
            /* m_wndStandingPosture.m_pbyteBits = m_JointEditDoc.m_FrontStandingImageBytes;
             m_wndStandingPosture.m_iOffscreenWidth = 304;
             m_wndStandingPosture.m_iOffscreenHeight = 380;*/
            Image<Bgr, byte> test = new Image<Bgr, byte>(m_StandingImage);
            m_wndStandingPosture.m_pbyteBits = test.Bytes;
            m_wndStandingPosture.UpdateOffscreen(e.Graphics);
        }

        private void pictureBox_KneeDown_Paint(object sender, PaintEventArgs e)
        {
            /* m_wndKneedownPosture.m_pbyteBits = m_JointEditDoc.m_FrontKneedownImageBytes;
             m_wndKneedownPosture.m_iOffscreenWidth = 304;
             m_wndKneedownPosture.m_iOffscreenHeight = 380;*/
            Image<Bgr, byte> test = new Image<Bgr, byte>(m_KneedownImage);
            m_wndKneedownPosture.m_pbyteBits = test.Bytes;
            m_wndKneedownPosture.UpdateOffscreen(e.Graphics);
        }
        public void UpdatePictureScale(int iSliderPos)
        {
            int iSrcX = m_wndStandingPosture.GetSrcX();
            int iSrcY = m_wndStandingPosture.GetSrcY();
            int iSrcWidth = m_wndStandingPosture.GetSrcWidth();
            int iSrcHeight = m_wndStandingPosture.GetSrcHeight();

            int iSrcCenterX = iSrcX + iSrcWidth / 2;
            int iSrcCenterY = iSrcY + iSrcHeight / 2;

            /*CRect rcClient;
            m_wndStandingPosture.GetClientRect(&rcClient);*/

            //int Val = 400 - (iSliderPos - 100);
            double dImgMag = /*(Constants.IMG_SCALE_MAX + Constants.IMG_SCALE_MIN) - Val / 100.0;*/iSliderPos / 100.0;
            if (dImgMag < Constants.IMG_SCALE_MIN)
            {
                dImgMag = Constants.IMG_SCALE_MIN;
            }
            else if (dImgMag > Constants.IMG_SCALE_MAX)
            {
                dImgMag = Constants.IMG_SCALE_MAX;
            }
            int iNewSrcWidth = (int)(m_wndStandingPosture.GetBackgroundWidth() / dImgMag);
            int iNewSrcHeight = (int)(m_wndStandingPosture.GetBackgroundHeight() / dImgMag);

            int iNewSrcX = iSrcCenterX - iNewSrcWidth / 2;
            int iNewSrcY = iSrcCenterY - iNewSrcHeight / 2;
            if (iNewSrcX < 0)
            {
                iNewSrcX = 0;
            }
            if (iNewSrcY < 0)
            {
                iNewSrcY = 0;
            }
            if (iNewSrcX + iNewSrcWidth >= m_wndStandingPosture.GetBackgroundWidth())
            {
                iNewSrcX = m_wndStandingPosture.GetBackgroundWidth() - 1 - iNewSrcWidth;
            }
            if (iNewSrcY + iNewSrcHeight >= m_wndStandingPosture.GetBackgroundHeight())
            {
                iNewSrcY = m_wndStandingPosture.GetBackgroundHeight() - 1 - iNewSrcHeight;
            }
            m_wndStandingPosture.SetSrcPos(iNewSrcX, iNewSrcY);
            m_wndStandingPosture.SetSrcSize(iNewSrcWidth, iNewSrcHeight);
            m_wndKneedownPosture.SetSrcPos(iNewSrcX, iNewSrcY);
            m_wndKneedownPosture.SetSrcSize(iNewSrcWidth, iNewSrcHeight);


            pictureBox_Standing.Invalidate();
            pictureBox_KneeDown.Invalidate();
        }


        private void IDC_SLIDER1_Scroll(object sender, EventArgs e)
        {

            int scale = IDC_SLIDER1.Value;
            UpdatePictureScale(scale);

        }

        private void IDC_Mag1Btn_Click(object sender, EventArgs e)
        {

            m_ZoomFlag = Constants.ZOOMOUT;
            timer1.Interval = 100;
            timer1.Start();
            IDC_Mag1Btn.Image = Yugamiru.Properties.Resources.mag2_down;

            int scale = IDC_SLIDER1.Value;
            if (scale >= 400)
                return;
            if ((scale + 50) > 400)
                IDC_SLIDER1.Value = 400;
            else
                IDC_SLIDER1.Value = scale + 50;
            UpdatePictureScale(scale + 50);

        }

        private void IDC_Mag2Btn_Click(object sender, EventArgs e)
        {

            m_ZoomFlag = Constants.ZOOMIN;
            timer1.Interval = 100;
            timer1.Start();
            IDC_Mag2Btn.Image = Yugamiru.Properties.Resources.mag1_down;

            int scale = IDC_SLIDER1.Value;
            //TRACE2("scale now %d updated %d",scale,scale-50);
            if (scale <= 100)
                return;
            if ((scale - 50) < 100)
                IDC_SLIDER1.Value = 100;
            else
                IDC_SLIDER1.Value = (scale - 50);
            UpdatePictureScale(scale - 50);

        }

        private void IDC_ResetImgBtn_Click(object sender, EventArgs e)
        {

            m_ZoomFlag = Constants.RESET;
            timer1.Interval = 100;
            timer1.Start();
            IDC_ResetImgBtn.Image = Yugamiru.Properties.Resources.mag3_down;

            int iBackgroundWidth = m_wndStandingPosture.GetBackgroundWidth();
            int iBackgroundHeight = m_wndStandingPosture.GetBackgroundHeight();
            int iSrcWidth = (int)(iBackgroundWidth / m_JointEditDoc.GetImgMag());
            int iSrcHeight = (int)(iBackgroundHeight / m_JointEditDoc.GetImgMag());
            int iSrcX = iBackgroundWidth / 2 - iSrcWidth / 2;
            int iSrcY = iBackgroundHeight / 2 - iSrcHeight / 2;
            m_wndStandingPosture.SetSrcPos(iSrcX, iSrcY);
            m_wndStandingPosture.SetSrcSize(iSrcWidth, iSrcHeight);
            m_wndKneedownPosture.SetSrcPos(iSrcX, iSrcY);
            m_wndKneedownPosture.SetSrcSize(iSrcWidth, iSrcHeight);
            // m_wndStandingPosture.UpdateOffscreen();
            // m_wndKneedownPosture.UpdateOffscreen();

            double dImgMag = (double)iBackgroundWidth / iSrcWidth;

            UpdatePictureScale((int)/*(Constants.IMG_SCALE_MAX * 100 + Constants.IMG_SCALE_MIN * 100 -*/ dImgMag * 100);

            IDC_SLIDER1.SetRange((int)(Constants.IMG_SCALE_MIN * 100), (int)(Constants.IMG_SCALE_MAX * 100));
            //IDC_SLIDER1.Value = (int)(Constants.IMG_SCALE_MAX * 100 + Constants.IMG_SCALE_MIN * 100 - dImgMag * 100);
            IDC_SLIDER1.Value = (int)dImgMag * 100;


        }

        private void pictureBox_Standing_MouseMove(object sender, MouseEventArgs e)
        {

            m_wndStandingPosture.OnMouseMove(e);
            m_wndKneedownPosture.OnMouseMove(e);
            pictureBox_Standing.Invalidate();
            pictureBox_KneeDown.Invalidate();



        }

        private void pictureBox_Standing_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                m_wndStandingPosture.OnRButtonUp(e);
                m_wndKneedownPosture.OnRButtonUp(e);
            }
            pictureBox_Standing.Invalidate();
            pictureBox_KneeDown.Invalidate();
        }

        private void pictureBox_Standing_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                m_wndStandingPosture.OnRButtonDown(e);
                m_wndKneedownPosture.OnRButtonDown(e);
            }
            pictureBox_Standing.Invalidate();
            pictureBox_KneeDown.Invalidate();

        }

        private void pictureBox_KneeDown_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                m_wndStandingPosture.OnRButtonUp(e);
                m_wndKneedownPosture.OnRButtonUp(e);
            }
            pictureBox_Standing.Invalidate();
            pictureBox_KneeDown.Invalidate();

        }

        private void pictureBox_KneeDown_MouseMove(object sender, MouseEventArgs e)
        {

            m_wndStandingPosture.OnMouseMove(e);
            m_wndKneedownPosture.OnMouseMove(e);
            pictureBox_Standing.Invalidate();
            pictureBox_KneeDown.Invalidate();


        }

        private void pictureBox_KneeDown_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                m_wndStandingPosture.OnRButtonDown(e);
                m_wndKneedownPosture.OnRButtonDown(e);
            }
            pictureBox_Standing.Invalidate();
            pictureBox_KneeDown.Invalidate();

        }

        private void IDC_BTN_RETURNTOPMENU_Click(object sender, EventArgs e)
        {
            IDC_BTN_NAMECHANGE.Image = Yugamiru.Properties.Resources.namechange_up;
            IDC_BTN_RETURNTOPMENU.Image = Yugamiru.Properties.Resources.returnstart_on;
            IDC_RemeasurementBtn.Image = Yugamiru.Properties.Resources.imagechange_up;
            IDC_EditBtn.Image = Yugamiru.Properties.Resources.jointedit_up;


            IDC_PrintBtn.Image = Yugamiru.Properties.Resources.reportprint_up;
            IDC_ScoresheetBtn.Image = Yugamiru.Properties.Resources.reportdisplay_up;
            IDC_BTN_DATASAVE.Image = Yugamiru.Properties.Resources.datasave_up;
            IDC_MeasurementEndBtn.Image = Yugamiru.Properties.Resources.startred_up;

            if (!CheckForDataAlreadyExistInDatabase())
            {
                DialogResult result = MessageBox.Show(
                    Yugamiru.Properties.Resources.WARNING1/*"if return to title view, current data will be lost. return to title view OK?"*/,
                    "JointEdit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.No)
                {
                    return;
                }
            }
            m_JointEditDoc.SetInputMode(Constants.INPUTMODE_NEW);
            m_JointEditDoc.SetFinalScreenMode(Constants.FINAL_SCREEN_MODE_NONE);
            m_JointEditDoc.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_NONE);
            m_JointEditDoc.SetDataMeasurementTime("");
            m_JointEditDoc.SetDataID("");
            m_JointEditDoc.SetDataName("");
            m_JointEditDoc.SetDataGender(0);
            m_JointEditDoc.SetDataHeight(0);



            m_JointEditDoc.m_SideImageBytes = null;
            m_JointEditDoc.m_FrontStandingImageBytes = null;
            m_JointEditDoc.m_FrontKneedownImageBytes = null;



            //FunctiontoInitialScreen(EventArgs.Empty);
            this.Close();
            DisposeControls();
            m_JointEditDoc.GetInitialScreen().Visible = true;
            m_JointEditDoc.GetInitialScreen().RefreshForms();
            this.ImportYGAFileFlag = false;
        }
        public event EventHandler EventToInitialScreen;
        public void FunctiontoInitialScreen(EventArgs e)
        {
            EventHandler eventHandler = EventToInitialScreen;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }

        }


        private void IDC_EditBtn_Click(object sender, EventArgs e) // check position button
        {


            IDC_BTN_NAMECHANGE.Image = Yugamiru.Properties.Resources.namechange_up;
            IDC_BTN_RETURNTOPMENU.Image = Yugamiru.Properties.Resources.returnstart_up;
            IDC_RemeasurementBtn.Image = Yugamiru.Properties.Resources.imagechange_up;
            IDC_EditBtn.Image = Yugamiru.Properties.Resources.jointedit_on;


            IDC_PrintBtn.Image = Yugamiru.Properties.Resources.reportprint_up;
            IDC_ScoresheetBtn.Image = Yugamiru.Properties.Resources.reportdisplay_up;
            IDC_BTN_DATASAVE.Image = Yugamiru.Properties.Resources.datasave_up;
            IDC_MeasurementEndBtn.Image = Yugamiru.Properties.Resources.startred_up;

            m_JointEditDoc.SetSaveFilePath("");
            m_JointEditDoc.SetInputMode(Constants.INPUTMODE_MODIFY);
            m_JointEditDoc.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_ANKLE_AND_HIP);
            //m_JointEditDoc.ChangeToJointEditView();
            //FunctiontoCheckPosition(EventArgs.Empty);
            this.Close();
            DisposeControls();
            m_JointEditDoc.GetJointEditView().Visible = true;
            m_JointEditDoc.GetJointEditView().RefreshForm();
        }
        public event EventHandler EventToCheckPosition; // creating event handler - step1
        public void FunctiontoCheckPosition(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToCheckPosition;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }
        public event EventHandler EventToEditID; // creating event handler - step1
        public void FunctiontoEditID(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToEditID;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDC_BTN_NAMECHANGE_Click(object sender, EventArgs e)
        {

            IDC_BTN_NAMECHANGE.Image = Yugamiru.Properties.Resources.namechange_on;
            IDC_BTN_RETURNTOPMENU.Image = Yugamiru.Properties.Resources.returnstart_up;
            IDC_RemeasurementBtn.Image = Yugamiru.Properties.Resources.imagechange_up;
            IDC_EditBtn.Image = Yugamiru.Properties.Resources.jointedit_up;


            IDC_PrintBtn.Image = Yugamiru.Properties.Resources.reportprint_up;
            IDC_ScoresheetBtn.Image = Yugamiru.Properties.Resources.reportdisplay_up;
            IDC_BTN_DATASAVE.Image = Yugamiru.Properties.Resources.datasave_up;
            IDC_MeasurementEndBtn.Image = Yugamiru.Properties.Resources.startred_up;


            m_JointEditDoc.SetSaveFilePath("");
            m_JointEditDoc.SetInputMode(Constants.INPUTMODE_MODIFY);
            m_JointEditDoc.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_RETAIN);
            // FunctiontoEditID(EventArgs.Empty);

            this.Close();
            DisposeControls();
            m_JointEditDoc.GetMeasurementDlg().Visible = true;
            m_JointEditDoc.GetMeasurementDlg().RefreshForm();
            m_JointEditDoc.GetMeasurementDlg().Reload();


        }

        private void IDC_RemeasurementBtn_Click(object sender, EventArgs e)
        {


            IDC_BTN_NAMECHANGE.Image = Yugamiru.Properties.Resources.namechange_up;
            IDC_BTN_RETURNTOPMENU.Image = Yugamiru.Properties.Resources.returnstart_up;
            IDC_RemeasurementBtn.Image = Yugamiru.Properties.Resources.imagechange_on;
            IDC_EditBtn.Image = Yugamiru.Properties.Resources.jointedit_up;


            IDC_PrintBtn.Image = Yugamiru.Properties.Resources.reportprint_up;
            IDC_ScoresheetBtn.Image = Yugamiru.Properties.Resources.reportdisplay_up;
            IDC_BTN_DATASAVE.Image = Yugamiru.Properties.Resources.datasave_up;
            IDC_MeasurementEndBtn.Image = Yugamiru.Properties.Resources.startred_up;


            m_JointEditDoc.SetSaveFilePath("");
            m_JointEditDoc.SetInputMode(Constants.INPUTMODE_MODIFY);
            m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING);
            //FunctiontoChange(EventArgs.Empty);
            //GetDocument.ChangeToMeasurementStartView();
            this.Close();
            DisposeControls();
            m_JointEditDoc.GetMeasurementStart().Visible = true;
            m_JointEditDoc.GetMeasurementStart().RefreshForm();
        }
        public event EventHandler EventToChange; // creating event handler - step1
        public void FunctiontoChange(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToChange;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDC_MeasurementEndBtn_Click(object sender, EventArgs e)// Restart Button
        {
            IDC_BTN_NAMECHANGE.Image = Yugamiru.Properties.Resources.namechange_up;
            IDC_BTN_RETURNTOPMENU.Image = Yugamiru.Properties.Resources.returnstart_up;
            IDC_RemeasurementBtn.Image = Yugamiru.Properties.Resources.imagechange_up;
            IDC_EditBtn.Image = Yugamiru.Properties.Resources.jointedit_up;

            IDC_PrintBtn.Image = Yugamiru.Properties.Resources.reportprint_up;
            IDC_ScoresheetBtn.Image = Yugamiru.Properties.Resources.reportdisplay_up;
            IDC_BTN_DATASAVE.Image = Yugamiru.Properties.Resources.datasave_up;
            IDC_MeasurementEndBtn.Image = Yugamiru.Properties.Resources.startred_on;
            //MessageBox.Show(m_JointEditDoc.GetDataMeasurementTime());
            if (!CheckForDataAlreadyExistInDatabase())
            {
                DialogResult result = MessageBox.Show(
                    Yugamiru.Properties.Resources.WARNING3/*"if start new measurement, current data will be lost. start new measurement OK?"*/,
                    "JointEdit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.No)
                {
                    return;
                }
            }

            m_JointEditDoc.m_SideImageBytes = null;
            m_JointEditDoc.m_FrontStandingImageBytes = null;
            m_JointEditDoc.m_FrontKneedownImageBytes = null;

            m_JointEditDoc.SetInputMode(Constants.INPUTMODE_NEW);
            m_JointEditDoc.SetFinalScreenMode(Constants.FINAL_SCREEN_MODE_NONE);
            m_JointEditDoc.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_INITIALIZE);
            m_JointEditDoc.SetDataMeasurementTime("");
            m_JointEditDoc.SetDataID("");
            m_JointEditDoc.SetDataName("");
            m_JointEditDoc.SetDataGender(0);
            m_JointEditDoc.SetDataHeight(0);
            //FunctiontoRestart(EventArgs.Empty);
            this.Close();
            this.DisposeControls();
            m_JointEditDoc.GetMeasurementDlg().Visible = true;
            m_JointEditDoc.GetMeasurementDlg().RefreshForm();
            m_JointEditDoc.GetMeasurementDlg().Reload();
            this.ImportYGAFileFlag = false;

        }
        public event EventHandler EventToRestart; // creating event handler - step1
        public void FunctiontoRestart(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToRestart;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDC_ScoresheetBtn_Click(object sender, EventArgs e)
        {
            IDC_BTN_NAMECHANGE.Image = Yugamiru.Properties.Resources.namechange_up;
            IDC_BTN_RETURNTOPMENU.Image = Yugamiru.Properties.Resources.returnstart_up;
            IDC_RemeasurementBtn.Image = Yugamiru.Properties.Resources.imagechange_up;
            IDC_EditBtn.Image = Yugamiru.Properties.Resources.jointedit_up;
            IDC_PrintBtn.Image = Yugamiru.Properties.Resources.reportprint_up;
            IDC_ScoresheetBtn.Image = Yugamiru.Properties.Resources.reportdisplay_on;
            IDC_BTN_DATASAVE.Image = Yugamiru.Properties.Resources.datasave_up;
            IDC_MeasurementEndBtn.Image = Yugamiru.Properties.Resources.startred_up;

            FromPage = 0; ToPage = 0;


            NextPageNum = 1;
            printDocument_temp = new PrintDocument();
            using (printPreviewDialog = new PrintPreviewDialog())
            {
                //Set the size, location, and name.
                printPreviewDialog.ClientSize = new Size(ClientSize.Width, ClientSize.Height);

                printPreviewDialog.Location = new Point(0, 0);
                printPreviewDialog.Name = "Yugamiru";
                //printPreviewDialog.PrintPreviewControl.Zoom = 0.75;
                string strComment;
                strComment = IDC_CommentField.Text;
                m_JointEditDoc.SetDataComment(strComment);

                // Associate the event-handling method with the 
                // document's PrintPage event.
                printPreviewDialog.UseAntiAlias = true;
                printPreviewDialog.AutoSizeMode = AutoSizeMode.GrowOnly;
                //printPreviewDialog.PrintPreviewControl.Columns = 2;


                ((ToolStripButton)((ToolStrip)printPreviewDialog.Controls[1]).Items[9]).Visible = false;


                Type T = typeof(PrintPreviewDialog);
                FieldInfo fi = T.GetField("toolStrip1", BindingFlags.Instance |
                BindingFlags.NonPublic);

                ToolStrip toolStrip1 = (ToolStrip)fi.GetValue(printPreviewDialog);
                ToolStripButton myTestButton;
                myTestButton = new ToolStripButton();

                myTestButton.ToolTipText = "Page 1";
                myTestButton.Text = "Page 1";
                myTestButton.Click += new EventHandler(Btn_Click);
                toolStrip1.Items.Add(myTestButton);


                ToolStripButton myTestButton2;
                myTestButton2 = new ToolStripButton();
                myTestButton2.ToolTipText = "Page 2";
                myTestButton2.Text = "Page 2";
                myTestButton2.Click += new EventHandler(Btn_Click2);
                toolStrip1.Items.Add(myTestButton2);

                ToolStripButton CloseButton;
                CloseButton = new ToolStripButton();
                CloseButton.ToolTipText = "Close";
                CloseButton.Text = "Close";
                CloseButton.Click += new EventHandler(CloseBtn_Click);
                toolStrip1.Items.Add(CloseButton);


                printDocument_temp.DocumentName = "Yugamiru";
                printDocument_temp.DefaultPageSettings.Landscape = true;
                printPreviewDialog.Document = printDocument_temp;

                printPreviewDialog.PrintPreviewControl.Click += PrintPreviewDialog_Click;
                printPreviewDialog.PrintPreviewControl.MouseMove += PrintPreviewControl_MouseMove;
                printDocument_temp.BeginPrint += PrintDocument_temp_BeginPrint;
                printDocument_temp.PrintPage += document_PrintPage;

                //printPreviewDialog.PrintPreviewControl.Zoom = .83;
                printPreviewDialog.PrintPreviewControl.StartPage = 0;
                printPreviewDialog.Icon = Yugamiru.Properties.Resources.yugamiru_multi_icon;
                printPreviewDialog.ShowDialog();

                //MessageBox.Show("hi");

                printPreviewDialog.Close();
                if (printDocument_temp != null)
                    printDocument_temp.Dispose();

                if (printPreviewDialog != null)
                    printPreviewDialog.Dispose();

            }
            //Added By Suhana For GSP 871
            #region POP-UP for Subscribe
            string comp_key = "";
            string license_key = "";

            string computerID = WebComCation.keyRequest.GetComputerID();
            QlmLicenseLib.QlmLicense qls = new QlmLicenseLib.QlmLicense();
            qls.DefineProduct(WebComCation.ProductInfo.ProductID, WebComCation.ProductInfo.ProductName, WebComCation.ProductInfo.ProductVersionMajor
               , WebComCation.ProductInfo.ProductVersionMinor, WebComCation.ProductInfo.ProductEncryptionKey, WebComCation.ProductInfo.ProductPersistencyKey);
            qls.PublicKey = WebComCation.ProductInfo.ProductEncryptionKey;

            qls.ReadKeys(ref license_key, ref comp_key);

            qls.ValidateLicenseEx(comp_key, WebComCation.keyRequest.GetComputerID());
            qls.GetStatus();
            qlsGetDate = qls.ExpiryDate;
            int dRenewDate = ValidateDates();
            int ReadRenewDates = dRenewDate;
            if (ReadRenewDates <= 30)
            {
                if (qls.DaysLeft <= 15)
                {
                    DialogResult dialogResult = MessageBox.Show(
                       /*"if return to title view, current data will be lost. return to title view OK?"*/
                       "Thanks, for using our trial version,do you wish to subscribe?", "Yugamiru", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        GetDefaultBrowserPath();

                        ProcessStartInfo sInfo = new ProcessStartInfo(@"https://yugamiru.net");
                        Process.Start(sInfo);
                    }

                }
            }
        }
        #endregion POP-UP for Subscribe
        //Added By Suhana For GSP 871
        public int ValidateDates()
        {
            string renewDate = WebComCation.LicenseValidator.showRenewDate;
            if (renewDate != null)
            {
                DateTime ddRenew = DateTime.Parse(renewDate).Date;
                DateTime GetQLSExpiryDays = qlsGetDate;
                TimeSpan leftTotalDays = (GetQLSExpiryDays - ddRenew);
                int getD = (int)leftTotalDays.TotalDays;



                return getD;
            }
            else
            {

                string showActivationDate = WebComCation.Utility.GetSavedStamp();
                if (showActivationDate == "")
                {
                    return 0;
                }
                else
                {
                    DateTime ddRenew = DateTime.Parse(showActivationDate).Date;
                    DateTime GetQLSExpiryDays = qlsGetDate;
                    TimeSpan leftTotalDays = (GetQLSExpiryDays - ddRenew);
                    int getD = (int)leftTotalDays.TotalDays;

                    return getD;
                }
            }
        }
        //Added By Suhana For GSP 868 and GSP 869
        //Added By Suhana For GSP 871
        #region DefaultbrowserCode
        public static string GetDefaultBrowserPath()
        {
            string urlAssociation = @"Software\Microsoft\Windows\Shell\Associations\UrlAssociations\http";
            string browserPathKey = @"$BROWSER$\shell\open\command";

            Microsoft.Win32.RegistryKey userChoiceKey = null;
            string browserPath = "";

            try
            {
                //Read default browser path from userChoiceLKey
                userChoiceKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(urlAssociation + @"\UserChoice", false);

                //If user choice was not found, try machine default
                if (userChoiceKey == null)
                {
                    //Read default browser path from Win XP registry key
                    var browserKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(@"HTTP\shell\open\command", false);

                    //If browser path wasn’t found, try Win Vista (and newer) registry key
                    if (browserKey == null)
                    {
                        browserKey =
                        Microsoft.Win32.Registry.CurrentUser.OpenSubKey(
                        urlAssociation, false);
                    }
                    var path = CleanifyBrowserPath(browserKey.GetValue(null) as string);
                    browserKey.Close();
                    return path.ToString();
                }
                else
                {
                    // user defined browser choice was found
                    string progId = (userChoiceKey.GetValue("ProgId").ToString());
                    userChoiceKey.Close();

                    // now look up the path of the executable
                    string concreteBrowserKey = browserPathKey.Replace("$BROWSER$", progId);
                    var kp = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(concreteBrowserKey, false);
                    browserPath = CleanifyBrowserPath(kp.GetValue(null) as string).ToString();
                    kp.Close();
                    return browserPath;
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
      
        private static object CleanifyBrowserPath(string v)
        {
            throw new NotImplementedException();
        }
        #endregion
        //Added By Suhana For GSP 871
        void CloseBtn_Click(object sender, EventArgs e)
        {
            printPreviewDialog.Close();
            if (printDocument_temp != null)
                printDocument_temp.Dispose();

            if (printPreviewDialog != null)
                printPreviewDialog.Dispose();
        }

        private void PrintDocument_temp_BeginPrint(object sender, PrintEventArgs e)
        {
            NextPageNum = 1;

        }

        private void PrintPreviewControl_MouseMove(object sender, MouseEventArgs e)
        {

            Bitmap bmp = new Bitmap(m_CL_Path + @"\cursor_icon.png");
            bmp.MakeTransparent(Color.White);
            if (printPreviewDialog.PrintPreviewControl.Cursor == Cursors.Default && m_ZoomCounter != 2)
                printPreviewDialog.PrintPreviewControl.Cursor = new Cursor(bmp.GetHicon());

            if (m_ZoomCounter == 2)
                printPreviewDialog.PrintPreviewControl.Cursor = Cursors.Default;
        }



        private void PrintPreviewDialog_Click(object sender, EventArgs e)
        {
            switch (m_ZoomCounter)
            {
                case 0:
                    this.printPreviewDialog.PrintPreviewControl.Zoom = 1;
                    m_ZoomCounter++;
                    break;
                case 1:
                    this.printPreviewDialog.PrintPreviewControl.Zoom = 1.15;
                    m_ZoomCounter = 2;
                    break;
                case 2:
                    this.printPreviewDialog.PrintPreviewControl.Zoom = 0.75;
                    m_ZoomCounter = 0;
                    break;
                default:
                    break;

            }

        }

        private void Btn_Click(object sender, EventArgs e)
        {
            printPreviewDialog.PrintPreviewControl.StartPage = 0;
        }
        private void Btn_Click2(object sender, EventArgs e)
        {
            printPreviewDialog.PrintPreviewControl.StartPage = 1;
        }
        Rectangle rect2;

        private void document_PrintPage(object sender, PrintPageEventArgs e)
        {
            if (NextPageNum == 1)
            {
                PrinterBounds objBounds = new PrinterBounds(e);
                rect2 = objBounds.Bounds;  // Get the REAL Margin Bounds !
                if (!m_PrintingStatus)
                {
                    if (printDocument_temp.PrinterSettings.PrinterName != "Microsoft Print to PDF")
                    {
                        rect2.X = rect2.X + 74;
                        rect2.Y = rect2.Y + 74;
                    }
                }
                else
                    m_PrintingStatus = false;


                e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                e.Graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            }

            switch (NextPageNum)
            {
                case 1:

                    using (Image Imgpage1 = PrintPage1())
                    {
                        using (Image<Bgr, byte> emgucvImage = new Image<Bgr, byte>(new Bitmap(Imgpage1)))
                        {
                            using (Image<Bgr, byte> emgucvImage1 = emgucvImage.Resize(emgucvImage.Width - 1, emgucvImage.Height, Emgu.CV.CvEnum.Inter.Linear))
                            {
                                DrawUsingStretchdibits(e.Graphics, rect2, emgucvImage1);
                            }
                        }
                    }
                    break;

                case 2:

                    using (Image Imgpage2 = PrintPage2())
                    {
                        using (Image<Bgr, byte> emgucvImage = new Image<Bgr, byte>(new Bitmap(Imgpage2)))
                        {
                            using (Image<Bgr, byte> emgucvImage2 = emgucvImage.Resize(emgucvImage.Width - 1, emgucvImage.Height, Emgu.CV.CvEnum.Inter.Linear))
                            {
                                DrawUsingStretchdibits(e.Graphics, rect2, emgucvImage2);
                            }
                        }
                    }

                    break;
                default:
                    break;
            }

            // Next time print the next page.
            NextPageNum += 1;

            // We have more pages if wee have not yet printed page 3.
            if (NextPageNum <= 2 && m_JointEditDoc.GetDisplayMuscleReport() == 1)
            {
                if (ToPage == 1)
                    e.HasMorePages = false;
                else
                {
                    e.HasMorePages = true;//(NextPageNum <= 2);
                    return;
                }
            }
            else
            {
                e.HasMorePages = false;

            }

        }
        //Bitmap m_dibPage1 = null, m_dibPage2 = null;
        CDatabase DB = new CDatabase();
        Image PrintPage1()
        {
            Image m_dibPage1;


            if (m_JointEditDoc.GetDisplayScore() == 1)
            {
                m_dibPage1 = Image.FromFile(m_CL_Path + @"\postureReport_bg.bmp");
            }
            else
                m_dibPage1 = Image.FromFile(m_CL_Path + @"\postureReport_bg2.bmp");

            using (Graphics g = Graphics.FromImage(m_dibPage1))
            {
                DrawComment(g, 113, 2504, 113 + 1122, 2504 + 289);
                // ‘ª’è“ú‚Ì•\Ž¦.
                DrawMeasurementTime(g, 273, 422, 273 + 446, 422 + 48);
                DrawUserID(g, 816, 422, 816 + 362, 422 + 48);
                // Ž–¼‚Ì•\Ž¦.
                DrawUserName(g, 273, 523, 273 + 446, 523 + 48);
                // «•Ê‚Ì•\Ž¦.
                DrawUserGender(g, 816, 523, 816 + 96, 523 + 48);
                // g’·‚Ì•\Ž¦.
                DrawUserHeight(g, 1057, 523, 1057 + 121, 523 + 48);
                // Žp¨ƒpƒ^[ƒ“‚Ì•\Ž¦.
                DrawPosturePatternPicture(g, 1962, 2002);
                if (m_JointEditDoc.GetDisplayScore() == 1)
                {
                    // ‚ä‚ª‚Ý[‚éŽw”‚Ì•\Ž¦.
                    DrawYugamiPointPicture(g, 298, 390, 482, 824);
                }
                if (m_JointEditDoc.GetDisplayScore() == 1)
                {
                    // ‚ä‚ª‚Ý[‚éƒ‰ƒ“ƒN‚Ì•\Ž¦.
                    DrawYugamiruPointRankPicture(g, 1065, 824);
                }
                else
                {
                    // ƒXƒRƒA‚Ì•ª‚¾‚¯¶‚É‚¸‚ç‚·.
                    DrawYugamiruPointRankPicture(g, 702, 824);
                }
                // ‚ä‚ª‚Ý[‚éƒ‰ƒ“ƒNà–¾‚Ì•\Ž¦.
                DrawYugamiruPointRankCommentPicture(g, 277, 981);
                // ƒŒ[ƒ_[ƒ`ƒƒ[ƒg‚Ì•\Ž¦.
                DrawRaderChart(g, 88, 1507);
                // —§ˆÊ‰æ‘œ‚Ì•\Ž¦.
                DrawStandingImage(g, 1375, 374, 856, 1375);
                // ‹üˆÊ‰æ‘œ‚Ì•\Ž¦.
                DrawKneedownImage(g, 2264, 374, 856, 1375);
                // ‘¤–Ê‰æ‘œ‚Ì•\Ž¦.
                DrawSideImage(g, 3144, 374, 856, 1375);
            }
            return m_dibPage1;
        }
        Image PrintPage2()
        {

            Image m_dibPage2 = Image.FromFile(m_CL_Path + @"\muscleReport_bg.bmp");
            using (Graphics g = Graphics.FromImage(m_dibPage2))
            {
                // ‘ª’è“ú‚Ì•\Ž¦.
                DrawMeasurementTime(g, 273, 422, 273 + 446, 422 + 48);
                // ƒ†[ƒU[‚h‚c‚Ì•\Ž¦.
                DrawUserID(g, 816, 422, 816 + 362, 422 + 48);
                // Ž–¼‚Ì•\Ž¦.
                DrawUserName(g, 273, 523, 273 + 446, 523 + 48);
                // «•Ê‚Ì•\Ž¦.
                DrawUserGender(g, 816, 523, 816 + 96, 523 + 48);
                // g’·‚Ì•\Ž¦.
                DrawUserHeight(g, 1057, 523, 1057 + 121, 523 + 48);
                // ‹Ø“÷ƒRƒƒ“ƒg‚Ì•\Ž¦.
                DrawMuscleCommentPicture(g, 298, 816);
                // „§ƒgƒŒ[ƒjƒ“ƒO‚Ì•\Ž¦.
                if (m_JointEditDoc.GetJudgeMode() == 1)
                {
                    int[] aiNewTrainingID = new int[4];
                    m_JointEditDoc.GetNewTrainingIDs(ref aiNewTrainingID);
                    DrawNewTrainingPicture(g, 1422, 1966, aiNewTrainingID[0]);
                    DrawNewTrainingPicture(g, 2725, 1966, aiNewTrainingID[1]);
                    DrawNewTrainingPicture(g, 1422, 2416, aiNewTrainingID[2]);
                    DrawNewTrainingPicture(g, 2725, 2416, aiNewTrainingID[3]);
                }
                else
                {
                    DrawTrainingPicture(g, 1415, 1925);
                }
                // —§ˆÊ‹Ø“÷‰æ‘œ‚Ì•\Ž¦.
                DrawStandingMusclePicture(g, 1379, 378);
                // ‹üˆÊ‹Ø“÷‰æ‘œ‚Ì•\Ž¦.
                DrawKneedownMusclePicture(g, 2670, 378);
            }
            return m_dibPage2;
        }
        void DrawKneedownMusclePicture(Graphics e, int x, int y)
        {
            // Image ImageFilePath = new Bitmap(m_CL_Path + @"\k_Body.bmp");

            //e.Graphics.DrawImage(ImageFilePath, new Point(x, y));

            BackgroundBodyBitmapFileImage BackgroundBodyBitmapFileImage = new BackgroundBodyBitmapFileImage();
            using (var ImageFilePath = new Bitmap(m_CL_Path + @"\k_Body.bmp"))
            {
                BackgroundBodyBitmapFileImage.Load(ImageFilePath);

            }


            /*  using (Graphics g = Graphics.FromImage(ImageFilePath))
              {
                  g.DrawImageUnscaled(Yugamiru.Properties.Resources.r_02l, new Point(0, 0));
              }*/


            MuscleBitmapFileImage MuscleBitmapFileImageBlue = new MuscleBitmapFileImage();
            if (!MuscleBitmapFileImageBlue.Create(BackgroundBodyBitmapFileImage.GetWidth(), BackgroundBodyBitmapFileImage.GetHeight()))
            {
                return;
            }
            MuscleBitmapFileImage MuscleBitmapFileImageRed = new MuscleBitmapFileImage();
            if (!MuscleBitmapFileImageRed.Create(BackgroundBodyBitmapFileImage.GetWidth(), BackgroundBodyBitmapFileImage.GetHeight()))
            {
                return;
            }
            MuscleBitmapFileImage MuscleBitmapFileImageYellow = new MuscleBitmapFileImage();
            if (!MuscleBitmapFileImageYellow.Create(BackgroundBodyBitmapFileImage.GetWidth(), BackgroundBodyBitmapFileImage.GetHeight()))
            {
                return;
            }
            MuscleColorInfo MuscleColorInfo = new MuscleColorInfo();
            m_JointEditDoc.CalcKneedownMuscleColor(MuscleColorInfo);

            int i = 0;
            for (i = 0; i < Constants.MUSCLEID_MAX; i++)
            {
                if (MuscleColorInfo.GetMuscleColor(i) != Constants.MUSCLECOLORID_NONE)
                {
                    using (Image MuscleImageFilePath = GetKneedownMuscleBMPFileNameByID(i))
                    {
                        MuscleBitmapFileImage MuscleBitmapFileImageNew = new MuscleBitmapFileImage();
                        MuscleBitmapFileImageNew.Load(MuscleImageFilePath);

                        switch (MuscleColorInfo.GetMuscleColor(i))
                        {
                            case Constants.MUSCLECOLORID_YELLOW:
                                MuscleBitmapFileImageYellow.Merge(MuscleBitmapFileImageNew);
                                break;
                            case Constants.MUSCLECOLORID_RED:
                                MuscleBitmapFileImageRed.Merge(MuscleBitmapFileImageNew);
                                break;
                            case Constants.MUSCLECOLORID_BLUE:
                                MuscleBitmapFileImageBlue.Merge(MuscleBitmapFileImageNew);
                                break;
                            default:
                                break;
                        }
                    }

                }
            }

            BackgroundBodyBitmapFileImage.ApplyMuscleBitmapFileImage(MuscleBitmapFileImageYellow, 255, 255, 0, 255, 255, 0);
            BackgroundBodyBitmapFileImage.ApplyMuscleBitmapFileImage(MuscleBitmapFileImageRed, 192, 0, 13, 235, 191, 215);
            BackgroundBodyBitmapFileImage.ApplyMuscleBitmapFileImage(MuscleBitmapFileImageBlue, 0, 0, 255, 178, 178, 255);
            BackgroundBodyBitmapFileImage.Draw(e, x, y);
            //m_JointEditDoc.SetKneedownMusclePicture(img);
        }
        void DrawStandingMusclePicture(Graphics e, int x, int y)
        {
            //e.Graphics.DrawImage(ImageFilePath, new Point(x, y));

            BackgroundBodyBitmapFileImage BackgroundBodyBitmapFileImage = new BackgroundBodyBitmapFileImage();
            using (var ImageFilePath = new Bitmap(m_CL_Path + @"\r_Body.bmp"))
            {
                BackgroundBodyBitmapFileImage.Load(ImageFilePath);
            }

            /*  using (Graphics g = Graphics.FromImage(ImageFilePath))
              {
                  g.DrawImageUnscaled(Yugamiru.Properties.Resources.r_02l, new Point(0, 0));
              }*/


            MuscleBitmapFileImage MuscleBitmapFileImageBlue = new MuscleBitmapFileImage();
            if (!MuscleBitmapFileImageBlue.Create(BackgroundBodyBitmapFileImage.GetWidth(), BackgroundBodyBitmapFileImage.GetHeight()))
            {
                return;
            }
            MuscleBitmapFileImage MuscleBitmapFileImageRed = new MuscleBitmapFileImage();
            if (!MuscleBitmapFileImageRed.Create(BackgroundBodyBitmapFileImage.GetWidth(), BackgroundBodyBitmapFileImage.GetHeight()))
            {
                return;
            }
            MuscleBitmapFileImage MuscleBitmapFileImageYellow = new MuscleBitmapFileImage();
            if (!MuscleBitmapFileImageYellow.Create(BackgroundBodyBitmapFileImage.GetWidth(), BackgroundBodyBitmapFileImage.GetHeight()))
            {
                return;
            }
            MuscleColorInfo MuscleColorInfo = new MuscleColorInfo();
            m_JointEditDoc.CalcStandingMuscleColor(MuscleColorInfo);

            int i = 0;
            //MessageBox.Show("step 1");
            for (i = 0; i < Constants.MUSCLEID_MAX; i++)
            {
                if (MuscleColorInfo.GetMuscleColor(i) != Constants.MUSCLECOLORID_NONE)
                {
                    using (Image MuscleImageFilePath = GetStandingMuscleBMPFileNameByID(i))
                    {
                        MuscleBitmapFileImage MuscleBitmapFileImageNew = new MuscleBitmapFileImage();
                        MuscleBitmapFileImageNew.Load(MuscleImageFilePath);
                        /*using (Graphics g = Graphics.FromImage(ImageFilePath))
                        {
                            g.DrawImageUnscaled(MuscleImageFilePath, new Point(0, 0));
                        }*/

                        switch (MuscleColorInfo.GetMuscleColor(i))
                        {
                            case Constants.MUSCLECOLORID_YELLOW:
                                MuscleBitmapFileImageYellow.Merge(MuscleBitmapFileImageNew);
                                break;
                            case Constants.MUSCLECOLORID_RED:
                                MuscleBitmapFileImageRed.Merge(MuscleBitmapFileImageNew);
                                break;
                            case Constants.MUSCLECOLORID_BLUE:
                                MuscleBitmapFileImageBlue.Merge(MuscleBitmapFileImageNew);
                                break;
                            default:
                                break;
                        }
                    }

                }
            }
            //MessageBox.Show("step 2");
            BackgroundBodyBitmapFileImage.ApplyMuscleBitmapFileImage(MuscleBitmapFileImageYellow, 255, 255, 0, 255, 255, 0);
            BackgroundBodyBitmapFileImage.ApplyMuscleBitmapFileImage(MuscleBitmapFileImageRed, 192, 0, 13, 235, 191, 215);
            BackgroundBodyBitmapFileImage.ApplyMuscleBitmapFileImage(MuscleBitmapFileImageBlue, 0, 0, 255, 178, 178, 255);
            BackgroundBodyBitmapFileImage.Draw(e, x, y);
            //m_JointEditDoc.SetStandingMusclePicture(img);
        }

        void DrawTrainingPicture(Graphics e, int x, int y)
        {
            int iPosturePatternID = m_JointEditDoc.CalcPosturePatternID();
            Bitmap ImageFileName = null;
            switch (iPosturePatternID)
            {
                case Constants.POSTUREPATTERNID_STOOP_AND_BEND_BACKWARD:  /* ”L”w{”½‚è˜ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\training_nekoSorigosi.bmp");
                    break;
                case Constants.POSTUREPATTERNID_STOOP:                    /* ”L”w */
                    ImageFileName = new Bitmap(m_CL_Path + @"\training_nekoze.bmp");
                    break;
                case Constants.POSTUREPATTERNID_BEND_BACKWARD:            /* ”½‚è˜ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\training_sorigosi_flatback.bmp");
                    break;
                case Constants.POSTUREPATTERNID_FLATBACK:                 /* ƒtƒ‰ƒbƒgƒoƒbƒN */
                    ImageFileName = new Bitmap(m_CL_Path + @"\training_sorigosi_flatback.bmp");
                    break;
                case Constants.POSTUREPATTERNID_NORMAL:                   /* —‘z‚ÌŽp¨ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\training_standard.bmp");
                    break;
                case Constants.POSTUREPATTERNID_FRONTSIDE_UNBALANCED: /* ³–Êƒoƒ‰ƒ“ƒX”ñ‘ÎÌ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\training_standard.bmp"); // ‰¼
                    break;
                default:
                    break;
            }
            if (ImageFileName != null)
            {
                ImageFileName.MakeTransparent(Color.White);
                e.InterpolationMode = InterpolationMode.HighQualityBicubic;
                e.SmoothingMode = SmoothingMode.HighQuality;
                using (ImageFileName)
                {
                    e.DrawImage(ImageFileName, x, y, 700, 210);

                }
                ImageFileName.Dispose();
                ImageFileName = null;
            }

        }
        void DrawNewTrainingPicture(Graphics e, int x, int y, int iTrainingID)
        {
            Bitmap ImageFileName = null;
            switch (iTrainingID)
            {
                case 1:
                    ImageFileName = new Bitmap(m_CL_Path + @"\u-1.bmp");
                    break;
                case 2:
                    ImageFileName = new Bitmap(m_CL_Path + @"\u-2.bmp");
                    break;
                case 3:
                    ImageFileName = new Bitmap(m_CL_Path + @"\t-3.bmp");
                    break;
                case 4:
                    ImageFileName = new Bitmap(m_CL_Path + @"\t-4.bmp");
                    break;
                case 5:
                    ImageFileName = new Bitmap(m_CL_Path + @"\t-5.bmp");
                    break;
                case 6:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-6.bmp");
                    break;
                case 7:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-7.bmp");
                    break;
                case 8:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-8.bmp");
                    break;
                case 9:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-9.bmp");
                    break;
                case 10:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-10.bmp");
                    break;
                case 11:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-11.bmp");
                    break;
                case 12:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-12.bmp");
                    break;
                case 13:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-13.bmp");
                    break;
                case 14:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-14.bmp");
                    break;
                case 15:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-15.bmp");
                    break;
                case 16:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-16.bmp");
                    break;
                case 17:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-17.bmp");
                    break;
                case 18:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-18.bmp");
                    break;
                case 19:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-19.bmp");
                    break;
                case 20:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-20.bmp");
                    break;
                case 21:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-21.bmp");
                    break;
                case 22:
                    ImageFileName = new Bitmap(m_CL_Path + @"\l-22.bmp");
                    break;
                default:
                    break;
            }

            if (ImageFileName != null)
            {
                ImageFileName.MakeTransparent(Color.White);
                e.InterpolationMode = InterpolationMode.HighQualityBicubic;
                e.SmoothingMode = SmoothingMode.HighQuality;
                using (ImageFileName)
                {
                    e.DrawImage(ImageFileName, x, y, 1300, 400);
                }
                ImageFileName.Dispose();
                ImageFileName = null;
            }



        }
        void DrawMuscleCommentPicture(Graphics e, int x, int y)
        {
            int iPosturePatternID = m_JointEditDoc.CalcPosturePatternID();
            Bitmap ImageFileName = null;
            switch (iPosturePatternID)
            {
                case Constants.POSTUREPATTERNID_STOOP_AND_BEND_BACKWARD:  /* ”L”w{”½‚è˜ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\muscleInfo_nekoSorigosi.bmp");
                    break;
                case Constants.POSTUREPATTERNID_STOOP:                    /* ”L”w */
                    ImageFileName = new Bitmap(m_CL_Path + @"\muscleInfo_nekoze.bmp");
                    break;
                case Constants.POSTUREPATTERNID_BEND_BACKWARD:            /* ”½‚è˜ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\muscleInfo_sorigosi.bmp");
                    break;
                case Constants.POSTUREPATTERNID_FLATBACK:                 /* ƒtƒ‰ƒbƒgƒoƒbƒN */
                    ImageFileName = new Bitmap(m_CL_Path + @"\muscleInfo_flatback.bmp");
                    break;
                case Constants.POSTUREPATTERNID_NORMAL:                   /* —‘z‚ÌŽp¨ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\muscleInfo_standard.bmp");
                    break;
                case Constants.POSTUREPATTERNID_FRONTSIDE_UNBALANCED: /* ³–Êƒoƒ‰ƒ“ƒX”ñ‘ÎÌ */
                    ImageFileName = new Bitmap(m_CL_Path + @"\muscleInfo_standard.bmp"); // ‰¼
                    break;
                default:
                    break;
            }
            ImageFileName.MakeTransparent(Color.White);
            if (ImageFileName != null)
            {
                using (ImageFileName)
                {
                    e.DrawImage(ImageFileName, x, y, 960, 1600);//new Rectangle(x, y, width, height), 0, 0, ImageFileName.Width, ImageFileName.Height - 20, GraphicsUnit.Pixel);
                }
                ImageFileName.Dispose();
                ImageFileName = null;
            }

        }
        void DrawUserHeight(Graphics e, int left, int top, int right, int bottom)
        {
            Rectangle RectforUserHeight = new Rectangle(left, top, right, bottom);
            e.DrawString(IDC_Height.Text, printFont, Brushes.Black, RectforUserHeight);
        }
        void DrawUserGender(Graphics e, int left, int top, int right, int bottom)
        {

            Rectangle RectforUserGender = new Rectangle(left, top, right, bottom);
            e.DrawString(IDC_Gender.Text, printFont, Brushes.Black, RectforUserGender);

        }
        void DrawMeasurementTime(Graphics e, int left, int top, int right, int bottom)
        {
            string strYear = string.Empty, strMonth = string.Empty, strDay = string.Empty, strTime = string.Empty;
            string MeasurementTime = m_JointEditDoc.GetDataMeasurementTime();
            if (MeasurementTime != "")
            {
                m_JointEditDoc.GetDataAcqDate(ref strYear, ref strMonth, ref strDay, ref strTime);
            }
            else
            {
                //	strYear		= time.Format(" % y");
                strYear = DateTime.Now.ToString("yy");
                //		strMonth	= time.Format("%m");

                strMonth = DateTime.Now.ToString("MM");
                strDay = DateTime.Now.ToString("dd");
                strTime = DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString();
            }
            string strTmp;
            if (m_JointEditDoc.GetLanguage() == "English" || m_JointEditDoc.GetLanguage() == "Thai")
                strTmp = strMonth + ".  " + strDay + " " + strYear;
            else
            {
                strTmp = strYear + Yugamiru.Properties.Resources.YY + strMonth + Yugamiru.Properties.Resources.MM + strDay + Yugamiru.Properties.Resources.DD;
            }

            Rectangle RectforMeasurementTime = new Rectangle(left, top, right, bottom);
            e.DrawString(strTmp, printFont, Brushes.Black, RectforMeasurementTime);

        }
        void DrawUserName(Graphics e, int left, int top, int right, int bottom)
        {
            Font SpecialFont =
               new Font("HP Simplified", 9,
               FontStyle.Regular);
            Rectangle RectforUserName = new Rectangle(left, top, right, bottom);
            e.DrawString(m_JointEditDoc.GetDataName(), SpecialFont, Brushes.Black, RectforUserName);

        }
        void DrawUserID(Graphics e, int left, int top, int right, int bottom)
        {
            Rectangle RectforUserID = new Rectangle(left, top, right, bottom);
            e.DrawString(m_JointEditDoc.GetDataID(), printFont, Brushes.Black, RectforUserID);

        }
        void DrawComment(Graphics e, int left, int top, int right, int bottom)
        {
            Font SpecialFont =
               new Font("HP Simplified", 9,
               FontStyle.Regular);

            Rectangle Rect4Comment = new Rectangle(left, top, right, bottom);
            e.DrawString(IDC_CommentField.Text, SpecialFont, Brushes.Black, Rect4Comment);

        }
        void DrawSideImage(Graphics e, int x, int y, int w, int h)
        {
            using (Image<Bgr, byte> test = new Image<Bgr, byte>(1024, 1280))
            {
                test.Bytes = m_JointEditDoc.m_SideImageBytes;
                SideBodyPosition SideBodyPosition = new SideBodyPosition();
                m_JointEditDoc.GetSideBodyPosition(ref SideBodyPosition);

                using (Bitmap test1 = test.ToBitmap())
                {


                    Point ptAnkle = new Point();
                    using (Graphics g = Graphics.FromImage(test1))
                    {
                        GlyphOverlayerToSideImage GlyphOverlayer = new GlyphOverlayerToSideImage();


                        SideBodyPosition.GetAnklePosition(ref ptAnkle);
                        int iCentroidLineXPos = SideBodyPosition.CalcCenterOfGravityX();

                        GlyphOverlayer.SetCenterLineData(
                           m_JointEditDoc.IsValidMidLine(),
                           m_JointEditDoc.GetMidLineStyle(),
                            Color.FromArgb(255, 192, 203),//m_JointEditDoc.GetMidLineColor(),
                            m_JointEditDoc.GetMidLineWidth(),
                            ptAnkle.X,
                            0,
                            /*m_JointEditDoc.GetImageHeight()*/1280);

                        GlyphOverlayer.SetCentroidLineData(
        m_JointEditDoc.IsValidCentroidLine(),
        m_JointEditDoc.GetCentroidLineStyle(),
       m_JointEditDoc.GetCentroidLineColor(),
        m_JointEditDoc.GetCentroidLineWidth(),
        iCentroidLineXPos,
        0,
        1280);

                        GlyphOverlayer.SetJointConnectionLineData(
                            m_JointEditDoc.IsValidStyleLine(),
                            m_JointEditDoc.GetStyleLineStyle(),
                            m_JointEditDoc.GetStyleLineColor(),
                            m_JointEditDoc.GetStyleLineWidth() + 8);

                        GlyphOverlayer.SetMarkerSize(m_JointEditDoc.GetMarkerSize() + 8);
                        GlyphOverlayer.SetSideBodyPosition(SideBodyPosition);
                        GlyphOverlayer.Draw(g);
                    }
                    Rectangle rcWindow = pictureBox_Standing.ClientRectangle;

                    int iNewWindowWidth = w * rcWindow.Height / h;
                    // ƒ_ƒCƒAƒƒO‚Ì•\Ž¦•‚É‚ ‚í‚¹‚ÄA“]‘—Œ³‹éŒ`‚Ì•‚ð‚ ‚í‚¹‚é.
                    int iSrcWidth2 = m_wndStandingPosture.GetSrcWidth() * iNewWindowWidth / rcWindow.Width;
                    int iSrcHeight = m_wndStandingPosture.GetSrcHeight();
                    int iSrcX2 = ptAnkle.X - iSrcWidth2 / 2;
                    int iSrcY = m_wndStandingPosture.GetSrcY();
                    if (iSrcX2 < 0)
                    {
                        iSrcX2 = 0;
                    }
                    if (iSrcX2 + iSrcWidth2 >= 1024)
                    {
                        iSrcX2 = 1024 - iSrcWidth2;
                    }
                    //using (test1)
                    {
                        e.DrawImage(test1, new Rectangle(x, y, w, h), new Rectangle(iSrcX2, iSrcY, iSrcWidth2, iSrcHeight),
                            GraphicsUnit.Pixel);//DesRe//DesRect, 130, 0, 1024 - 260, 1280, GraphicsUnit.Pixel);
                    }
                }
                SideBodyAngle tempSideBodyAngle = new SideBodyAngle();
                m_JointEditDoc.CalcBodyAngleSide(ref tempSideBodyAngle);
                SideBodyLabelString tempSideBodyLabelString = new SideBodyLabelString(ref SideBodyPosition,
                    ref tempSideBodyAngle, m_JointEditDoc.GetBenchmarkDistance(), m_JointEditDoc.GetLanguage());
                if (m_JointEditDoc.IsLabelInvisible() <= 0)
                    LabelandFont(e, tempSideBodyLabelString, x, y);
            }
        }
        public void LabelandFont(Graphics e, SideBodyLabelString m_SidetBodyLabelString, int x, int y)
        {
            Font f = new Font("MS UI Gothic", 54, FontStyle.Regular, GraphicsUnit.Pixel);

            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.EAR_POS_REPORT, f, new Point(14 + x, 6 + y));
            SampleDrawOutlineText(e, m_SidetBodyLabelString.GetEarPositionString(), f, new Point(14 + x, 32 + y + 80));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.SHOULDER_POS_REPORT, f, new Point(14 + x, 70 + y + 180));
            SampleDrawOutlineText(e, m_SidetBodyLabelString.GetShoulderPositionString(), f, new Point(14 + x, 96 + y + 260));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.HIP_POS_REPORT, f, new Point(14 + x, 141 + y + 360));
            SampleDrawOutlineText(e, m_SidetBodyLabelString.GetHipPositionString(), f, new Point(14 + x, 167 + y + 440));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.KNEE_POS_REPORT, f, new Point(14 + x, 212 + y + 540));
            SampleDrawOutlineText(e, m_SidetBodyLabelString.GetKneePositionString(), f, new Point(14 + x, 238 + y + 620));

            SizeF stringSize = new SizeF();
            stringSize = e.MeasureString(Yugamiru.Properties.Resources.SHOULDER_EAR_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.SHOULDER_EAR_ANGLE_REPORT, f, new Point(890 + x - (int)stringSize.Width, 6 + y));

            stringSize = e.MeasureString(m_SidetBodyLabelString.GetShoulderEarAngleString(), f);
            SampleDrawOutlineText(e, m_SidetBodyLabelString.GetShoulderEarAngleString(), f, new Point(880 + x - (int)stringSize.Width, 32 + y + 80));

            stringSize = e.MeasureString(Yugamiru.Properties.Resources.HIP_SHOULDER_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.HIP_SHOULDER_ANGLE_REPORT, f, new Point(890 + x - (int)stringSize.Width, 70 + y + 180));

            stringSize = e.MeasureString(m_SidetBodyLabelString.GetHipShoulderAngleString(), f);
            SampleDrawOutlineText(e, m_SidetBodyLabelString.GetHipShoulderAngleString(), f, new Point(880 + x - (int)stringSize.Width, 96 + y + 260));

            stringSize = e.MeasureString(Yugamiru.Properties.Resources.HIP_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.HIP_ANGLE_REPORT, f, new Point(890 + x - (int)stringSize.Width, 141 + y + 360));

            stringSize = e.MeasureString(m_SidetBodyLabelString.GetHipBalanceString(), f);
            SampleDrawOutlineText(e, m_SidetBodyLabelString.GetHipBalanceString(), f, new Point(880 + x - (int)stringSize.Width, 167 + y + 440));

            m_SecondPageFlag = true;
        }
        void SampleDrawOutlineText(Graphics g, String text, Font font, Point p)
        {
            // set atialiasing
            g.SmoothingMode = SmoothingMode.HighQuality;
            // make thick pen for outlining
            Pen pen = new Pen(Color.White, 6);
            // round line joins of the pen
            pen.LineJoin = LineJoin.Round;
            // create graphics path
            GraphicsPath textPath = new GraphicsPath();
            // convert string to path
            textPath.AddString(text, font.FontFamily, (int)font.Style, font.Size, p, StringFormat.GenericTypographic);
            // clone path to make outlining path
            GraphicsPath outlinePath = (GraphicsPath)textPath.Clone();
            // outline the path
            outlinePath.Widen(pen);
            // fill outline path with some color
            g.FillPath(Brushes.White, outlinePath);
            // fill original text path with some color
            g.FillPath(Brushes.Black, textPath);
        }
        public stretchDIBbits.BITMAPINFO bmi = new stretchDIBbits.BITMAPINFO();
        void SetBitmap(Image<Bgr, byte> Image)
        {
            bmi.bmiHeader.biSize = 40;//new stretchDIBbits.BITMAPINFOHEADER().biSize;
            bmi.bmiHeader.biWidth = Image.Width;//test.Width;
            bmi.bmiHeader.biHeight = -Image.Height;//-test.Height;
            bmi.bmiHeader.biPlanes = 1;
            bmi.bmiHeader.biBitCount = 24;
            //bmi.bmiHeader.biCompression = stretchDIBbits.BitmapCompressionMode.BI_RGB;
            //bmi.bmiHeader.biCompression = stretchDIBbits.BitmapCompressionMode.BI_JPEG;
            bmi.bmiHeader.biSizeImage = 0;
            bmi.bmiHeader.biXPelsPerMeter = 0;
            bmi.bmiHeader.biYPelsPerMeter = 0;
            bmi.bmiHeader.biClrUsed = 0;
            bmi.bmiHeader.biClrImportant = 0;

            bmi.bmiColors = new RGBQUAD[] { new RGBQUAD { } };
            bmi.bmiColors[0].rgbBlue = 255;
            bmi.bmiColors[0].rgbGreen = 255;
            bmi.bmiColors[0].rgbRed = 255;
            bmi.bmiColors[0].rgbReserved = 255;
        }
        void DrawUsingStretchdibits(Graphics e, Rectangle rect, Image<Bgr, byte> Image)
        {
            SetBitmap(Image);
            //Image<Bgr, byte> emgucvImage = new Image<Bgr, byte>(bmp);
            stretchDIBbits.SetStretchBltMode(e.GetHdc(), StretchBltMode.STRETCH_HALFTONE);
            e.ReleaseHdc();
            try
            {
                var t = stretchDIBbits.StretchDIBits(
                                e.GetHdc(),
                                rect.X,
                                rect.Y,
                               rect.Width,
                               rect.Height,
                                0,
                                0,
                                 Image.Width,
                                  Image.Height,
                                 Image.Bytes,
                                ref bmi,
                                Constants.DIB_RGB_COLORS,
                                Constants.SRCCOPY);

                e.ReleaseHdc();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        void DrawStandingImage(Graphics e, int x, int y, int w, int h)
        {


            using (Image<Bgr, byte> test = new Image<Bgr, byte>(1024, 1280))
            {
                test.Bytes = m_JointEditDoc.m_FrontStandingImageBytes;

                using (Bitmap test1 = test.ToBitmap())
                {
                    FrontBodyPosition FrontBodyPositionStanding = new FrontBodyPosition();
                    m_JointEditDoc.GetStandingFrontBodyPosition(ref FrontBodyPositionStanding);
                    Point ptRightAnkle = new Point();
                    Point ptLeftAnkle = new Point();
                    int iCentroidLineXPos = FrontBodyPositionStanding.CalcCenterOfGravityX();
                    using (Graphics g = Graphics.FromImage(test1))
                    {
                        GlyphOverlayerToFrontImage GlyphOverlayer = new GlyphOverlayerToFrontImage();



                        FrontBodyPositionStanding.GetRightAnklePosition(ref ptRightAnkle);
                        FrontBodyPositionStanding.GetLeftAnklePosition(ref ptLeftAnkle);

                        GlyphOverlayer.SetArrowLength(30);
                        GlyphOverlayer.SetArrowWidth(10);
                        GlyphOverlayer.SetArrowInvisible(m_JointEditDoc.IsArrowInvisible());

                        GlyphOverlayer.SetCenterLineData(
                            m_JointEditDoc.IsValidMidLine(),
                            (int)m_JointEditDoc.GetMidLineStyle(),
                            Color.FromArgb(255, 192, 203),//m_JointEditDoc.GetMidLineColor(),
                            (int)m_JointEditDoc.GetMidLineWidth(),
                            (ptRightAnkle.X + ptLeftAnkle.X) / 2,
                            0,
                            1280);//m_JointEditDoc.GetImageHeight());
                        GlyphOverlayer.SetCentroidLineData(
        m_JointEditDoc.IsValidCentroidLine(),
        m_JointEditDoc.GetCentroidLineStyle(),
        m_JointEditDoc.GetCentroidLineColor(),
        m_JointEditDoc.GetCentroidLineWidth(),
        iCentroidLineXPos,
        0,
        1280);//m_JointEditDoc.GetImageHeight());

                        GlyphOverlayer.SetJointConnectionLineData(
                            m_JointEditDoc.IsValidStyleLine(),
                            (int)m_JointEditDoc.GetStyleLineStyle(),
                            m_JointEditDoc.GetStyleLineColor(),
                            10);//(int)m_JointEditDoc.GetStyleLineWidth());

                        GlyphOverlayer.ptRightShoulder.X = FrontBodyPositionStanding.m_ptRightShoulder.X;
                        GlyphOverlayer.ptRightShoulder.Y = FrontBodyPositionStanding.m_ptRightShoulder.Y;
                        GlyphOverlayer.ptRightAnkle.X = FrontBodyPositionStanding.m_ptRightAnkle.X;
                        GlyphOverlayer.ptRightAnkle.Y = FrontBodyPositionStanding.m_ptRightAnkle.Y;
                        GlyphOverlayer.ptRightHip.X = FrontBodyPositionStanding.m_ptRightHip.X;
                        GlyphOverlayer.ptRightHip.Y = FrontBodyPositionStanding.m_ptRightHip.Y;
                        GlyphOverlayer.ptRightKnee.X = FrontBodyPositionStanding.m_ptRightKnee.X;
                        GlyphOverlayer.ptRightKnee.Y = FrontBodyPositionStanding.m_ptRightKnee.Y;

                        GlyphOverlayer.ptLeftShoulder.X = FrontBodyPositionStanding.m_ptLeftShoulder.X;
                        GlyphOverlayer.ptLeftShoulder.Y = FrontBodyPositionStanding.m_ptLeftShoulder.Y;
                        GlyphOverlayer.ptLeftAnkle.X = FrontBodyPositionStanding.m_ptLeftAnkle.X;
                        GlyphOverlayer.ptLeftAnkle.Y = FrontBodyPositionStanding.m_ptLeftAnkle.Y;
                        GlyphOverlayer.ptLeftHip.X = FrontBodyPositionStanding.m_ptLeftHip.X;
                        GlyphOverlayer.ptLeftHip.Y = FrontBodyPositionStanding.m_ptLeftHip.Y;
                        GlyphOverlayer.ptLeftKnee.X = FrontBodyPositionStanding.m_ptLeftKnee.X;
                        GlyphOverlayer.ptLeftKnee.Y = FrontBodyPositionStanding.m_ptLeftKnee.Y;


                        GlyphOverlayer.ptGlabella.X = FrontBodyPositionStanding.m_ptGlabella.X;
                        GlyphOverlayer.ptGlabella.Y = FrontBodyPositionStanding.m_ptGlabella.Y;
                        GlyphOverlayer.ptRightEar.X = FrontBodyPositionStanding.m_ptRightEar.X;
                        GlyphOverlayer.ptRightEar.Y = FrontBodyPositionStanding.m_ptRightEar.Y;
                        GlyphOverlayer.ptLeftEar.X = FrontBodyPositionStanding.m_ptLeftEar.X;
                        GlyphOverlayer.ptLeftEar.Y = FrontBodyPositionStanding.m_ptLeftEar.Y;
                        GlyphOverlayer.ptChin.X = FrontBodyPositionStanding.m_ptChin.X;
                        GlyphOverlayer.ptChin.Y = FrontBodyPositionStanding.m_ptChin.Y;


                        GlyphOverlayer.SetMarkerSize(m_JointEditDoc.GetMarkerSize());

                        GlyphOverlayer.SetFrontBodyPosition(FrontBodyPositionStanding);
                        FrontBodyResultData FrontBodyResultDataStanding = new FrontBodyResultData();
                        m_JointEditDoc.CalcStandingFrontBodyResultData(ref FrontBodyResultDataStanding);
                        GlyphOverlayer.SetFrontBodyResultData(FrontBodyResultDataStanding);
                        GlyphOverlayer.Draw(g);
                    }

                    Rectangle rcWindow = pictureBox_Standing.ClientRectangle;

                    int iNewWindowWidth = w * rcWindow.Height / h;
                    // ƒ_ƒCƒAƒƒO‚Ì•\Ž¦•‚É‚ ‚í‚¹‚ÄA“]‘—Œ³‹éŒ`‚Ì•‚ð‚ ‚í‚¹‚é.
                    int iSrcWidth2 = m_wndStandingPosture.GetSrcWidth() * iNewWindowWidth / rcWindow.Width;
                    int iSrcHeight = m_wndStandingPosture.GetSrcHeight();
                    int iSrcX2 = (ptRightAnkle.X + ptLeftAnkle.X) / 2 - iSrcWidth2 / 2;
                    int iSrcY = m_wndStandingPosture.GetSrcY();
                    if (iSrcX2 < 0)
                    {
                        iSrcX2 = 0;
                    }
                    if (iSrcX2 + iSrcWidth2 >= 1024)
                    {
                        iSrcX2 = 1024 - iSrcWidth2;
                    }
                    //using (test1)
                    {
                        e.DrawImage(test1, new Rectangle(x, y, w, h), new Rectangle(iSrcX2, iSrcY, iSrcWidth2, iSrcHeight),
                            GraphicsUnit.Pixel);//DesRect, 130, 0, 1024 - 260, 1280, GraphicsUnit.Pixel);
                    }

                    SideBodyPosition SideBodyPosition = new SideBodyPosition();
                    m_JointEditDoc.GetSideBodyPosition(ref SideBodyPosition);
                    Point ptBenchmark1 = new Point(0, 0);
                    Point ptBenchmark2 = new Point(0, 0);
                    SideBodyPosition.GetBenchmark1Position(ref ptBenchmark1);
                    SideBodyPosition.GetBenchmark2Position(ref ptBenchmark2);
                    FrontBodyAngle FrontBodyAngleStanding = new FrontBodyAngle();
                    //m_JointEditDoc.CalcBodyAngleKneedown(ref FrontBodyAngleStanding); changed by meena
                    m_JointEditDoc.CalcBodyAngleStanding(ref FrontBodyAngleStanding);

                    FrontBodyLabelString temp_FrontBodyLabelString = new FrontBodyLabelString(FrontBodyPositionStanding, FrontBodyAngleStanding,
                    ptBenchmark1, ptBenchmark2, m_JointEditDoc.GetBenchmarkDistance(), m_JointEditDoc.GetLanguage());

                    Font f = new Font("MS UI Gothic", 54, FontStyle.Regular, GraphicsUnit.Pixel);
                    if (m_JointEditDoc.IsLabelInvisible() <= 0)
                        DrawOutlineText(e, temp_FrontBodyLabelString, f, x, y);
                }
            }


        }
        void DrawOutlineText(Graphics e, FrontBodyLabelString temp_FrontBodyLabelString, Font f, int x, int y)
        {
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.BROW_POS_REPORT, f, new Point(14 + x, 6 + y));
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strGlabellaPosition, f, new Point(14 + x, 32 + y + 80));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.EAR_POS_REPORT, f, new Point(14 + x, 70 + y + 180));
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strEarCenterPosition, f, new Point(14 + x, 96 + y + 260));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.JAW_POS_REPORT, f, new Point(14 + x, 141 + y + 360));
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strChinPosition, f, new Point(14 + x, 167 + y + 440));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.SHOULDER_POS_REPORT, f, new Point(14 + x, 212 + y + 540));
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strShoulderCenterPosition, f, new Point(14 + x, 238 + y + 620));
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.HIP_POS_REPORT, f, new Point(14 + x, 283 + y + 720));
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strHipCenterPosition, f, new Point(14 + x, 309 + y + 800));

            SizeF stringSize = new SizeF();
            stringSize = e.MeasureString(Yugamiru.Properties.Resources.EAR_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.EAR_ANGLE_REPORT, f, new Point(240 + x + 600 - (int)stringSize.Width, 6 + y));

            stringSize = e.MeasureString(temp_FrontBodyLabelString.m_strEarBalance, f);
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strEarBalance, f, new Point(240 + x + 600 - (int)stringSize.Width, 32 + y + 80));


            //if(m_JointEditDoc.GetLanguage() == "English")
            stringSize = e.MeasureString(Yugamiru.Properties.Resources.SHOULDER_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.SHOULDER_ANGLE_REPORT, f, new Point(240 + x + 600 - (int)stringSize.Width, 70 + y + 180));
            //else
            //  SampleDrawOutlineText(e, Yugamiru.Properties.Resources.SHOULDER_ANGLE_REPORT, f, new Point(220 + x - 50, 70 + y));
            stringSize = e.MeasureString(temp_FrontBodyLabelString.m_strShoulderBalance, f);
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strShoulderBalance, f, new Point(240 + x + 600 - (int)stringSize.Width, 96 + y + 260));

            stringSize = e.MeasureString(Yugamiru.Properties.Resources.HIP_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.HIP_ANGLE_REPORT, f, new Point(240 + x + 600 - (int)stringSize.Width, 141 + y + 360));

            stringSize = e.MeasureString(temp_FrontBodyLabelString.m_strHipBalance, f);
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strHipBalance, f, new Point(240 + x + 600 - (int)stringSize.Width, 167 + y + 440));

            //if (m_JointEditDoc.GetLanguage() == "English")
            stringSize = e.MeasureString(Yugamiru.Properties.Resources.RIGHTKNEE_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.RIGHTKNEE_ANGLE_REPORT, f, new Point(240 + x + 600 - (int)stringSize.Width, 212 + y + 540));
            //else
            //SampleDrawOutlineText(e, Yugamiru.Properties.Resources.RIGHTKNEE_ANGLE_REPORT, f, new Point(224 + x - 50, 212 + y));
            stringSize = e.MeasureString(temp_FrontBodyLabelString.m_strRightKneeAngle, f);
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strRightKneeAngle, f, new Point(240 + x + 600 - (int)stringSize.Width, 238 + y + 620));

            //if (m_JointEditDoc.GetLanguage() == "English")
            stringSize = e.MeasureString(Yugamiru.Properties.Resources.LEFTKNEE_ANGLE_REPORT, f);
            SampleDrawOutlineText(e, Yugamiru.Properties.Resources.LEFTKNEE_ANGLE_REPORT, f, new Point(240 + x + 600 - (int)stringSize.Width, 283 + y + 720));
            //else
            //SampleDrawOutlineText(e, Yugamiru.Properties.Resources.LEFTKNEE_ANGLE_REPORT, f, new Point(224 + x - 50, 283 + y));
            stringSize = e.MeasureString(temp_FrontBodyLabelString.m_strLeftKneeAngle, f);
            SampleDrawOutlineText(e, temp_FrontBodyLabelString.m_strLeftKneeAngle, f, new Point(240 + x + 600 - (int)stringSize.Width, 309 + y + 800));


        }
        void DrawKneedownImage(Graphics e, int x, int y, int w, int h)
        {


            using (Image<Bgr, byte> test = new Image<Bgr, byte>(1024, 1280))
            {
                test.Bytes = m_JointEditDoc.m_FrontKneedownImageBytes;

                using (Bitmap test1 = test.ToBitmap())
                {
                    FrontBodyPosition FrontBodyPositionKneedown = new FrontBodyPosition();
                    m_JointEditDoc.GetKneedownFrontBodyPosition(ref FrontBodyPositionKneedown);
                    Point ptRightAnkle = new Point();
                    Point ptLeftAnkle = new Point();
                    int iCentroidLineXPos = FrontBodyPositionKneedown.CalcCenterOfGravityX();
                    using (Graphics g = Graphics.FromImage(test1))
                    {
                        GlyphOverlayerToFrontImage GlyphOverlayer = new GlyphOverlayerToFrontImage();



                        FrontBodyPositionKneedown.GetRightAnklePosition(ref ptRightAnkle);
                        FrontBodyPositionKneedown.GetLeftAnklePosition(ref ptLeftAnkle);

                        GlyphOverlayer.SetArrowLength(30);
                        GlyphOverlayer.SetArrowWidth(10);
                        GlyphOverlayer.SetArrowInvisible(m_JointEditDoc.IsArrowInvisible());

                        GlyphOverlayer.SetCenterLineData(
                            m_JointEditDoc.IsValidMidLine(),
                            m_JointEditDoc.GetMidLineStyle(),
                            Color.FromArgb(255, 192, 203),//m_JointEditDoc.GetMidLineColor(),
                            m_JointEditDoc.GetMidLineWidth(),
                            (ptRightAnkle.X + ptLeftAnkle.X) / 2,
                            0,
                            1280);//m_JointEditDoc.GetImageHeight());

                        GlyphOverlayer.SetCentroidLineData(
        m_JointEditDoc.IsValidCentroidLine(),
        m_JointEditDoc.GetCentroidLineStyle(),
        m_JointEditDoc.GetCentroidLineColor(),
        m_JointEditDoc.GetCentroidLineWidth(),
        iCentroidLineXPos,
        0,
        1280);//m_JointEditDoc.GetImageHeight());

                        GlyphOverlayer.SetJointConnectionLineData(
                            m_JointEditDoc.IsValidStyleLine(),
                            (int)m_JointEditDoc.GetStyleLineStyle(),
                            m_JointEditDoc.GetStyleLineColor(),
                            10);//(int)m_JointEditDoc.GetStyleLineWidth());

                        GlyphOverlayer.ptRightShoulder.X = FrontBodyPositionKneedown.m_ptRightShoulder.X;
                        GlyphOverlayer.ptRightShoulder.Y = FrontBodyPositionKneedown.m_ptRightShoulder.Y;
                        GlyphOverlayer.ptRightAnkle.X = FrontBodyPositionKneedown.m_ptRightAnkle.X;
                        GlyphOverlayer.ptRightAnkle.Y = FrontBodyPositionKneedown.m_ptRightAnkle.Y;
                        GlyphOverlayer.ptRightHip.X = FrontBodyPositionKneedown.m_ptRightHip.X;
                        GlyphOverlayer.ptRightHip.Y = FrontBodyPositionKneedown.m_ptRightHip.Y;
                        GlyphOverlayer.ptRightKnee.X = FrontBodyPositionKneedown.m_ptRightKnee.X;
                        GlyphOverlayer.ptRightKnee.Y = FrontBodyPositionKneedown.m_ptRightKnee.Y;

                        GlyphOverlayer.ptLeftShoulder.X = FrontBodyPositionKneedown.m_ptLeftShoulder.X;
                        GlyphOverlayer.ptLeftShoulder.Y = FrontBodyPositionKneedown.m_ptLeftShoulder.Y;
                        GlyphOverlayer.ptLeftAnkle.X = FrontBodyPositionKneedown.m_ptLeftAnkle.X;
                        GlyphOverlayer.ptLeftAnkle.Y = FrontBodyPositionKneedown.m_ptLeftAnkle.Y;
                        GlyphOverlayer.ptLeftHip.X = FrontBodyPositionKneedown.m_ptLeftHip.X;
                        GlyphOverlayer.ptLeftHip.Y = FrontBodyPositionKneedown.m_ptLeftHip.Y;
                        GlyphOverlayer.ptLeftKnee.X = FrontBodyPositionKneedown.m_ptLeftKnee.X;
                        GlyphOverlayer.ptLeftKnee.Y = FrontBodyPositionKneedown.m_ptLeftKnee.Y;


                        GlyphOverlayer.ptGlabella.X = FrontBodyPositionKneedown.m_ptGlabella.X;
                        GlyphOverlayer.ptGlabella.Y = FrontBodyPositionKneedown.m_ptGlabella.Y;
                        GlyphOverlayer.ptRightEar.X = FrontBodyPositionKneedown.m_ptRightEar.X;
                        GlyphOverlayer.ptRightEar.Y = FrontBodyPositionKneedown.m_ptRightEar.Y;
                        GlyphOverlayer.ptLeftEar.X = FrontBodyPositionKneedown.m_ptLeftEar.X;
                        GlyphOverlayer.ptLeftEar.Y = FrontBodyPositionKneedown.m_ptLeftEar.Y;
                        GlyphOverlayer.ptChin.X = FrontBodyPositionKneedown.m_ptChin.X;
                        GlyphOverlayer.ptChin.Y = FrontBodyPositionKneedown.m_ptChin.Y;


                        GlyphOverlayer.SetMarkerSize(m_JointEditDoc.GetMarkerSize());

                        GlyphOverlayer.SetFrontBodyPosition(FrontBodyPositionKneedown);
                        FrontBodyResultData FrontBodyResultDataKneedown = new FrontBodyResultData();
                        //m_JointEditDoc.CalcStandingFrontBodyResultData(ref FrontBodyResultDataKneedown);
                        m_JointEditDoc.CalcKneedownFrontBodyResultData(ref FrontBodyResultDataKneedown);
                        GlyphOverlayer.SetFrontBodyResultData(FrontBodyResultDataKneedown);
                        GlyphOverlayer.Draw(g);

                    }

                    Rectangle rcWindow = pictureBox_Standing.ClientRectangle;

                    int iNewWindowWidth = w * rcWindow.Height / h;
                    // ƒ_ƒCƒAƒƒO‚Ì•\Ž¦•‚É‚ ‚í‚¹‚ÄA“]‘—Œ³‹éŒ`‚Ì•‚ð‚ ‚í‚¹‚é.
                    int iSrcWidth2 = m_wndStandingPosture.GetSrcWidth() * iNewWindowWidth / rcWindow.Width;
                    int iSrcHeight = m_wndStandingPosture.GetSrcHeight();
                    int iSrcX2 = (ptRightAnkle.X + ptLeftAnkle.X) / 2 - iSrcWidth2 / 2;
                    int iSrcY = m_wndStandingPosture.GetSrcY();
                    if (iSrcX2 < 0)
                    {
                        iSrcX2 = 0;
                    }
                    if (iSrcX2 + iSrcWidth2 >= 1024)
                    {
                        iSrcX2 = 1024 - iSrcWidth2;
                    }
                    //using (test1)
                    {
                        e.DrawImage(test1, new Rectangle(x, y, w, h), new Rectangle(iSrcX2, iSrcY, iSrcWidth2, iSrcHeight),
                            GraphicsUnit.Pixel);//DesRect, 130, 0, 1024 - 260, 1280, GraphicsUnit.Pixel);
                    }


                    SideBodyPosition SideBodyPosition = new SideBodyPosition();
                    m_JointEditDoc.GetSideBodyPosition(ref SideBodyPosition);
                    Point ptBenchmark1 = new Point(0, 0);
                    Point ptBenchmark2 = new Point(0, 0);
                    SideBodyPosition.GetBenchmark1Position(ref ptBenchmark1);
                    SideBodyPosition.GetBenchmark2Position(ref ptBenchmark2);
                    FrontBodyAngle FrontBodyAngleKneedown = new FrontBodyAngle();
                    m_JointEditDoc.CalcBodyAngleKneedown(ref FrontBodyAngleKneedown);

                    FrontBodyLabelString temp_FrontBodyLabelString = new FrontBodyLabelString(FrontBodyPositionKneedown, FrontBodyAngleKneedown,
                    ptBenchmark1, ptBenchmark2, m_JointEditDoc.GetBenchmarkDistance(), m_JointEditDoc.GetLanguage());

                    Font f = new Font("MS UI Gothic", 54, FontStyle.Regular, GraphicsUnit.Pixel);
                    if (m_JointEditDoc.IsLabelInvisible() <= 0)
                        DrawOutlineText(e, temp_FrontBodyLabelString, f, x, y);
                }
            }
        }

        public void DrawPosturePatternPicture(Graphics e, int x, int y)
        {
            int iPosturePatternID = m_JointEditDoc.CalcPosturePatternID();
            //Bitmap strImageFileName = null;
            string Path_for_Image = string.Empty;
            switch (iPosturePatternID)
            {
                case Constants.POSTUREPATTERNID_STOOP_AND_BEND_BACKWARD:  /* ”L”w{”½‚è˜ */
                    Path_for_Image = m_CL_Path + @"\state_nekoSorigosi.bmp";
                    break;
                case Constants.POSTUREPATTERNID_STOOP:                    /* ”L”w */
                    Path_for_Image = m_CL_Path + @"\state_nekoze.bmp";
                    break;
                case Constants.POSTUREPATTERNID_BEND_BACKWARD:            /* ”½‚è˜ */
                    Path_for_Image = m_CL_Path + @"\state_sorigosi.bmp";
                    break;
                case Constants.POSTUREPATTERNID_FLATBACK:                 /* ƒtƒ‰ƒbƒgƒoƒbƒN */
                    Path_for_Image = m_CL_Path + @"\state_flatBack.bmp";
                    break;
                case Constants.POSTUREPATTERNID_NORMAL:                   /* —‘z‚ÌŽp¨ */
                    Path_for_Image = m_CL_Path + @"\state_standard.bmp";
                    break;
                case Constants.POSTUREPATTERNID_FRONTSIDE_UNBALANCED: /* ³–Êƒoƒ‰ƒ“ƒX”ñ‘ÎÌ */
                    Path_for_Image = m_CL_Path + @"\state_standard.bmp";
                    //strImageFileName = Properties.Resources.propstate_standard;    // ‰¼.
                    break;
                default:
                    break;
            }
            if (Path_for_Image != string.Empty)
            {
                using (var strImageFileName = new Bitmap(Path_for_Image))
                {
                    strImageFileName.MakeTransparent(Color.White);
                    e.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    e.DrawImage(strImageFileName, x, y, 586 + 1400, 230 + 600);//new Rectangle(x + 4, y - 6, 550 + 36, 230), 0, 0, strImageFileName.Width, strImageFileName.Height, GraphicsUnit.Pixel);
                }

            }
        }

        public void DrawYugamiPointPicture(Graphics e, int x1, int x2, int x3, int y)
        {
            e.InterpolationMode = InterpolationMode.HighQualityBicubic;
            bool bAlreadyDisplayed = false;
            string path_for_image = string.Empty;
            int iPoint = m_JointEditDoc.CalcYugamiPoint();//FrontBodyAngleStanding, FrontBodyAngleKneedown, m_SideBodyAngle);
            //iPoint = 90; // only for testing by meena
            // 100‚ÌˆÊ‚Ì•\Ž¦.
            if (iPoint >= 100)
            {

                using (var NumImage = new Bitmap(m_CL_Path + @"\num1.bmp"))
                {
                    e.DrawImage(NumImage, x1, y, 100, 100);
                    NumImage.Dispose();
                }


                iPoint = iPoint % 100;
                bAlreadyDisplayed = true;
            }
            // 10‚ÌˆÊ‚Ì•\Ž¦.
            if ((bAlreadyDisplayed) || (iPoint >= 10))
            {
                int iImageID = (iPoint / 10) % 10;

                switch (iImageID)
                {
                    case 0:
                        path_for_image = m_CL_Path + @"\num0.bmp";
                        break;
                    case 1:
                        path_for_image = m_CL_Path + @"\num1.bmp";
                        break;
                    case 2:
                        path_for_image = m_CL_Path + @"\num2.bmp";
                        break;
                    case 3:
                        path_for_image = m_CL_Path + @"\num3.bmp";
                        break;
                    case 4:
                        path_for_image = m_CL_Path + @"\num4.bmp";
                        break;
                    case 5:
                        path_for_image = m_CL_Path + @"\num5.bmp";
                        break;
                    case 6:
                        path_for_image = m_CL_Path + @"\num6.bmp";
                        break;
                    case 7:
                        path_for_image = m_CL_Path + @"\num7.bmp";
                        break;
                    case 8:
                        path_for_image = m_CL_Path + @"\num8.bmp";
                        break;
                    case 9:
                        path_for_image = m_CL_Path + @"\num9.bmp";
                        break;
                    default:
                        break;
                }
                if (path_for_image != string.Empty)
                {
                    using (var ImageFileName = new Bitmap(path_for_image))
                    {
                        e.DrawImage(ImageFileName, x2, y, 100, 100);
                    }

                    iPoint = iPoint % 10;
                    bAlreadyDisplayed = true;
                }
            }
            // 1‚ÌˆÊ‚Ì•\Ž¦.
            {
                int iImageID = iPoint % 10;
                switch (iImageID)
                {
                    case 0:
                        path_for_image = m_CL_Path + @"\num0.bmp";
                        break;
                    case 1:
                        path_for_image = m_CL_Path + @"\num1.bmp";
                        break;
                    case 2:
                        path_for_image = m_CL_Path + @"\num2.bmp";
                        break;
                    case 3:
                        path_for_image = m_CL_Path + @"\num3.bmp";
                        break;
                    case 4:
                        path_for_image = m_CL_Path + @"\num4.bmp";
                        break;
                    case 5:
                        path_for_image = m_CL_Path + @"\num5.bmp";
                        break;
                    case 6:
                        path_for_image = m_CL_Path + @"\num6.bmp";
                        break;
                    case 7:
                        path_for_image = m_CL_Path + @"\num7.bmp";
                        break;
                    case 8:
                        path_for_image = m_CL_Path + @"\num8.bmp";
                        break;
                    case 9:
                        path_for_image = m_CL_Path + @"\num9.bmp";
                        break;
                    default:
                        break;
                }
                if (path_for_image != null)
                {
                    using (var ImageFileName = new Bitmap(path_for_image))
                    {
                        e.DrawImage(ImageFileName, x3, y, 100, 100);
                    }


                    bAlreadyDisplayed = true;
                }
            }
        }

        void DrawYugamiruPointRankPicture(Graphics e, int x, int y)
        {
            e.InterpolationMode = InterpolationMode.HighQualityBicubic;
            int YugamiruPointRank = m_JointEditDoc.CalcYugamiPointRank();//FrontBodyAngleStanding, FrontBodyAngleKneedown, m_SideBodyAngle);

            string Path_for_Image = string.Empty;
            switch (YugamiruPointRank)
            {
                case Constants.YUGAMIRU_POINT_RANK_A:
                    Path_for_Image = m_CL_Path + @"\rankA.bmp";
                    break;
                case Constants.YUGAMIRU_POINT_RANK_B:
                    Path_for_Image = m_CL_Path + @"\rankB.bmp";
                    break;
                case Constants.YUGAMIRU_POINT_RANK_C:
                    Path_for_Image = m_CL_Path + @"\rankC.bmp";
                    break;
                case Constants.YUGAMIRU_POINT_RANK_D:
                    Path_for_Image = m_CL_Path + @"\rankD.bmp";
                    break;
                default:
                    break;
            }
            if (Path_for_Image != string.Empty)
            {

                using (var ImageFileName = new Bitmap(Path_for_Image))
                {
                    e.DrawImage(ImageFileName, x, y, 100, 100);

                }
            }
        }

        void DrawYugamiruPointRankCommentPicture(Graphics e, int x, int y)
        {
            int YugamiruPointRank = m_JointEditDoc.CalcYugamiPointRank();//FrontBodyAngleStanding, FrontBodyAngleKneedown, m_SideBodyAngle);
            string path_for_file = string.Empty;
            switch (YugamiruPointRank)
            {
                case Constants.YUGAMIRU_POINT_RANK_A:
                    path_for_file = m_CL_Path + @"\rankA_memo.bmp";
                    break;
                case Constants.YUGAMIRU_POINT_RANK_B:
                    path_for_file = m_CL_Path + @"\rankB_memo.bmp";
                    break;
                case Constants.YUGAMIRU_POINT_RANK_C:
                    path_for_file = m_CL_Path + @"\rankC_memo.bmp";
                    break;
                case Constants.YUGAMIRU_POINT_RANK_D:
                    path_for_file = m_CL_Path + @"\rankD_memo.bmp";
                    break;
                default:
                    break;
            }
            if (path_for_file != string.Empty)
            {

                using (var ImageFileName = new Bitmap(path_for_file))
                {
                    ImageFileName.MakeTransparent(Color.White);
                    e.DrawImage(ImageFileName, x, y, 1000, 150);
                }

            }
        }

        void DrawRaderChart(Graphics e, int x, int y)
        {

            //Image ImageFilePath = new Bitmap(m_CL_Path + @"\chart.bmp");

            //CBackgroundBodyBitmapFileImage BackgroundBodyBitmapFileImage;
            /* if (ImageFilePath == null)
             {
                 return;
             }*/
            // ‚Ü‚¸A”wŒi•`‰æ.
            //BackgroundBodyBitmapFileImage.Draw(&dc, 0, 0);            
            //e.Graphics.DrawImage(ImageFilePath, 0,400 + 50,350,250);            

            // ‚±‚±‚ÅƒOƒ‰ƒt‚ð•`‰æ.

            //•]‰¿‚Ì’iŠK
            int[] GRADE_VALUE = new int[] { 100, 80, 50, 30, 10, 5 };
            int GRADE_COUNT = GRADE_VALUE.Length / GRADE_VALUE.GetLength(0);
            const double FIT = 2.8;

            //•]‰¿‚·‚é€–Ú
            const int EST_COUNT = 11;
            //•]‰¿‚·‚é€–Ú•ª‚ÌXŽ²•ûŒü‚ÌˆÚ“®‹——£
            double[] WidthAlfa = new double[EST_COUNT];
            double[] HeightAlfa = new double[EST_COUNT];
            const double PI = 3.1415926535;
            const double offset_rad = 2 * PI / EST_COUNT;
            double rad = PI / 2;
            for (int i = 0; i < EST_COUNT; i++)
            {
                WidthAlfa[i] = Math.Cos(rad);
                HeightAlfa[i] = Math.Sin(rad);
                rad -= offset_rad;
            }
            using (var ImageFilePath = new Bitmap(m_CL_Path + @"\chart.bmp"))
            {

                //ƒŒ[ƒ_[ƒ`ƒƒ[ƒg‚Ì’†S
                Point centerP = new Point(ImageFilePath.Width / 2, ImageFilePath.Height / 2);
                using (Graphics g = Graphics.FromImage(ImageFilePath))
                {
                    MyDrawPoint(g, centerP.X, centerP.Y, 26, Color.FromArgb(255, 0, 0), true);
                    //g.DrawLine(Pens.Red, 0, 0, 600, 600);


                    int[] rst = new int[EST_COUNT];
                    Point[] pt = new Point[EST_COUNT];

                    ResultData result = new ResultData();
                    m_JointEditDoc.CalcBodyBalanceResultData(ref result);

                    rst[0] = result.GetStandingHip() + result.GetKneedownHip();
                    rst[1] = result.GetStandingHip() + result.GetKneedownHip();
                    rst[2] = result.GetStandingShoulderBal() + result.GetKneedownShoulderBal();
                    rst[3] = result.GetStandingShoulderBal() + result.GetKneedownShoulderBal();
                    rst[4] = result.GetStandingRightKnee();
                    rst[5] = result.GetKneedownRightKnee();
                    rst[6] = result.GetKneedownLeftKnee();
                    rst[7] = result.GetStandingLeftKnee();
                    rst[8] = result.GetStandingCenterBalance() + result.GetKneedownCenterBalance();
                    rst[9] = result.GetStandingEarBal() + result.GetKneedownEarBal();
                    rst[10] = result.GetStandingHeadCenter() + result.GetKneedownHeadCenter();

                    //Še“_‚ðŒvŽZ
                    for (int i = 0; i < EST_COUNT; i++)
                    {
                        //ASSERT(abs(rst[i]) < GRADE_COUNT);

                        double userVal = GRADE_VALUE[Math.Abs(rst[i])] * FIT;
                        pt[i] = new Point((int)(centerP.X + userVal * WidthAlfa[i]), (int)(centerP.Y - (userVal * HeightAlfa[i])));
                    }
                    //ü‚ðˆø‚¢‚Ä
                    for (int i = 0; i < EST_COUNT; i++)
                    {
                        // Ô‚Å•\Ž¦.
                        MyDrawLine(g, pt[i], pt[(i + 1) % EST_COUNT], 0, Color.FromArgb(255, 0, 0), 7);
                    }
                    //‚»‚Ìã‚Éƒ|ƒCƒ“ƒg
                    for (int i = 0; i < EST_COUNT; i++)
                    {
                        // Ô‚Å•\Ž¦.
                        MyDrawPoint(g, pt[i].X, pt[i].Y, 26, Color.FromArgb(255, 0, 0), true);
                    }
                }

                e.DrawImage(ImageFilePath, x, y);//new Rectangle(x, y, 350 - 18, 250 - 14), 0, 0, ImageFilePath.Width, ImageFilePath.Height, GraphicsUnit.Pixel);
            }

            /*
            if (hBitmapOld != NULL)
            {
                dc.SelectObject(hBitmapOld);
                hBitmapOld = NULL;
            }
            CDIB dibTmp;
            DWORD dwBitsSize = (bmi.bmiHeader.biWidth * 3 + 3) / 4 * 4 * bmi.bmiHeader.biHeight;
            dibTmp.LoadFromBitmapInfoHeaderAndImageBits(&(bmi.bmiHeader), (unsigned char *)pvBits, dwBitsSize );
            dibDst.Blt(x, y, dibTmp);

            if (hBitmap != NULL)
            {

        ::DeleteObject(hBitmap);
                hBitmap = NULL;
                pvBits = NULL;
            }*/
        }

        void MyDrawPoint(Graphics pDC, int x, int y, int size, Color rgb /* =RGB(0,0,0) */, bool fill /*=false*/)
        {
            Pen penNew = new Pen(rgb, 0);
            SolidBrush brNew = new SolidBrush(rgb);

            if (fill)
            {
                pDC.FillEllipse(brNew, x - size / 2, y - size / 2, size, size);
            }
            else
            {
                pDC.DrawEllipse(penNew, x - size / 2, y - size / 2, size, size);
            }


        }


        void MyDrawLine(Graphics pDC, Point st, Point end, int style /* =PS_SOLID */, Color rgb /*=RGB(0,0,0)*/, int width /*=1*/)
        {
            //	MoveToEx(hDC, st.x, st.y, NULL);
            //	LineTo(hDC, end.x, end.y);

            uint[] type = new uint[8];
            Pen penNew = new Pen(rgb, width);
            pDC.DrawLine(penNew, st, end);


        }

        private void IDC_BTN_DATASAVE_Click(object sender, EventArgs e)
        {
            m_ZoomFlag = Constants.SAVERESULT;
            timer1.Interval = 100;
            timer1.Start();
            IDC_BTN_DATASAVE.Image = Yugamiru.Properties.Resources.datasave_down;

            IDC_BTN_NAMECHANGE.Image = Yugamiru.Properties.Resources.namechange_up;
            IDC_BTN_RETURNTOPMENU.Image = Yugamiru.Properties.Resources.returnstart_up;
            IDC_RemeasurementBtn.Image = Yugamiru.Properties.Resources.imagechange_up;
            IDC_EditBtn.Image = Yugamiru.Properties.Resources.jointedit_up;

            IDC_PrintBtn.Image = Yugamiru.Properties.Resources.reportprint_up;
            IDC_ScoresheetBtn.Image = Yugamiru.Properties.Resources.reportdisplay_up;
            IDC_BTN_DATASAVE.Image = Yugamiru.Properties.Resources.datasave_on;
            IDC_MeasurementEndBtn.Image = Yugamiru.Properties.Resources.startred_up;
            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            CustomMessageBox MessageBox = new CustomMessageBox(m_JointEditDoc);

            if (ImportYGAFileFlag)
            {
                /* string YY = string.Empty; string MM = string.Empty; string DD = string.Empty;
                 m_JointEditDoc.GetDataDoB(ref YY, ref MM, ref DD);
                 DateTime DOB = new DateTime(int.Parse(YY), int.Parse(MM), int.Parse(DD));
                 string query = "select uniqueid from patientdetails where " +
                     "patientid = '" + m_JointEditDoc.GetDataID() + "' and " +
                     "name = '" + m_JointEditDoc.GetDataName() + "' and " +
                     "DOB = '" + DOB + "'";

                 dt = database.selectQuery(query);
                 if (dt.Rows.Count > 0)
                 {
                     foreach (DataRow row in dt.Rows)
                     {
                         m_JointEditDoc.SetUniqueId(int.Parse(row[0].ToString()));
                     }
                     MessageBox.ShowDialog();

                     if (m_JointEditDoc.GetUpdateFlag() == Constants.UPDATE_EXISTING_RECORD)                    
                         UpdateRecord(m_JointEditDoc.GetDataMeasurementTime());                  
                     else if (m_JointEditDoc.GetUpdateFlag() == Constants.ADD_NEW_RECORD)
                         AddRecord();
                     else if (m_JointEditDoc.GetUpdateFlag() == Constants.DO_NONE)
                         return;
                 }
                 else
                     AddRecord();*/

                AddRecord();// Fix for GSP-458

            }
            else
            {
                string strMeasurementTime = string.Empty;
                strMeasurementTime = m_JointEditDoc.GetDataMeasurementTime();
                // CTime timeCurrent = CTime::GetCurrentTime();

                if (strMeasurementTime != "")
                {

                    MessageBox.ShowDialog();

                    if (m_JointEditDoc.GetUpdateFlag() == Constants.UPDATE_EXISTING_RECORD)
                        UpdateRecord(m_JointEditDoc.GetDataMeasurementTime());
                    else if (m_JointEditDoc.GetUpdateFlag() == Constants.ADD_NEW_RECORD)
                        AddRecord();
                    else if (m_JointEditDoc.GetUpdateFlag() == Constants.DO_NONE)
                        return;
                }
                else
                {
                    AddRecord();

                }
            }
            IDC_BTN_DATASAVE.Image = Yugamiru.Properties.Resources.datasave_on;
            timer1.Stop();


        }
        public void AddRecord()
        {
            try
            {
                if (!AddValuesIntoPatientDetails())// Insert values into PatientDetails table
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                throw (ex);

            }
            AddValuesIntoFrontBodyPositionStanding(); // Insert values into FrontBodyPositionStanding Table
            AddValuesIntoFrontBodyPositionKneedown(); // Insert values into FrontBodyPositionKneedown Table
            AddValuesIntoSideBodyPosition();// Insert into SideBodyPosition Table

            //===========Added by sumit GSP-556 (cloud backup)
            try
            {
                CloudManager.StartUpdateToServer.StartSending();
            }
            catch (Exception ex)
            {
                //exception at taking cloud backup
            }
            //==========End GSP-556

            MessageBox.Show(Yugamiru.Properties.Resources.FINALSCREEN_SR2);//"Saved Successfully !!!");

        }
        private void AddValuesIntoSideBodyPosition()
        {
            SideBodyPosition SideBodyPosition = new SideBodyPosition();
            m_JointEditDoc.GetSideBodyPosition(ref SideBodyPosition);

            byte[] Compressed_bytes =
                m_JointEditDoc.Compress(m_JointEditDoc.m_SideImageBytes, false);
            string Query = "insert into SideBodyPosition values((select max(UniqueId) from PatientDetails)" +
                ",'" + SideBodyPosition.m_ptAnkle.X + "','" +
            SideBodyPosition.m_ptAnkle.Y + "','" +
            SideBodyPosition.m_ptAnkleLeftBelt.X + "','" +
            SideBodyPosition.m_ptAnkleLeftBelt.Y + "','" +
            SideBodyPosition.m_ptAnkleRightBelt.X + "','" +
            SideBodyPosition.m_ptAnkleRightBelt.Y + "','" +
            SideBodyPosition.m_ptBenchmark1.X + "','" +
            SideBodyPosition.m_ptBenchmark1.Y + "','" +
            SideBodyPosition.m_ptBenchmark2.X + "','" +
            SideBodyPosition.m_ptBenchmark2.Y + "','" +
            SideBodyPosition.m_ptChin.X + "','" +
            SideBodyPosition.m_ptChin.Y + "','" +
            SideBodyPosition.m_ptEar.X + "','" +
            SideBodyPosition.m_ptEar.Y + "','" +
            SideBodyPosition.m_ptGlabella.X + "','" +
            SideBodyPosition.m_ptGlabella.Y + "','" +
            SideBodyPosition.m_ptHip.X + "','" +
            SideBodyPosition.m_ptHip.Y + "','" +
            SideBodyPosition.m_ptKnee.X + "','" +
            SideBodyPosition.m_ptKnee.Y + "','" +
            SideBodyPosition.m_ptKneeLeftBelt.X + "','" +
            SideBodyPosition.m_ptKneeLeftBelt.Y + "','" +
            SideBodyPosition.m_ptKneeRightBelt.X + "','" +
            SideBodyPosition.m_ptKneeRightBelt.Y + "','" +
            SideBodyPosition.m_ptLeftBelt.X + "','" +
            SideBodyPosition.m_ptLeftBelt.Y + "','" +
            SideBodyPosition.m_ptRightBelt.X + "','" +
            SideBodyPosition.m_ptRightBelt.Y + "','" +
            SideBodyPosition.m_ptShoulder.X + "','" +
            SideBodyPosition.m_ptShoulder.Y + "','" +
            Convert.ToBase64String(Compressed_bytes) + "')";

            CDatabase db = new CDatabase();
            db.ExecuteQuery(Query);
        }
        private void AddValuesIntoFrontBodyPositionKneedown()
        {
            FrontBodyPosition FrontBodyPositionKneedown = new FrontBodyPosition();
            m_JointEditDoc.GetKneedownFrontBodyPosition(ref FrontBodyPositionKneedown);

            byte[] Compressed_bytes =
                m_JointEditDoc.Compress(m_JointEditDoc.m_FrontKneedownImageBytes, false);

            string Query = "insert into FrontBodyPositionKneedown values((select max(UniqueId) from PatientDetails)" +
                ",'" + FrontBodyPositionKneedown.IsKneePositionDetected() + "','" +
                FrontBodyPositionKneedown.IsUnderBodyPositionDetected() + "','" +
                FrontBodyPositionKneedown.IsUpperBodyPositionDetected() + "','" +
                FrontBodyPositionKneedown.m_ptChin.X + "','" +
                FrontBodyPositionKneedown.m_ptChin.Y + "','" +
                FrontBodyPositionKneedown.m_ptGlabella.X + "','" +
                FrontBodyPositionKneedown.m_ptGlabella.Y + "','" +
                FrontBodyPositionKneedown.m_ptLeftAnkle.X + "','" +
                FrontBodyPositionKneedown.m_ptLeftAnkle.Y + "','" +
                FrontBodyPositionKneedown.m_ptLeftBelt.X + "','" +
                FrontBodyPositionKneedown.m_ptLeftBelt.Y + "','" +
                FrontBodyPositionKneedown.m_ptLeftEar.X + "','" +
                FrontBodyPositionKneedown.m_ptLeftEar.Y + "','" +
                FrontBodyPositionKneedown.m_ptLeftHip.X + "','" +
                FrontBodyPositionKneedown.m_ptLeftHip.Y + "','" +
                FrontBodyPositionKneedown.m_ptLeftKnee.X + "','" +
                FrontBodyPositionKneedown.m_ptLeftKnee.Y + "','" +
                FrontBodyPositionKneedown.m_ptLeftShoulder.X + "','" +
                FrontBodyPositionKneedown.m_ptLeftShoulder.Y + "','" +
                FrontBodyPositionKneedown.m_ptRightAnkle.X + "','" +
                FrontBodyPositionKneedown.m_ptRightAnkle.Y + "','" +
                FrontBodyPositionKneedown.m_ptRightBelt.X + "','" +
                FrontBodyPositionKneedown.m_ptRightBelt.Y + "','" +
                FrontBodyPositionKneedown.m_ptRightEar.X + "','" +
                FrontBodyPositionKneedown.m_ptRightEar.Y + "','" +
                FrontBodyPositionKneedown.m_ptRightHip.X + "','" +
                FrontBodyPositionKneedown.m_ptRightHip.Y + "','" +
                FrontBodyPositionKneedown.m_ptRightKnee.X + "','" +
                FrontBodyPositionKneedown.m_ptRightKnee.Y + "','" +
                FrontBodyPositionKneedown.m_ptRightShoulder.X + "','" +
                FrontBodyPositionKneedown.m_ptRightShoulder.Y + "','" +
                Convert.ToBase64String(Compressed_bytes) + "')";

            CDatabase db = new CDatabase();
            db.ExecuteQuery(Query);
        }
        private void AddValuesIntoFrontBodyPositionStanding()
        {
            FrontBodyPosition FrontBodyPositionStanding = new FrontBodyPosition();
            m_JointEditDoc.GetStandingFrontBodyPosition(ref FrontBodyPositionStanding);

            byte[] Compressed_bytes =
                m_JointEditDoc.Compress(m_JointEditDoc.m_FrontStandingImageBytes, false);

            string Query = "insert into FrontBodyPositionStanding values((select max(UniqueId) from PatientDetails)" +
                ",'" + FrontBodyPositionStanding.IsKneePositionDetected() + "','" +
                FrontBodyPositionStanding.IsUnderBodyPositionDetected() + "','" +
                FrontBodyPositionStanding.IsUpperBodyPositionDetected() + "','" +
                FrontBodyPositionStanding.m_ptChin.X + "','" +
                FrontBodyPositionStanding.m_ptChin.Y + "','" +
                FrontBodyPositionStanding.m_ptGlabella.X + "','" +
                FrontBodyPositionStanding.m_ptGlabella.Y + "','" +
                FrontBodyPositionStanding.m_ptLeftAnkle.X + "','" +
                FrontBodyPositionStanding.m_ptLeftAnkle.Y + "','" +
                FrontBodyPositionStanding.m_ptLeftBelt.X + "','" +
                FrontBodyPositionStanding.m_ptLeftBelt.Y + "','" +
                FrontBodyPositionStanding.m_ptLeftEar.X + "','" +
                FrontBodyPositionStanding.m_ptLeftEar.Y + "','" +
                FrontBodyPositionStanding.m_ptLeftHip.X + "','" +
                FrontBodyPositionStanding.m_ptLeftHip.Y + "','" +
                FrontBodyPositionStanding.m_ptLeftKnee.X + "','" +
                FrontBodyPositionStanding.m_ptLeftKnee.Y + "','" +
                FrontBodyPositionStanding.m_ptLeftShoulder.X + "','" +
                FrontBodyPositionStanding.m_ptLeftShoulder.Y + "','" +
                FrontBodyPositionStanding.m_ptRightAnkle.X + "','" +
                FrontBodyPositionStanding.m_ptRightAnkle.Y + "','" +
                FrontBodyPositionStanding.m_ptRightBelt.X + "','" +
                FrontBodyPositionStanding.m_ptRightBelt.Y + "','" +
                FrontBodyPositionStanding.m_ptRightEar.X + "','" +
                FrontBodyPositionStanding.m_ptRightEar.Y + "','" +
                FrontBodyPositionStanding.m_ptRightHip.X + "','" +
                FrontBodyPositionStanding.m_ptRightHip.Y + "','" +
                FrontBodyPositionStanding.m_ptRightKnee.X + "','" +
                FrontBodyPositionStanding.m_ptRightKnee.Y + "','" +
                FrontBodyPositionStanding.m_ptRightShoulder.X + "','" +
                FrontBodyPositionStanding.m_ptRightShoulder.Y + "','" +
                Convert.ToBase64String(Compressed_bytes) + "')";

            CDatabase db = new CDatabase();
            db.ExecuteQuery(Query);
        }
        private bool AddValuesIntoPatientDetails()
        {
            string YY = string.Empty; string MM = string.Empty; string DD = string.Empty;
            m_JointEditDoc.GetDataDoB(ref YY, ref MM, ref DD);

            DateTime DOB = new DateTime(int.Parse(YY), int.Parse(MM), int.Parse(DD));
            m_JointEditDoc.SetDataMeasurementTime(DateTime.Now.ToString("yy-MM-dd HH:mm"));

            try
            {
                #region Old Code Commented by Rajnish for GSP-700
                //string Query = "insert into PatientDetails " +
                //    "(PatientId,Name,Gender,Year,Month,Date,Height,DOB,MeasurementTime,Comment,BenchmarkDistance) values ('" +
                //    m_JointEditDoc.GetDataID() + "','" +
                //    m_JointEditDoc.GetDataName() + "','" +
                //    m_JointEditDoc.GetDataGender() + "','" +
                //    YY + "','" + MM + "','" + DD + "','" +
                //    m_JointEditDoc.GetDataHeight() + "','" +
                //    DOB + "','" +
                //    m_JointEditDoc.GetDataMeasurementTime() + "','" +
                //    IDC_CommentField.Text + "','" +
                //    m_JointEditDoc.GetBenchmarkDistance() + "')";
                #endregion

                //--Modified by Rajnish For GSP-700--//
                string Query = "insert into PatientDetails " +
                    "(PatientId,Name,Gender,Year,Month,Date,Height,DOB,MeasurementTime,Comment,BenchmarkDistance,lut) values ('" +
                    m_JointEditDoc.GetDataID().Replace("'", "''").Replace("\"", "\"\"") + "','" +
                    m_JointEditDoc.GetDataName().Replace("'", "''").Replace("\"", "\"\"") + "','" +
                    m_JointEditDoc.GetDataGender() + "','" +
                    YY + "','" + MM + "','" + DD + "','" +
                    m_JointEditDoc.GetDataHeight() + "','" +
                    DOB + "','" +
                    m_JointEditDoc.GetDataMeasurementTime() + "','" +
                    IDC_CommentField.Text.Replace("'", "''").Replace("\"", "\"\"") + "','" +
                    m_JointEditDoc.GetBenchmarkDistance() + "','123')";
                //---------------------------------------//
                CDatabase db = new CDatabase();
                db.ExecuteQuery(Query);

                Query = "select max(uniqueid) from PatientDetails";

                DataTable dt = new DataTable();
                dt = db.selectQuery(Query);

                foreach (DataRow row in dt.Rows)
                {
                    m_JointEditDoc.SetUniqueId(int.Parse(row[0].ToString()));
                }

            }
            catch (Exception)
            {
                return false;

            }
            return true;

        }
        int FromPage, ToPage;

        bool m_PrintingStatus = false;
        private void IDC_PrintBtn_Click(object sender, EventArgs e)
        {
            m_PrintingStatus = true;
            IDC_BTN_NAMECHANGE.Image = Yugamiru.Properties.Resources.namechange_up;
            IDC_BTN_RETURNTOPMENU.Image = Yugamiru.Properties.Resources.returnstart_up;
            IDC_RemeasurementBtn.Image = Yugamiru.Properties.Resources.imagechange_up;
            IDC_EditBtn.Image = Yugamiru.Properties.Resources.jointedit_up;

            IDC_PrintBtn.Image = Yugamiru.Properties.Resources.reportprint_on;
            IDC_ScoresheetBtn.Image = Yugamiru.Properties.Resources.reportdisplay_up;
            IDC_BTN_DATASAVE.Image = Yugamiru.Properties.Resources.datasave_up;
            IDC_MeasurementEndBtn.Image = Yugamiru.Properties.Resources.startred_up;


            NextPageNum = 1;
            PrintDialog printDlg = new PrintDialog();
            PrintDocument printDoc = new PrintDocument();
            printDoc.DocumentName = "Print Document";
            printDlg.Document = printDoc;
            printDlg.AllowSelection = true;
            printDlg.AllowSomePages = true;


            //Call ShowDialog
            if (printDlg.ShowDialog() == DialogResult.OK)
            {
                FromPage = printDlg.PrinterSettings.FromPage;
                if (FromPage == 2)
                    NextPageNum = 2;
                if (FromPage > 2)
                    NextPageNum = 3;


                ToPage = printDlg.PrinterSettings.ToPage;
                // Create a new PrintPreviewDialog using constructor.
                this.PrintPreviewDialog1 = new PrintPreviewDialog();

                //Set the size, location, and name.
                this.PrintPreviewDialog1.ClientSize = new Size(1024, 1280);
                this.PrintPreviewDialog1.Location = new Point(0, 0);
                this.PrintPreviewDialog1.Name = "Yugamiru";
                //this.PrintPreviewDialog1.PrintPreviewControl.Zoom = 1.25;

                string strComment;
                strComment = IDC_CommentField.Text;
                //’èŒ`•¶‚Ì‚Ü‚Ü‚È‚ç‚Î•Û‘¶‚µ‚È‚¢
                /* string strFixedComment;
                 m_JointEditDoc.GetFixedComment(strFixedComment);
                 if (strComment.Compare(strFixedComment) == 0)
                 {
                     strComment = "";
                 }*/
                m_JointEditDoc.SetDataComment(strComment);
                //printDoc.DefaultPageSettings.PaperSize = new PaperSize("", 778, 1100);
                //printDoc.DefaultPageSettings.PaperSize = new PaperSize("", 827, 1170);

                // Associate the event-handling method with the 
                // document's PrintPage event.
                printDoc.PrintPage += document_PrintPage; //new PrintPageEventHandler(document_PrintPage);

                //document.PrintPage += PrintImage;
                // Set the minimum size the dialog can be resized to.
                //this.PrintPreviewDialog1.MinimumSize = new Size(375, 250);

                // Set the UseAntiAlias property to true, which will allow the 
                // operating system to smooth fonts.
                this.PrintPreviewDialog1.UseAntiAlias = true;
                printDoc.DocumentName = "Yugamiru";
                printDoc.DefaultPageSettings.Landscape = true;
                //PrintPreviewDialog1.Document = document;
                //PrintPreviewDialog1.ShowDialog();
                //document.PrinterSettings.PrintToFile = true;

                if (printDoc.PrinterSettings.PrinterName == "Microsoft Print to PDF")
                {
                    //MessageBox.Show("hi");
                    printDoc.PrinterSettings.PrintToFile = true;
                    printDoc.Print();
                }
                else
                    printDoc.Print();
                //document.PrinterSettings.PrintFileName = Path.Combine("e://", "hktespri" + ".pdf");
                //document.Print();
            }


        }

        public Image GetStandingMuscleBMPFileNameByID(int iMuscleID)
        {

            switch (iMuscleID)
            {
                case Constants.MUSCLEID_LATISSIMUSDORSI:
                    return new Bitmap(m_CL_Path + @"\r_01.bmp");
                case Constants.MUSCLEID_BICEPSFEMORIS_LEFT:
                    return new Bitmap(m_CL_Path + @"\r_02l.bmp");
                case Constants.MUSCLEID_BICEPSFEMORIS_RIGHT:
                    return new Bitmap(m_CL_Path + @"\r_02r.bmp");
                case Constants.MUSCLEID_TENSORFASCIAELATAE_LEFT:
                    return new Bitmap(m_CL_Path + @"\r_03l.bmp");
                case Constants.MUSCLEID_TENSORFASCIAELATAE_RIGHT:
                    return new Bitmap(m_CL_Path + @"\r_03r.bmp");
                case Constants.MUSCLEID_VASTUSLATERALIS_LEFT:
                    return new Bitmap(m_CL_Path + @"\r_04l.bmp");
                case Constants.MUSCLEID_VASTUSLATERALIS_RIGHT:
                    return new Bitmap(m_CL_Path + @"\r_04r.bmp");
                case Constants.MUSCLEID_QUADRICEPSFEMORIS_LEFT:
                    return new Bitmap(m_CL_Path + @"\r_05l.bmp");
                case Constants.MUSCLEID_QUADRICEPSFEMORIS_RIGHT:
                    return new Bitmap(m_CL_Path + @"\r_05r.bmp");
                case Constants.MUSCLEID_HIPJOINTCAPSULE_LEFT:
                    return new Bitmap(m_CL_Path + @"\r_06l.bmp");
                case Constants.MUSCLEID_HIPJOINTCAPSULE_RIGHT:
                    return new Bitmap(m_CL_Path + @"\r_06r.bmp");
                case Constants.MUSCLEID_GLUTEUSMEDIUS_LEFT:
                    return new Bitmap(m_CL_Path + @"\r_07l.bmp");
                case Constants.MUSCLEID_GLUTEUSMEDIUS_RIGHT:
                    return new Bitmap(m_CL_Path + @"\r_07r.bmp");
                case Constants.MUSCLEID_RECTUSABDOMINIS:
                    return new Bitmap(m_CL_Path + @"\r_08.bmp");
                case Constants.MUSCLEID_ILIOTIBIALTRACT_LEFT:
                    return new Bitmap(m_CL_Path + @"\r_09l.bmp");
                case Constants.MUSCLEID_ILIOTIBIALTRACT_RIGHT:
                    return new Bitmap(m_CL_Path + @"\r_09r.bmp");
                case Constants.MUSCLEID_FIBULARIS_LEFT:
                    return new Bitmap(m_CL_Path + @"\r_10l.bmp");
                case Constants.MUSCLEID_FIBULARIS_RIGHT:
                    return new Bitmap(m_CL_Path + @"\r_10r.bmp");
                case Constants.MUSCLEID_GASTROCNEMIUS_LEFT:
                    return new Bitmap(m_CL_Path + @"\r_11l.bmp");
                case Constants.MUSCLEID_GASTROCNEMIUS_RIGHT:
                    return new Bitmap(m_CL_Path + @"\r_11r.bmp");
                case Constants.MUSCLEID_CUBOID_LEFT:
                    return new Bitmap(m_CL_Path + @"\r_12l.bmp");
                case Constants.MUSCLEID_CUBOID_RIGHT:
                    return new Bitmap(m_CL_Path + @"\r_12r.bmp");
                case Constants.MUSCLEID_ADDUCTOR_LEFT:
                    return new Bitmap(m_CL_Path + @"\r_13l.bmp");
                case Constants.MUSCLEID_ADDUCTOR_RIGHT:
                    return new Bitmap(m_CL_Path + @"\r_13r.bmp");
                case Constants.MUSCLEID_TRAPEZIUS_LEFT:
                    return new Bitmap(m_CL_Path + @"\r_14l.bmp");
                case Constants.MUSCLEID_TRAPEZIUS_RIGHT:
                    return new Bitmap(m_CL_Path + @"\r_14r.bmp");
                case Constants.MUSCLEID_STERNOCLEIDOMASTOID:
                    return new Bitmap(m_CL_Path + @"\r_15.bmp");
                case Constants.MUSCLEID_MAX:
                    return null;
            }
            return null;

        }

        public Image GetKneedownMuscleBMPFileNameByID(int iMuscleID)
        {
            switch (iMuscleID)
            {

                case Constants.MUSCLEID_LATISSIMUSDORSI:
                    return new Bitmap(m_CL_Path + @"\k_01.bmp");
                case Constants.MUSCLEID_BICEPSFEMORIS_LEFT:
                    return new Bitmap(m_CL_Path + @"\k_02l.bmp");
                case Constants.MUSCLEID_BICEPSFEMORIS_RIGHT:
                    return new Bitmap(m_CL_Path + @"\k_02r.bmp");
                case Constants.MUSCLEID_TENSORFASCIAELATAE_LEFT:
                    return new Bitmap(m_CL_Path + @"\k_03l.bmp");
                case Constants.MUSCLEID_TENSORFASCIAELATAE_RIGHT:
                    return new Bitmap(m_CL_Path + @"\k_03r.bmp");
                case Constants.MUSCLEID_VASTUSLATERALIS_LEFT:
                    return new Bitmap(m_CL_Path + @"\k_04l.bmp");
                case Constants.MUSCLEID_VASTUSLATERALIS_RIGHT:
                    return new Bitmap(m_CL_Path + @"\k_04r.bmp");
                case Constants.MUSCLEID_QUADRICEPSFEMORIS_LEFT:
                    return new Bitmap(m_CL_Path + @"\k_05l.bmp");
                case Constants.MUSCLEID_QUADRICEPSFEMORIS_RIGHT:
                    return new Bitmap(m_CL_Path + @"\k_05r.bmp");
                case Constants.MUSCLEID_HIPJOINTCAPSULE_LEFT:
                    return new Bitmap(m_CL_Path + @"\k_06l.bmp");
                case Constants.MUSCLEID_HIPJOINTCAPSULE_RIGHT:
                    return new Bitmap(m_CL_Path + @"\k_06r.bmp");
                case Constants.MUSCLEID_GLUTEUSMEDIUS_LEFT:
                    return new Bitmap(m_CL_Path + @"\k_07l.bmp");
                case Constants.MUSCLEID_GLUTEUSMEDIUS_RIGHT:
                    return new Bitmap(m_CL_Path + @"\k_07r.bmp");
                case Constants.MUSCLEID_RECTUSABDOMINIS:
                    return new Bitmap(m_CL_Path + @"\k_08.bmp");
                case Constants.MUSCLEID_ILIOTIBIALTRACT_LEFT:
                    return new Bitmap(m_CL_Path + @"\k_09l.bmp");
                case Constants.MUSCLEID_ILIOTIBIALTRACT_RIGHT:
                    return new Bitmap(m_CL_Path + @"\k_09r.bmp");
                case Constants.MUSCLEID_FIBULARIS_LEFT:
                    return new Bitmap(m_CL_Path + @"\k_10l.bmp");
                case Constants.MUSCLEID_FIBULARIS_RIGHT:
                    return new Bitmap(m_CL_Path + @"\k_10r.bmp");
                case Constants.MUSCLEID_GASTROCNEMIUS_LEFT:
                    return new Bitmap(m_CL_Path + @"\k_11l.bmp");
                case Constants.MUSCLEID_GASTROCNEMIUS_RIGHT:
                    return new Bitmap(m_CL_Path + @"\k_11r.bmp");
                case Constants.MUSCLEID_CUBOID_LEFT:
                    return new Bitmap(m_CL_Path + @"\k_12l.bmp");
                case Constants.MUSCLEID_CUBOID_RIGHT:
                    return new Bitmap(m_CL_Path + @"\k_12r.bmp");
                case Constants.MUSCLEID_ADDUCTOR_LEFT:
                    return new Bitmap(m_CL_Path + @"\k_13l.bmp");
                case Constants.MUSCLEID_ADDUCTOR_RIGHT:
                    return new Bitmap(m_CL_Path + @"\k_13r.bmp");
                case Constants.MUSCLEID_TRAPEZIUS_LEFT:
                    return new Bitmap(m_CL_Path + @"\k_14l.bmp");
                case Constants.MUSCLEID_TRAPEZIUS_RIGHT:
                    return new Bitmap(m_CL_Path + @"\k_14r.bmp");
                case Constants.MUSCLEID_STERNOCLEIDOMASTOID:
                    return new Bitmap(m_CL_Path + @"\k_15.bmp");
                case Constants.MUSCLEID_MAX:
                    return null;
            }


            return null;
        }
        public void DisposeControls()
        {
            m_MainPic.Image.Dispose();

            IDC_Mag1Btn.Image.Dispose();
            IDC_Mag2Btn.Image.Dispose();
            IDC_ResetImgBtn.Image.Dispose();

            IDC_BTN_NAMECHANGE.Image.Dispose();
            IDC_BTN_RETURNTOPMENU.Image.Dispose();
            IDC_RemeasurementBtn.Image.Dispose();
            IDC_EditBtn.Image.Dispose();

            IDC_PrintBtn.Image.Dispose();
            IDC_ScoresheetBtn.Image.Dispose();
            IDC_BTN_DATASAVE.Image.Dispose();
            IDC_MeasurementEndBtn.Image.Dispose();

            Yugamiru.Properties.Resources.Mainpic6.Dispose();

            Yugamiru.Properties.Resources.mag2.Dispose();
            Yugamiru.Properties.Resources.mag1.Dispose();
            Yugamiru.Properties.Resources.mag3_down.Dispose();

            Yugamiru.Properties.Resources.namechange_down.Dispose();
            Yugamiru.Properties.Resources.returnstart_down.Dispose();
            Yugamiru.Properties.Resources.imagechange_down.Dispose();
            Yugamiru.Properties.Resources.jointedit_down.Dispose();

            Yugamiru.Properties.Resources.reportprint_down.Dispose();
            Yugamiru.Properties.Resources.reportdisplay_down.Dispose();
            Yugamiru.Properties.Resources.datasave_down.Dispose();
            Yugamiru.Properties.Resources.startred_down.Dispose();

            this.Dispose();
            this.Close();

        }

        public void ResultView_FormClosed(object sender, FormClosedEventArgs e)
        {

            /* if (Application.OpenForms.Count == 0) Application.Exit();
             else
             {
                 List<Form> openForms = new List<Form>();

                 foreach (Form f in Application.OpenForms)
                     openForms.Add(f);

                 foreach (Form f in openForms)
                 {
                     if (f.Name != "IDD_BALANCELABO_DIALOG")
                     {
                         f.Controls.Clear();
                         f.Close();
                     }

                 }

             }*/


        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            switch (m_ZoomFlag)
            {
                case Constants.ZOOMOUT:
                    IDC_Mag1Btn.Image = Yugamiru.Properties.Resources.mag2_up;
                    break;
                case Constants.ZOOMIN:
                    IDC_Mag2Btn.Image = Yugamiru.Properties.Resources.mag1_up;
                    break;
                case Constants.RESET:
                    IDC_ResetImgBtn.Image = Yugamiru.Properties.Resources.mag3_up;
                    break;
                case Constants.SAVERESULT:
                    IDC_BTN_DATASAVE.Image = Yugamiru.Properties.Resources.datasave_up;
                    timer1.Stop();
                    break;

            }
        }
        public void UpdateRecord(string strMeasurementTime)
        {
            UpdateValuesIntoPatientDetails(); // update values into patientdetails
            UpdateValuesIntoFrontBodyPositionStanding(); // update values into FrontBodyPositionStanding Table
            UpdateValuesIntoFrontBodyPositionKneedown(); // update values into FrontBodyPositionKneedown Table
            UpdateValuesIntoSideBodyPosition();// update into SideBodyPosition Table

            //===========Added by sumit GSP-556, GSP-858 (cloud backup)
            try
            {
                CloudManager.StartUpdateToServer.StartSending();
            }
            catch (Exception ex)
            {
                //exception at taking cloud backup
            }
            //==========End GSP-556, GSP-858

            MessageBox.Show(Yugamiru.Properties.Resources.FINALSCREEN_SR1);//"Updated Sucessfully !!!");

        }

        private void UpdateValuesIntoPatientDetails()
        {
            string YY = string.Empty; string MM = string.Empty; string DD = string.Empty;
            m_JointEditDoc.GetDataDoB(ref YY, ref MM, ref DD);

            DateTime DOB = new DateTime(int.Parse(YY), int.Parse(MM), int.Parse(DD));
            //m_JointEditDoc.SetDataMeasurementTime(DateTime.Now.ToString("yy-MM-dd HH:mm"));

            try
            {
                #region Old Code Commented by Rajnish for GSP-700
                //string Query = "update PatientDetails set" +
                //    " PatientId = '" + m_JointEditDoc.GetDataID() + "'," +
                //    " Name = '" + m_JointEditDoc.GetDataName() + "'," +
                //    " Gender = '" + m_JointEditDoc.GetDataGender() + "'," +
                //    " Year = '" + YY + "'," +
                //    " Month = '" + MM + "'," +
                //    " Date = '" + DD + "'," +
                //    " Height = '" + m_JointEditDoc.GetDataHeight() + "'," +
                //    " Comment = '" + IDC_CommentField.Text + "'," +
                //    " DOB = '" + DOB + "' where uniqueid = '" + m_JointEditDoc.GetUniqueId() + "'";
                #endregion
                //--Modified by Rajnish For GSP-700--//
                string Query = "update PatientDetails set" +
                    " PatientId = '" + m_JointEditDoc.GetDataID().Replace("'", "''").Replace("\"", "\"\"") + "'," +
                    " Name = '" + m_JointEditDoc.GetDataName().Replace("'", "''").Replace("\"", "\"\"") + "'," +
                    " Gender = '" + m_JointEditDoc.GetDataGender() + "'," +
                    " Year = '" + YY + "'," +
                    " Month = '" + MM + "'," +
                    " Date = '" + DD + "'," +
                    " Height = '" + m_JointEditDoc.GetDataHeight() + "'," +
                    " Comment = '" + IDC_CommentField.Text.Replace("'", "''").Replace("\"", "\"\"") + "'," +
                    " DOB = '" + DOB + "', lut='789' where uniqueid = '" + m_JointEditDoc.GetUniqueId() + "'";
                //--------------------------------------//

                CDatabase db = new CDatabase();
                db.ExecuteQuery(Query);
            }
            catch (Exception)
            {
                return;

            }


        }
        private void UpdateValuesIntoFrontBodyPositionKneedown()
        {
            FrontBodyPosition FrontBodyPositionKneedown = new FrontBodyPosition();
            m_JointEditDoc.GetKneedownFrontBodyPosition(ref FrontBodyPositionKneedown);

            byte[] Compressed_bytes =
                m_JointEditDoc.Compress(m_JointEditDoc.m_FrontKneedownImageBytes, false);

            string Query = "Update FrontBodyPositionKneedown set" +
                " KneePositionDetected = '" + FrontBodyPositionKneedown.IsKneePositionDetected() + "'," +
                " UnderBodyPositionDetected = '" + FrontBodyPositionKneedown.IsUnderBodyPositionDetected() + "'," +
                " UpperBodyPositionDetected = '" + FrontBodyPositionKneedown.IsUpperBodyPositionDetected() + "'," +
                " ChinX= '" + FrontBodyPositionKneedown.m_ptChin.X + "'," +
                " ChinY = '" + FrontBodyPositionKneedown.m_ptChin.Y + "'," +
                " GlabellaX = '" + FrontBodyPositionKneedown.m_ptGlabella.X + "'," +
                " GlabellaY = '" + FrontBodyPositionKneedown.m_ptGlabella.Y + "'," +
                " LeftAnkleX = '" + FrontBodyPositionKneedown.m_ptLeftAnkle.X + "'," +
                " LeftAnkleY ='" + FrontBodyPositionKneedown.m_ptLeftAnkle.Y + "'," +
                " LeftBeltX = '" + FrontBodyPositionKneedown.m_ptLeftBelt.X + "'," +
                " LeftBeltY = '" + FrontBodyPositionKneedown.m_ptLeftBelt.Y + "'," +
                " LeftEarX='" + FrontBodyPositionKneedown.m_ptLeftEar.X + "'," +
                " LeftEarY='" + FrontBodyPositionKneedown.m_ptLeftEar.Y + "'," +
                " LeftHipX='" + FrontBodyPositionKneedown.m_ptLeftHip.X + "'," +
                " LeftHipY='" + FrontBodyPositionKneedown.m_ptLeftHip.Y + "'," +
                " LeftKneeX='" + FrontBodyPositionKneedown.m_ptLeftKnee.X + "'," +
                " LeftKneeY='" + FrontBodyPositionKneedown.m_ptLeftKnee.Y + "'," +
                " LeftShoulderX='" + FrontBodyPositionKneedown.m_ptLeftShoulder.X + "'," +
                " LeftShoulderY = '" + FrontBodyPositionKneedown.m_ptLeftShoulder.Y + "'," +
                " RightAnkleX='" + FrontBodyPositionKneedown.m_ptRightAnkle.X + "'," +
                " RightAnkleY='" + FrontBodyPositionKneedown.m_ptRightAnkle.Y + "'," +
                " RightBeltX='" + FrontBodyPositionKneedown.m_ptRightBelt.X + "'," +
                " RightBeltY='" + FrontBodyPositionKneedown.m_ptRightBelt.Y + "'," +
                " RightEarX='" + FrontBodyPositionKneedown.m_ptRightEar.X + "'," +
                " RightEarY='" + FrontBodyPositionKneedown.m_ptRightEar.Y + "'," +
                " RightHipX='" + FrontBodyPositionKneedown.m_ptRightHip.X + "'," +
                " RightHipY='" + FrontBodyPositionKneedown.m_ptRightHip.Y + "'," +
                " RightKneeX='" + FrontBodyPositionKneedown.m_ptRightKnee.X + "'," +
                " RightKneeY='" + FrontBodyPositionKneedown.m_ptRightKnee.Y + "'," +
                " RightShoulderX='" + FrontBodyPositionKneedown.m_ptRightShoulder.X + "'," +
                " RightShoulderY='" + FrontBodyPositionKneedown.m_ptRightShoulder.Y + "'," +
                " ImageBytes ='" + Convert.ToBase64String(Compressed_bytes) + "'" +
                " where uniqueid = '" + m_JointEditDoc.GetUniqueId() + "'";

            CDatabase db = new CDatabase();
            db.ExecuteQuery(Query);
        }
        private void UpdateValuesIntoFrontBodyPositionStanding()
        {
            FrontBodyPosition FrontBodyPositionStanding = new FrontBodyPosition();
            m_JointEditDoc.GetStandingFrontBodyPosition(ref FrontBodyPositionStanding);

            byte[] Compressed_bytes =
                m_JointEditDoc.Compress(m_JointEditDoc.m_FrontStandingImageBytes, false);

            string Query = "Update FrontBodyPositionStanding set" +
                " KneePositionDetected = '" + FrontBodyPositionStanding.IsKneePositionDetected() + "'," +
                " UnderBodyPositionDetected = '" + FrontBodyPositionStanding.IsUnderBodyPositionDetected() + "'," +
                " UpperBodyPositionDetected = '" + FrontBodyPositionStanding.IsUpperBodyPositionDetected() + "'," +
                " ChinX= '" + FrontBodyPositionStanding.m_ptChin.X + "'," +
                " ChinY = '" + FrontBodyPositionStanding.m_ptChin.Y + "'," +
                " GlabellaX = '" + FrontBodyPositionStanding.m_ptGlabella.X + "'," +
                " GlabellaY = '" + FrontBodyPositionStanding.m_ptGlabella.Y + "'," +
                " LeftAnkleX = '" + FrontBodyPositionStanding.m_ptLeftAnkle.X + "'," +
                " LeftAnkleY ='" + FrontBodyPositionStanding.m_ptLeftAnkle.Y + "'," +
                " LeftBeltX = '" + FrontBodyPositionStanding.m_ptLeftBelt.X + "'," +
                " LeftBeltY = '" + FrontBodyPositionStanding.m_ptLeftBelt.Y + "'," +
                " LeftEarX='" + FrontBodyPositionStanding.m_ptLeftEar.X + "'," +
                " LeftEarY='" + FrontBodyPositionStanding.m_ptLeftEar.Y + "'," +
                " LeftHipX='" + FrontBodyPositionStanding.m_ptLeftHip.X + "'," +
                " LeftHipY='" + FrontBodyPositionStanding.m_ptLeftHip.Y + "'," +
                " LeftKneeX='" + FrontBodyPositionStanding.m_ptLeftKnee.X + "'," +
                " LeftKneeY='" + FrontBodyPositionStanding.m_ptLeftKnee.Y + "'," +
                " LeftShoulderX='" + FrontBodyPositionStanding.m_ptLeftShoulder.X + "'," +
                " LeftShoulderY = '" + FrontBodyPositionStanding.m_ptLeftShoulder.Y + "'," +
                " RightAnkleX='" + FrontBodyPositionStanding.m_ptRightAnkle.X + "'," +
                " RightAnkleY='" + FrontBodyPositionStanding.m_ptRightAnkle.Y + "'," +
                " RightBeltX='" + FrontBodyPositionStanding.m_ptRightBelt.X + "'," +
                " RightBeltY='" + FrontBodyPositionStanding.m_ptRightBelt.Y + "'," +
                " RightEarX='" + FrontBodyPositionStanding.m_ptRightEar.X + "'," +
                " RightEarY='" + FrontBodyPositionStanding.m_ptRightEar.Y + "'," +
                " RightHipX='" + FrontBodyPositionStanding.m_ptRightHip.X + "'," +
                " RightHipY='" + FrontBodyPositionStanding.m_ptRightHip.Y + "'," +
                " RightKneeX='" + FrontBodyPositionStanding.m_ptRightKnee.X + "'," +
                " RightKneeY='" + FrontBodyPositionStanding.m_ptRightKnee.Y + "'," +
                " RightShoulderX='" + FrontBodyPositionStanding.m_ptRightShoulder.X + "'," +
                " RightShoulderY='" + FrontBodyPositionStanding.m_ptRightShoulder.Y + "'," +
                " ImageBytes ='" + Convert.ToBase64String(Compressed_bytes) + "'" +
                " where uniqueid = '" + m_JointEditDoc.GetUniqueId() + "'";

            CDatabase db = new CDatabase();
            db.ExecuteQuery(Query);
        }
        private void UpdateValuesIntoSideBodyPosition()
        {
            SideBodyPosition SideBodyPosition = new SideBodyPosition();
            m_JointEditDoc.GetSideBodyPosition(ref SideBodyPosition);

            byte[] Compressed_bytes =
                m_JointEditDoc.Compress(m_JointEditDoc.m_SideImageBytes, false);
            string Query = "Update SideBodyPosition set" +
                " AnkleX ='" + SideBodyPosition.m_ptAnkle.X + "'," +
            " AnkleY='" + SideBodyPosition.m_ptAnkle.Y + "'," +
            " AnkleLeftBeltX='" + SideBodyPosition.m_ptAnkleLeftBelt.X + "'," +
            " AnkleLeftBeltY='" + SideBodyPosition.m_ptAnkleLeftBelt.Y + "'," +
            " AnkleRightBeltX = '" + SideBodyPosition.m_ptAnkleRightBelt.X + "'," +
            " AnkleRightBeltY='" + SideBodyPosition.m_ptAnkleRightBelt.Y + "'," +
            " BenchMark1X = '" + SideBodyPosition.m_ptBenchmark1.X + "'," +
            " BenchMark1Y='" + SideBodyPosition.m_ptBenchmark1.Y + "'," +
            " BenchMark2X='" + SideBodyPosition.m_ptBenchmark2.X + "'," +
            " BenchMark2Y='" + SideBodyPosition.m_ptBenchmark2.Y + "'," +
            " ChinX='" + SideBodyPosition.m_ptChin.X + "'," +
            " ChinY='" + SideBodyPosition.m_ptChin.Y + "'," +
            " EarX='" + SideBodyPosition.m_ptEar.X + "'," +
            " EarY='" + SideBodyPosition.m_ptEar.Y + "'," +
            " GlabellaX='" + SideBodyPosition.m_ptGlabella.X + "'," +
            " GlabellaY='" + SideBodyPosition.m_ptGlabella.Y + "'," +
            " HipX='" + SideBodyPosition.m_ptHip.X + "'," +
            " HipY='" + SideBodyPosition.m_ptHip.Y + "'," +
            " KneeX='" + SideBodyPosition.m_ptKnee.X + "'," +
            " KneeY='" + SideBodyPosition.m_ptKnee.Y + "'," +
            " KneeLeftBeltX='" + SideBodyPosition.m_ptKneeLeftBelt.X + "'," +
            " KneeLeftBeltY='" + SideBodyPosition.m_ptKneeLeftBelt.Y + "'," +
            " KneeRightBeltX='" + SideBodyPosition.m_ptKneeRightBelt.X + "'," +
            " KneeRightBeltY='" + SideBodyPosition.m_ptKneeRightBelt.Y + "'," +
            " LeftBeltX='" + SideBodyPosition.m_ptLeftBelt.X + "'," +
            " LeftBeltY='" + SideBodyPosition.m_ptLeftBelt.Y + "'," +
            " RightBeltX='" + SideBodyPosition.m_ptRightBelt.X + "'," +
            " RightBeltY='" + SideBodyPosition.m_ptRightBelt.Y + "'," +
            " ShoulderX='" + SideBodyPosition.m_ptShoulder.X + "'," +
            " ShoulderY = '" + SideBodyPosition.m_ptShoulder.Y + "'," +
            " ImageBytes = '" + Convert.ToBase64String(Compressed_bytes) + "'" +
            " where uniqueid = '" + m_JointEditDoc.GetUniqueId() + "'";

            CDatabase db = new CDatabase();
            db.ExecuteQuery(Query);
        }

        private void ResultView_Load(object sender, EventArgs e)
        {
            IDC_CommentField.Focus();

        }

        private void IDC_CommentField_KeyDown(object sender, KeyEventArgs e)
        {


        }

        private void ResultView_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void IDC_CommentField_TextChanged(object sender, EventArgs e)
        {

        }

        private void ResultView_KeyUp(object sender, KeyEventArgs e)
        {
        }

        private void IDC_CommentField_KeyUp(object sender, KeyEventArgs e)
        {
        }

        private void IDC_CommentField_Enter(object sender, EventArgs e)
        {
        }

        private void IDC_CommentField_Leave(object sender, EventArgs e)
        {
        }
        bool CheckForDataAlreadyExistInDatabase()
        {
            DataTable dt = new DataTable();
            CDatabase database = new CDatabase();
            if (ImportYGAFileFlag)
            {
                string YY = string.Empty; string MM = string.Empty; string DD = string.Empty;
                m_JointEditDoc.GetDataDoB(ref YY, ref MM, ref DD);
                DateTime DOB = new DateTime(int.Parse(YY), int.Parse(MM), int.Parse(DD));
                #region Old Code Commented by Rajnish for GSP-700
                //string query = "select uniqueid from patientdetails where " +
                //    "patientid = '" + m_JointEditDoc.GetDataID() + "' and " +
                //    "name = '" + m_JointEditDoc.GetDataName() + "' and " +
                //    "DOB = '" + DOB + "'";
                #endregion
                //--Modified by Rajnish For GSP-700--//
                string query = "select uniqueid from patientdetails where " +
                    "patientid = '" + m_JointEditDoc.GetDataID().Replace("'", "''").Replace("\"", "\"\"") + "' and " +
                    "name = '" + m_JointEditDoc.GetDataName().Replace("'", "''").Replace("\"", "\"\"") + "' and " +
                    "DOB = '" + DOB + "'";
                //------------------------------------//

                dt = database.selectQuery(query);
                if (dt.Rows.Count > 0)
                {
                    return true;
                }
                else
                    return false;
            }
            else
            {
                string strMeasurementTime = string.Empty;
                strMeasurementTime = m_JointEditDoc.GetDataMeasurementTime();
                // CTime timeCurrent = CTime::GetCurrentTime();

                if (strMeasurementTime != "")
                {
                    return true;
                }
                else
                    return false;
            }
        }
    }
}
