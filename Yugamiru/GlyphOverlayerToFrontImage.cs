﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Yugamiru
{
    public class GlyphOverlayerToFrontImage
    {
        int ARROW_OFFSET = 30;
        int ARROW_LENGTH = 50;
        int ARROW_WIDTH = 8;
        //int ARROW_LENGTH = 5;//by Meena
        //int ARROW_WIDTH = 2;// by Meena



        bool m_bValidCenterLine;
        int m_uiCenterLineStyle;
        Color m_crCenterLineColor;
        int m_uiCenterLineWidth;
        int m_iCenterLineXPos;
        int m_iCenterLineTop;
        int m_iCenterLineBottom;

        bool m_bValidCentroidLine;
        int m_uiCentroidLineStyle;
        Color m_crCentroidLineColor;
        int m_uiCentroidLineWidth;
        int m_iCentroidLineXPos;
        int m_iCentroidLineTop;
        int m_iCentroidLineBottom;

        // ŠÖßŠÔƒ‰ƒCƒ“•\Ž¦.
        bool m_bValidJointConnectionLine;
        int m_uiJointConnectionLineStyle;
        Color m_crJointConnectionLineColor;
        int m_uiJointConnectionLineWidth;

        // ŠÖß“_ƒ}[ƒJ[ƒTƒCƒY
        int m_iMarkerSize;
        int m_iArrowWidth;
        int m_iArrowLength;
        int m_iArrowInvisible;

        public Point ptRightEar;
        public Point ptLeftEar;
        public Point ptChin;
        public Point ptGlabella;
        public Point ptRightHip;
        public Point ptLeftHip;
        public Point ptRightKnee;
        public Point ptLeftKnee;
        public Point ptRightAnkle;
        public Point ptLeftAnkle;
        public Point ptRightShoulder;
        public Point ptLeftShoulder;

        // ŠÖßˆÊ’u.
        FrontBodyPosition m_FrontBodyPosition;

        // ”»’èŒ‹‰Ê
        FrontBodyResultData m_FrontBodyResultData;

        public void MyDrawLine(Graphics pDC, Point st, Point end, int style /* =PS_SOLID */, Color rgb /*=RGB(0,0,0)*/, int width /*=1*/)
        {
            //	MoveToEx(hDC, st.x, st.y, NULL);
            //	LineTo(hDC, end.x, end.y);

            /*    unsigned type[8];
                int c = CDashLine::GetPattern(type, false, width, style);
                CDashLine line(*pDC, type, c);
                CPen penNew(style, width, rgb );

                CPen* ppenOld = pDC->SelectObject(&penNew);
                line.MoveTo(st.x, st.y);
            line.LineTo(end.x, end.y);

            pDC->SelectObject(ppenOld );*/
            Pen penNew = new Pen(rgb, width - 2);// width); by meena
            /*   double ratioX = 304.00 / 1024.00;
               double ratioY = 380.00 / 1280.00;


               double X_new = (double)st.X * ratioX;
               double Y_new = (double)st.Y * ratioY;
               double X_new1 = (double)end.X * ratioX;
               double Y_new1 = (double)end.Y * ratioY;
               pDC.DrawLine(penNew, (int)X_new, (int)Y_new, (int)X_new1, (int)Y_new1);*/
            pDC.DrawLine(penNew, st.X, st.Y, end.X, end.Y);

        }


        public GlyphOverlayerToFrontImage()
        {
            m_bValidCenterLine = false;

            m_uiCenterLineStyle = 0;

            m_crCenterLineColor = new Color();

            m_uiCenterLineWidth = 0;

            m_iCenterLineXPos = 0;

            m_iCenterLineTop = 0;

            m_iCenterLineBottom = 0;

            m_bValidJointConnectionLine = false;

            m_uiJointConnectionLineStyle = 0;

            m_crJointConnectionLineColor = new Color();

            m_uiJointConnectionLineWidth = 0;

            m_iMarkerSize = 0;

            m_iArrowWidth = 0;

            m_iArrowLength = 0;

            m_iArrowInvisible = 0;

            m_FrontBodyPosition = new FrontBodyPosition();

            m_FrontBodyResultData = new FrontBodyResultData();

        }




        public GlyphOverlayerToFrontImage(GlyphOverlayerToFrontImage rSrc)
        {
            m_bValidCenterLine = rSrc.m_bValidCenterLine;

            m_uiCenterLineStyle = rSrc.m_uiCenterLineStyle;

            m_crCenterLineColor = rSrc.m_crCenterLineColor;

            m_uiCenterLineWidth = rSrc.m_uiCenterLineWidth;

            m_iCenterLineXPos = rSrc.m_iCenterLineXPos;

            m_iCenterLineTop = rSrc.m_iCenterLineTop;

            m_iCenterLineBottom = rSrc.m_iCenterLineBottom;

            m_bValidJointConnectionLine = rSrc.m_bValidJointConnectionLine;

            m_uiJointConnectionLineStyle = rSrc.m_uiJointConnectionLineStyle;

            m_crJointConnectionLineColor = rSrc.m_crJointConnectionLineColor;

            m_uiJointConnectionLineWidth = rSrc.m_uiJointConnectionLineWidth;

            m_iMarkerSize = rSrc.m_iMarkerSize;

            m_iArrowWidth = rSrc.m_iArrowWidth;

            m_iArrowLength = rSrc.m_iArrowLength;

            m_iArrowInvisible = rSrc.m_iArrowInvisible;

            m_FrontBodyPosition = rSrc.m_FrontBodyPosition;

        }




        /*  m_FrontBodyResultData(rSrc.m_FrontBodyResultData )
              {
              }*/

        ~GlyphOverlayerToFrontImage()
        {
        }

        /*GlyphOverlayerToFrontImage::operator=( const CGlyphOverlayerToFrontImage &rSrc )
        {
            m_bValidCenterLine = rSrc.m_bValidCenterLine;
            m_uiCenterLineStyle = rSrc.m_uiCenterLineStyle;
            m_crCenterLineColor = rSrc.m_crCenterLineColor;
            m_uiCenterLineWidth = rSrc.m_uiCenterLineWidth;
            m_iCenterLineXPos = rSrc.m_iCenterLineXPos;
            m_iCenterLineTop = rSrc.m_iCenterLineTop;
            m_iCenterLineBottom = rSrc.m_iCenterLineBottom;
            m_bValidJointConnectionLine = rSrc.m_bValidJointConnectionLine;
            m_uiJointConnectionLineStyle = rSrc.m_uiJointConnectionLineStyle;
            m_crJointConnectionLineColor = rSrc.m_crJointConnectionLineColor;
            m_uiJointConnectionLineWidth = rSrc.m_uiJointConnectionLineWidth;
            m_iMarkerSize = rSrc.m_iMarkerSize;
            m_iArrowWidth = rSrc.m_iArrowWidth;
            m_iArrowLength = rSrc.m_iArrowLength;
            m_iArrowInvisible = rSrc.m_iArrowInvisible;
            m_FrontBodyPosition = rSrc.m_FrontBodyPosition;
            m_FrontBodyResultData = rSrc.m_FrontBodyResultData;
            return *this;
        }*/

        public void SetCenterLineData(
            bool bValid, int uiStyle, Color crColor, int uiWidth,
            int iXPos, int iTop, int iBottom)
        {
            m_bValidCenterLine = bValid;
            m_uiCenterLineStyle = uiStyle;
            m_crCenterLineColor = crColor;
            m_uiCenterLineWidth = uiWidth;
            m_iCenterLineXPos = iXPos;
            m_iCenterLineTop = iTop;
            m_iCenterLineBottom = iBottom;
        }
        public void SetCentroidLineData(
        bool bValid, int uiStyle, Color crColor, int uiWidth,
        int iXPos, int iTop, int iBottom)
        {
            m_bValidCentroidLine = bValid;
            m_uiCentroidLineStyle = uiStyle;
            m_crCentroidLineColor = crColor;
            m_uiCentroidLineWidth = uiWidth;
            m_iCentroidLineXPos = iXPos;
            m_iCentroidLineTop = iTop;
            m_iCentroidLineBottom = iBottom;
        }

        public void SetJointConnectionLineData(bool bValid, int uiStyle, Color crColor, int uiWidth)
        {
            m_bValidJointConnectionLine = bValid;
            m_uiJointConnectionLineStyle = uiStyle;
            m_crJointConnectionLineColor = crColor;
            m_uiJointConnectionLineWidth = uiWidth;
        }

        public void SetMarkerSize(int iMarkerSize)
        {
            m_iMarkerSize = iMarkerSize;
        }

        public void SetFrontBodyPosition(FrontBodyPosition FrontBodyPosition)
        {
            m_FrontBodyPosition = FrontBodyPosition;
        }

        public void SetFrontBodyResultData(FrontBodyResultData FrontBodyResultData)
        {
            m_FrontBodyResultData = FrontBodyResultData;
        }

        public void DrawCenterLine(Graphics pDC)
        {
            if (m_bValidCenterLine)
            {
                Point ptStart = new Point(m_iCenterLineXPos, m_iCenterLineTop);
                Point ptEnd = new Point(m_iCenterLineXPos, m_iCenterLineBottom);
                MyDrawLine(pDC, ptStart, ptEnd,
                    m_uiCenterLineStyle, m_crCenterLineColor, m_uiCenterLineWidth);


            }
        }
        void DrawCentroidLine(Graphics pDC)
        {
            if (m_bValidCentroidLine)
            {
                Point ptStart = new Point(m_iCentroidLineXPos, m_iCentroidLineTop );
                Point ptEnd = new Point(m_iCentroidLineXPos, m_iCentroidLineBottom );
                MyDrawLine(pDC, ptStart, ptEnd,
                    m_uiCentroidLineStyle, m_crCentroidLineColor, m_uiCentroidLineWidth);
            }
        }

        public void DrawMarker(Graphics pDC, Point ptCenter, Color col, bool bFill)
        {
            Pen penNew = new Pen(col);
            //Brush brNew = Brushes.AliceBlue;
            SolidBrush brNew = new SolidBrush(col);

            /*   CPen* ppenOld = pDC->SelectObject(&penNew);
               CBrush* pbrOld = NULL;
               HBRUSH hbrOld = NULL;
               if (bFill)
               {
                   pbrOld = pDC->SelectObject(&brNew);
               }
               else
               {
                   hbrOld = (HBRUSH)(pDC->SelectObject((HBRUSH)GetStockObject(NULL_BRUSH)));
               }*/
            /*   double ratioX = 304.00 / 1024.00;
               double ratioY = 380.00 / 1280.00;
               double new_X = (double)ptCenter.X * ratioX;
               double new_Y = (double)ptCenter.Y * ratioY;*/



            pDC.FillEllipse(brNew,
                ptCenter.X - m_iMarkerSize / 2,
                ptCenter.Y - m_iMarkerSize / 2,
                m_iMarkerSize + 1,//ptCenter.X + m_iMarkerSize + 1,
                m_iMarkerSize + 1);//ptCenter.Y + m_iMarkerSize + 1);
            /*    if (ppenOld != NULL)
                {
                    pDC->SelectObject(ppenOld);
                    ppenOld = NULL;
                }
                if (pbrOld != NULL)
                {
                    pDC->SelectObject(pbrOld);
                    pbrOld = NULL;
                }
                if (hbrOld != NULL)
                {
                    pDC->SelectObject(hbrOld);
                    hbrOld = NULL;
                }*/
        }

        public void DrawLineBetweenMarkers(Graphics pDC, Point ptStart, Point ptEnd)
        {
            if (m_bValidJointConnectionLine)
            {
                MyDrawLine(pDC, ptStart, ptEnd,
                    m_uiJointConnectionLineStyle,
                    m_crJointConnectionLineColor,
                    m_uiJointConnectionLineWidth);
            }
        }

        public void DrawArrow(Graphics pDC, int iXPos, int iYPos, int iDirAndNorm, int iVertical)
        {
            if (m_iArrowInvisible > 0)
            {
                return;
            }
            if (iDirAndNorm == 0)
            {
                return;
            }
            Point ptStart = new Point(0, 0);
            Point ptEnd = new Point(0, 0);
            int iDir = 0;
            int iNorm = 0;
            int iLength = 0;

            int iArrowLength = m_iArrowLength;
            if (iArrowLength == 0)
            {
                iArrowLength = ARROW_LENGTH;
                //iArrowLength = 5;
            }
            int iArrowWidth = m_iArrowWidth;
            if (iArrowWidth == 0)
            {
                iArrowWidth = ARROW_WIDTH;
                //iArrowWidth = 2;
            }

            if (iVertical > 0)
            {
                // ‚’¼ƒxƒNƒgƒ‹‚Ìê‡
                if (iDirAndNorm > 0)
                {
                    iDir = 1;
                    iNorm = iDirAndNorm;
                    iLength = iArrowLength * iNorm;
                    ptStart.X = iXPos;
                    ptStart.Y = iYPos - iLength / 2;
                    ptEnd.X = iXPos;
                    ptEnd.Y = iYPos + iLength / 2;
                }
                else
                {

                    iDir = -1;
                    iNorm = -iDirAndNorm;
                    iLength = iArrowLength * iNorm;
                    ptStart.X = iXPos;
                    ptStart.Y = iYPos + iLength / 2;
                    ptEnd.X = iXPos;
                    ptEnd.Y = iYPos - iLength / 2;
                }
            }
            else
            {
                //iArrowLength = 20;
                // …•½ƒxƒNƒgƒ‹‚Ìê‡
                if (iDirAndNorm > 0)
                {
                    iDir = 1;
                    iNorm = iDirAndNorm;
                    iLength = iArrowLength * iNorm;
                    ptStart.X = iXPos - iLength / 2;
                    ptStart.Y = iYPos;
                    ptEnd.X = iXPos + iLength / 2;
                    ptEnd.Y = iYPos;
                }
                else
                {
                    iDir = -1;
                    iNorm = -iDirAndNorm;
                    iLength = iArrowLength * iNorm;
                    ptStart.X = iXPos + iLength / 2;
                    ptStart.Y = iYPos;
                    ptEnd.X = iXPos - iLength / 2;
                    ptEnd.Y = iYPos;
                }
            }

            Color colArrow = Color.Black;
            switch (iNorm)
            {
                case 1:
                    colArrow = Color.Yellow;
                    break;
                case 2:
                case 3:
                    colArrow = Color.Red;
                    break;
            }

            Pen penNew = new Pen(colArrow, iArrowWidth);
            //Pen* ppenOld = pDC->SelectObject(&penNew);

            //Ž²
            pDC.DrawLine(penNew, ptStart.X, ptStart.Y, ptEnd.X, ptEnd.Y);
            // pDC->MoveTo(ptStart.x, ptStart.y);
            // pDC->LineTo(ptEnd.x, ptEnd.y);

            //ž¶
            double dArrowHeadLength = (double)iLength / 3;
            double theta = Math.Atan2((double)(ptStart.Y - ptEnd.Y), (double)(ptStart.X - ptEnd.X));
            double dt = 3.14 / 6.0;
            double dClockwiseTheta = theta - dt;
            double dAnticlockwiseTheta = theta + dt;
            // I“_‚©‚çŽn“_‚ÉŒü‚©‚¤ƒxƒNƒgƒ‹‚ðAI“_‚ð’†S‚ÉŽžŒv‰ñ‚è‚É‚R‚O“x‰ñ“]‚·‚éB
            // ‚³‚ç‚É‚»‚ÌƒxƒNƒgƒ‹‚ð•ûŒü‚ð•Û‚Á‚½‚Ü‚ÜAŒ³‚ÌƒxƒNƒgƒ‹‚Ì‚R•ª‚Ì‚P‚É‚·‚éB
            // ‚±‚¤‚µ‚Ä‹‚ß‚½ƒxƒNƒgƒ‹‚ÆAI“_‚ðŒ‹‚Ô.
            Point ptClockwise = new Point(0, 0);
            ptClockwise.X = ptEnd.X + (int)(dArrowHeadLength * Math.Cos(dClockwiseTheta));
            ptClockwise.Y = ptEnd.Y + (int)(dArrowHeadLength * Math.Sin(dClockwiseTheta));
            pDC.DrawLine(penNew, ptEnd.X, ptEnd.Y, ptClockwise.X, ptClockwise.Y);
            //pDC->LineTo(ptClockwise.x, ptClockwise.y);
            // I“_‚©‚çŽn“_‚ÉŒü‚©‚¤ƒxƒNƒgƒ‹‚ðAI“_‚ð’†S‚É”½ŽžŒv‰ñ‚è‚É‚R‚O“x‰ñ“]‚·‚éB
            // ‚³‚ç‚É‚»‚ÌƒxƒNƒgƒ‹‚ð•ûŒü‚ð•Û‚Á‚½‚Ü‚ÜAŒ³‚ÌƒxƒNƒgƒ‹‚Ì‚R•ª‚Ì‚P‚É‚·‚éB
            // ‚±‚¤‚µ‚Ä‹‚ß‚½ƒxƒNƒgƒ‹‚ÆAI“_‚ðŒ‹‚Ô.
            Point ptAnticlockwise = new Point(0, 0);
            ptAnticlockwise.X = ptEnd.X + (int)(dArrowHeadLength * Math.Cos(dAnticlockwiseTheta));
            ptAnticlockwise.Y = ptEnd.Y + (int)(dArrowHeadLength * Math.Sin(dAnticlockwiseTheta));
            pDC.DrawLine(penNew, ptEnd.X, ptEnd.Y, ptAnticlockwise.X, ptAnticlockwise.Y);

            //  pDC->MoveTo(ptEnd.x, ptEnd.y);
            //  pDC->LineTo(ptAnticlockwise.x, ptAnticlockwise.y);

            // pDC->SelectObject(ppenOld);
            // ppenOld = NULL;

        }

        public void DrawVerticalArrow(Graphics pDC, int iXPos, int iYPos, int iDirAndNorm)
        {
            /*    double iXPos_new = iXPos *(double) (304.00 / 1024.00);
                double iYPos_new = iYPos * (double)(380.00 / 1280.00);
                DrawArrow(pDC,(int)iXPos_new, (int)iYPos_new, iDirAndNorm, 1);*/
            DrawArrow(pDC, iXPos, iYPos, iDirAndNorm, 1);
        }

        public void DrawHorizontalArrow(Graphics pDC, int iXPos, int iYPos, int iDirAndNorm)
        {
            /*      double iXPos_new = iXPos * (double)(304.00 / 1024.00);
                  double iYPos_new = iYPos * (double)(380.00 / 1280.00);            
                  DrawArrow(pDC, (int)iXPos_new, (int)iYPos_new, iDirAndNorm, 0);*/
            DrawArrow(pDC, iXPos, iYPos, iDirAndNorm, 0);
        }

        public void DrawHipArrow(Graphics pDC, int iXPos, int iYPos, int iDirAndNorm)
        {
            if (m_iArrowInvisible > 0)
            {
                return;
            }
            if (iDirAndNorm == 0)
            {
                return;
            }
            Point ptStart = new Point();
            Point ptEnd = new Point();
            int iDir = 0;
            int iNorm = 0;
            int iLength = 0;

            int iArrowLength = m_iArrowLength;
            if (iArrowLength == 0)
            {
                iArrowLength = ARROW_LENGTH;
                //iArrowLength = 20;
            }
            int iArrowWidth = m_iArrowWidth;
            if (iArrowWidth == 0)
            {
                iArrowWidth = ARROW_WIDTH;
                //iArrowWidth = 2;
            }

            if (iDirAndNorm > 0)
            {
                iDir = 1;
                iNorm = iDirAndNorm;
                iLength = iArrowLength * iNorm;
                ptStart.X = iXPos;
                ptStart.Y = iYPos;
                ptEnd.X = iXPos + iLength;
                ptEnd.Y = iYPos;
            }
            else
            {
                iDir = -1;
                iNorm = -iDirAndNorm;
                iLength = iArrowLength * iNorm;
                ptStart.X = iXPos;
                ptStart.Y = iYPos;
                ptEnd.X = iXPos - iLength;
                ptEnd.Y = iYPos;
            }

            // F‚Íí‚ÉÔ.
            Color colArrow = Color.Red;
            Pen penNew = new Pen(colArrow, iArrowWidth);
            //CPen* ppenOld = pDC->SelectObject(&penNew);

            //Ž²
            pDC.DrawLine(penNew, ptStart.X, ptStart.Y, ptEnd.X, ptEnd.Y);
            //pDC->MoveTo(ptStart.x, ptStart.y);
            //pDC->LineTo(ptEnd.x, ptEnd.y);

            //ž¶
            double dArrowHeadLength = (double)iLength / 3;
            double theta = Math.Atan2((double)(ptStart.Y - ptEnd.Y), (double)(ptStart.X - ptEnd.X));
            double dt = 3.14 / 6.0;
            double dClockwiseTheta = theta - dt;
            double dAnticlockwiseTheta = theta + dt;
            // I“_‚©‚çŽn“_‚ÉŒü‚©‚¤ƒxƒNƒgƒ‹‚ðAI“_‚ð’†S‚ÉŽžŒv‰ñ‚è‚É‚R‚O“x‰ñ“]‚·‚éB
            // ‚³‚ç‚É‚»‚ÌƒxƒNƒgƒ‹‚ð•ûŒü‚ð•Û‚Á‚½‚Ü‚ÜAŒ³‚ÌƒxƒNƒgƒ‹‚Ì‚R•ª‚Ì‚P‚É‚·‚éB
            // ‚±‚¤‚µ‚Ä‹‚ß‚½ƒxƒNƒgƒ‹‚ÆAI“_‚ðŒ‹‚Ô.
            Point ptClockwise = new Point();
            ptClockwise.X = ptEnd.X + (int)(dArrowHeadLength * Math.Cos(dClockwiseTheta));
            ptClockwise.Y = ptEnd.Y + (int)(dArrowHeadLength * Math.Sin(dClockwiseTheta));
            pDC.DrawLine(penNew, ptEnd.X, ptEnd.Y, ptClockwise.X, ptClockwise.Y);
            //pDC->MoveTo(ptEnd.x, ptEnd.y);
            //pDC->LineTo(ptClockwise.x, ptClockwise.y);
            // I“_‚©‚çŽn“_‚ÉŒü‚©‚¤ƒxƒNƒgƒ‹‚ðAI“_‚ð’†S‚É”½ŽžŒv‰ñ‚è‚É‚R‚O“x‰ñ“]‚·‚éB
            // ‚³‚ç‚É‚»‚ÌƒxƒNƒgƒ‹‚ð•ûŒü‚ð•Û‚Á‚½‚Ü‚ÜAŒ³‚ÌƒxƒNƒgƒ‹‚Ì‚R•ª‚Ì‚P‚É‚·‚éB
            // ‚±‚¤‚µ‚Ä‹‚ß‚½ƒxƒNƒgƒ‹‚ÆAI“_‚ðŒ‹‚Ô.
            Point ptAnticlockwise = new Point();
            ptAnticlockwise.X = ptEnd.X + (int)(dArrowHeadLength * Math.Cos(dAnticlockwiseTheta));
            ptAnticlockwise.Y = ptEnd.Y + (int)(dArrowHeadLength * Math.Sin(dAnticlockwiseTheta));
            pDC.DrawLine(penNew, ptEnd.X, ptEnd.Y, ptAnticlockwise.X, ptAnticlockwise.Y);
            //pDC->MoveTo(ptEnd.x, ptEnd.y);
            //pDC->LineTo(ptAnticlockwise.x, ptAnticlockwise.y);

            //pDC->SelectObject(ppenOld);
            //ppenOld = NULL;
        }

        public void Draw(Graphics pDC)
        {
            int iArrowLength = m_iArrowLength;
            if (iArrowLength == 0)
            {
                iArrowLength = ARROW_LENGTH;
            }

            // ’†Sü•`‰æ.
            DrawCenterLine(pDC);

            DrawCentroidLine(pDC);

            // ŠÖßÚ‘±ü•`‰æ.



            bool bUnderBodyPositionDetected = m_FrontBodyPosition.IsUnderBodyPositionDetected();
            bool bKneePositionDetected = m_FrontBodyPosition.IsKneePositionDetected();
            bool bUpperBodyPositionDetected = m_FrontBodyPosition.IsUpperBodyPositionDetected();



            DrawLineBetweenMarkers(pDC, ptRightHip, ptLeftHip);
            DrawLineBetweenMarkers(pDC, ptRightShoulder, ptLeftShoulder);
            DrawLineBetweenMarkers(pDC, ptRightEar, ptLeftEar);
            DrawLineBetweenMarkers(pDC, ptChin, ptGlabella);
            DrawLineBetweenMarkers(pDC, ptRightShoulder, ptRightHip);
            DrawLineBetweenMarkers(pDC, ptRightHip, ptRightKnee);
            DrawLineBetweenMarkers(pDC, ptRightKnee, ptRightAnkle);
            DrawLineBetweenMarkers(pDC, ptLeftShoulder, ptLeftHip);
            DrawLineBetweenMarkers(pDC, ptLeftHip, ptLeftKnee);
            DrawLineBetweenMarkers(pDC, ptLeftKnee, ptLeftAnkle);

            // ŠÖßƒ}[ƒJ[•`‰æ.
            DrawMarker(pDC, ptRightHip, Color.Red, bUnderBodyPositionDetected);
            DrawMarker(pDC, ptLeftHip, Color.Red, bUnderBodyPositionDetected);
            DrawMarker(pDC, ptRightKnee, Color.Lime, bKneePositionDetected);
            DrawMarker(pDC, ptLeftKnee, Color.Lime, bKneePositionDetected);
            DrawMarker(pDC, ptRightAnkle, Color.Blue, bUnderBodyPositionDetected);
            DrawMarker(pDC, ptLeftAnkle, Color.Blue, bUnderBodyPositionDetected);
            //DrawMarker(pDC, ptRightShoulder, RGB(200, 200, 0), bUpperBodyPositionDetected);
            DrawMarker(pDC, ptRightShoulder, Color.FromArgb(200, 200, 0), bUpperBodyPositionDetected);
            //DrawMarker(pDC, ptLeftShoulder, RGB(200, 200, 0), bUpperBodyPositionDetected);
            DrawMarker(pDC, ptLeftShoulder, Color.FromArgb(200, 200, 0), bUpperBodyPositionDetected);
            //DrawMarker(pDC, ptRightEar, RGB(126, 0, 126), bUpperBodyPositionDetected);
            DrawMarker(pDC, ptRightEar, Color.FromArgb(126, 0, 126), bUpperBodyPositionDetected);
            //DrawMarker(pDC, ptLeftEar, RGB(126, 0, 126), bUpperBodyPositionDetected);
            DrawMarker(pDC, ptLeftEar, Color.FromArgb(126, 0, 126), bUpperBodyPositionDetected);
            //DrawMarker(pDC, ptChin, RGB(126, 126, 0), bUpperBodyPositionDetected);
            DrawMarker(pDC, ptChin, Color.FromArgb(126, 126, 0), bUpperBodyPositionDetected);
            //DrawMarker(pDC, ptGlabella, RGB(126, 126, 0), bUpperBodyPositionDetected);
            DrawMarker(pDC, ptGlabella, Color.FromArgb(126, 126, 0), bUpperBodyPositionDetected);

            // ¶‰E‘ÎÌˆÊ’u‚É‚ ‚éŠÖßƒ}[ƒJ[‚Ì’†“_‚ð‹‚ß‚é.
            Point ptCenterOfHip = new Point();
            Point ptCenterOfShoulder = new Point();
            Point ptCenterOfEar = new Point();
            ptCenterOfHip.X = (ptRightHip.X + ptLeftHip.X) / 2;
            ptCenterOfHip.Y = (ptRightHip.Y + ptLeftHip.Y) / 2;
            ptCenterOfShoulder.X = (ptRightShoulder.X + ptLeftShoulder.X) / 2;
            ptCenterOfShoulder.Y = (ptRightShoulder.Y + ptLeftShoulder.Y) / 2;
            ptCenterOfEar.X = (ptRightEar.X + ptLeftEar.X) / 2;
            ptCenterOfEar.Y = (ptRightEar.Y + ptLeftEar.Y) / 2;

            // –îˆó•`‰æ.
            DrawHipArrow(pDC, ptCenterOfHip.X, ptCenterOfHip.Y - ARROW_OFFSET,
                m_FrontBodyResultData.GetCenterBalance());

            DrawVerticalArrow(pDC, ptRightHip.X - ARROW_OFFSET, ptCenterOfHip.Y,
                +m_FrontBodyResultData.GetHip());

            DrawVerticalArrow(pDC, ptLeftHip.X + ARROW_OFFSET, ptCenterOfHip.Y,
                -m_FrontBodyResultData.GetHip());

            int iRightKneeOffset = iArrowLength * Math.Abs(m_FrontBodyResultData.GetRightKnee()) / 2;
            DrawHorizontalArrow(pDC, ptRightKnee.X - (iRightKneeOffset + ARROW_OFFSET),
                ptRightKnee.Y,
                -m_FrontBodyResultData.GetRightKnee());

            int iLeftKneeOffset = iArrowLength * Math.Abs(m_FrontBodyResultData.GetLeftKnee()) / 2;
            DrawHorizontalArrow(pDC, ptLeftKnee.X + (iLeftKneeOffset + ARROW_OFFSET),
                ptLeftKnee.Y,
                +m_FrontBodyResultData.GetLeftKnee());

            DrawHorizontalArrow(pDC, ptCenterOfShoulder.X, ptCenterOfShoulder.Y + ARROW_OFFSET,
                m_FrontBodyResultData.GetBodyCenter());

            DrawVerticalArrow(pDC, ptRightShoulder.X - ARROW_OFFSET, ptCenterOfShoulder.Y,
                +m_FrontBodyResultData.GetShoulderBal());


            DrawVerticalArrow(pDC, ptLeftShoulder.X + ARROW_OFFSET, ptCenterOfShoulder.Y,
                -m_FrontBodyResultData.GetShoulderBal());

            DrawHorizontalArrow(pDC, ptCenterOfEar.X, ptCenterOfEar.Y + ARROW_OFFSET,
                m_FrontBodyResultData.GetHeadCenter());

            DrawVerticalArrow(pDC, ptRightEar.X - ARROW_OFFSET, ptCenterOfEar.Y,
                +m_FrontBodyResultData.GetEarBal());

            DrawVerticalArrow(pDC, ptLeftEar.X + ARROW_OFFSET, ptCenterOfEar.Y,
                -m_FrontBodyResultData.GetEarBal());

        }

        public int GetArrowWidth()
        {
            return m_iArrowWidth;
        }

        public int GetArrowLength()
        {
            return m_iArrowLength;
        }

        public int IsArrowInvisible()
        {
            return m_iArrowInvisible;
        }

        public void SetArrowWidth(int iArrowWidth)
        {
            m_iArrowWidth = iArrowWidth;
        }

        public void SetArrowLength(int iArrowLength)
        {
            m_iArrowLength = iArrowLength;
        }

        public void SetArrowInvisible(int iArrowInvisible)
        {
            m_iArrowInvisible = iArrowInvisible;
        }

    }
}
