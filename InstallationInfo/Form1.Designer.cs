﻿namespace LicenseInformation
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PCNamelbl = new System.Windows.Forms.Label();
            this.Installlabl = new System.Windows.Forms.Label();
            this.InstallDtLbl = new System.Windows.Forms.Label();
            this.InstallByLbl = new System.Windows.Forms.Label();
            this.CommentLbl = new System.Windows.Forms.Label();
            this.PCNameTxtBox = new System.Windows.Forms.TextBox();
            this.InstallNameTxtBox = new System.Windows.Forms.TextBox();
            this.InstallDateTxtBox = new System.Windows.Forms.TextBox();
            this.InstallByTxtBox = new System.Windows.Forms.TextBox();
            this.OkBtn = new System.Windows.Forms.Button();
            this.CommentTxtBox = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.InstallNameLabelQue = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.LicenseKeyLblQue = new System.Windows.Forms.Label();
            this.licenseTxtbox = new System.Windows.Forms.TextBox();
            this.LicenseLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // PCNamelbl
            // 
            this.PCNamelbl.AutoSize = true;
            this.PCNamelbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PCNamelbl.Location = new System.Drawing.Point(9, 120);
            this.PCNamelbl.Name = "PCNamelbl";
            this.PCNamelbl.Size = new System.Drawing.Size(66, 16);
            this.PCNamelbl.TabIndex = 1;
            this.PCNamelbl.Text = "PC Name";
            // 
            // Installlabl
            // 
            this.Installlabl.AutoSize = true;
            this.Installlabl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Installlabl.Location = new System.Drawing.Point(9, 163);
            this.Installlabl.Name = "Installlabl";
            this.Installlabl.Size = new System.Drawing.Size(111, 16);
            this.Installlabl.TabIndex = 2;
            this.Installlabl.Text = "Installation Name";
            // 
            // InstallDtLbl
            // 
            this.InstallDtLbl.AutoSize = true;
            this.InstallDtLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstallDtLbl.Location = new System.Drawing.Point(9, 203);
            this.InstallDtLbl.Name = "InstallDtLbl";
            this.InstallDtLbl.Size = new System.Drawing.Size(103, 16);
            this.InstallDtLbl.TabIndex = 3;
            this.InstallDtLbl.Text = "Installation Date";
            // 
            // InstallByLbl
            // 
            this.InstallByLbl.AutoSize = true;
            this.InstallByLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstallByLbl.Location = new System.Drawing.Point(9, 245);
            this.InstallByLbl.Name = "InstallByLbl";
            this.InstallByLbl.Size = new System.Drawing.Size(77, 16);
            this.InstallByLbl.TabIndex = 4;
            this.InstallByLbl.Text = "Installed By";
            // 
            // CommentLbl
            // 
            this.CommentLbl.AutoSize = true;
            this.CommentLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CommentLbl.Location = new System.Drawing.Point(9, 286);
            this.CommentLbl.Name = "CommentLbl";
            this.CommentLbl.Size = new System.Drawing.Size(65, 16);
            this.CommentLbl.TabIndex = 5;
            this.CommentLbl.Text = "Comment";
            // 
            // PCNameTxtBox
            // 
            this.PCNameTxtBox.Enabled = false;
            this.PCNameTxtBox.Location = new System.Drawing.Point(169, 120);
            this.PCNameTxtBox.Name = "PCNameTxtBox";
            this.PCNameTxtBox.Size = new System.Drawing.Size(239, 20);
            this.PCNameTxtBox.TabIndex = 7;
            // 
            // InstallNameTxtBox
            // 
            this.InstallNameTxtBox.Location = new System.Drawing.Point(169, 163);
            this.InstallNameTxtBox.Name = "InstallNameTxtBox";
            this.InstallNameTxtBox.Size = new System.Drawing.Size(239, 20);
            this.InstallNameTxtBox.TabIndex = 8;
            this.InstallNameTxtBox.TextChanged += new System.EventHandler(this.licenseTxtbox_TextChanged);
            this.InstallNameTxtBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.licenseTxtbox_KeyDown);
            // 
            // InstallDateTxtBox
            // 
            this.InstallDateTxtBox.Enabled = false;
            this.InstallDateTxtBox.Location = new System.Drawing.Point(169, 203);
            this.InstallDateTxtBox.Name = "InstallDateTxtBox";
            this.InstallDateTxtBox.Size = new System.Drawing.Size(220, 20);
            this.InstallDateTxtBox.TabIndex = 9;
            // 
            // InstallByTxtBox
            // 
            this.InstallByTxtBox.Location = new System.Drawing.Point(169, 245);
            this.InstallByTxtBox.Name = "InstallByTxtBox";
            this.InstallByTxtBox.Size = new System.Drawing.Size(239, 20);
            this.InstallByTxtBox.TabIndex = 10;
            this.InstallByTxtBox.TextChanged += new System.EventHandler(this.licenseTxtbox_TextChanged);
            this.InstallByTxtBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.licenseTxtbox_KeyDown);
            // 
            // OkBtn
            // 
            this.OkBtn.Location = new System.Drawing.Point(333, 359);
            this.OkBtn.Name = "OkBtn";
            this.OkBtn.Size = new System.Drawing.Size(75, 23);
            this.OkBtn.TabIndex = 12;
            this.OkBtn.Text = "OK";
            this.OkBtn.UseVisualStyleBackColor = true;
            this.OkBtn.Click += new System.EventHandler(this.OkBtn_Click);
            // 
            // CommentTxtBox
            // 
            this.CommentTxtBox.Location = new System.Drawing.Point(169, 286);
            this.CommentTxtBox.Multiline = true;
            this.CommentTxtBox.Name = "CommentTxtBox";
            this.CommentTxtBox.Size = new System.Drawing.Size(238, 53);
            this.CommentTxtBox.TabIndex = 13;
            this.CommentTxtBox.TextChanged += new System.EventHandler(this.licenseTxtbox_TextChanged);
            this.CommentTxtBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.licenseTxtbox_KeyDown);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(388, 203);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(20, 20);
            this.dateTimePicker1.TabIndex = 14;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // InstallNameLabelQue
            // 
            this.InstallNameLabelQue.AutoSize = true;
            this.InstallNameLabelQue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InstallNameLabelQue.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.InstallNameLabelQue.Location = new System.Drawing.Point(411, 164);
            this.InstallNameLabelQue.Name = "InstallNameLabelQue";
            this.InstallNameLabelQue.Size = new System.Drawing.Size(19, 20);
            this.InstallNameLabelQue.TabIndex = 17;
            this.InstallNameLabelQue.Text = "?";
            this.toolTip1.SetToolTip(this.InstallNameLabelQue, "You can give a name to this installation.\r\nThe same name will appear on all other" +
        " linked PCs under the cloud settings");
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(169, 359);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 18;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(421, 50);
            this.label1.TabIndex = 19;
            this.label1.Text = "Please provide the information required for identifying this PC over Yugamiru clo" +
    "ud network";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LicenseKeyLblQue
            // 
            this.LicenseKeyLblQue.AutoSize = true;
            this.LicenseKeyLblQue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LicenseKeyLblQue.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.LicenseKeyLblQue.Location = new System.Drawing.Point(411, 76);
            this.LicenseKeyLblQue.Name = "LicenseKeyLblQue";
            this.LicenseKeyLblQue.Size = new System.Drawing.Size(19, 20);
            this.LicenseKeyLblQue.TabIndex = 15;
            this.LicenseKeyLblQue.Text = "?";
            // 
            // licenseTxtbox
            // 
            this.licenseTxtbox.Location = new System.Drawing.Point(169, 76);
            this.licenseTxtbox.Name = "licenseTxtbox";
            this.licenseTxtbox.Size = new System.Drawing.Size(239, 20);
            this.licenseTxtbox.TabIndex = 6;
            this.licenseTxtbox.TextChanged += new System.EventHandler(this.licenseTxtbox_TextChanged);
            this.licenseTxtbox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.licenseTxtbox_KeyDown);
            // 
            // LicenseLbl
            // 
            this.LicenseLbl.AutoSize = true;
            this.LicenseLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LicenseLbl.Location = new System.Drawing.Point(9, 76);
            this.LicenseLbl.Name = "LicenseLbl";
            this.LicenseLbl.Size = new System.Drawing.Size(60, 16);
            this.LicenseLbl.TabIndex = 0;
            this.LicenseLbl.Text = "License*";
            this.LicenseLbl.Click += new System.EventHandler(this.LicenseLbl_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 390);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.InstallNameLabelQue);
            this.Controls.Add(this.LicenseKeyLblQue);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.CommentTxtBox);
            this.Controls.Add(this.OkBtn);
            this.Controls.Add(this.InstallByTxtBox);
            this.Controls.Add(this.InstallDateTxtBox);
            this.Controls.Add(this.InstallNameTxtBox);
            this.Controls.Add(this.PCNameTxtBox);
            this.Controls.Add(this.licenseTxtbox);
            this.Controls.Add(this.CommentLbl);
            this.Controls.Add(this.InstallByLbl);
            this.Controls.Add(this.InstallDtLbl);
            this.Controls.Add(this.Installlabl);
            this.Controls.Add(this.PCNamelbl);
            this.Controls.Add(this.LicenseLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(449, 429);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(449, 429);
            this.Name = "Form1";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "gsport";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label PCNamelbl;
        private System.Windows.Forms.Label Installlabl;
        private System.Windows.Forms.Label InstallDtLbl;
        private System.Windows.Forms.Label InstallByLbl;
        private System.Windows.Forms.Label CommentLbl;
        private System.Windows.Forms.TextBox PCNameTxtBox;
        private System.Windows.Forms.TextBox InstallNameTxtBox;
        private System.Windows.Forms.TextBox InstallDateTxtBox;
        private System.Windows.Forms.TextBox InstallByTxtBox;
        private System.Windows.Forms.Button OkBtn;
        private System.Windows.Forms.TextBox CommentTxtBox;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label InstallNameLabelQue;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LicenseKeyLblQue;
        private System.Windows.Forms.TextBox licenseTxtbox;
        private System.Windows.Forms.Label LicenseLbl;
    }
}

