﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudManager
{    
   public enum Tables {None, PatientDetails, FrontBodyPositionKneeDown, FrontBodyPositionStanding, SideBodyPosition };
    public enum RowNumToJSON { AllRows=0,FirstRow=1,LastRow=2};
}
