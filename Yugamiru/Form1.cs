﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;
using System.Net;
using QRCoder;
using System.IO;
using static Yugamiru.QRCode;
using System.Resources;
using Emgu.CV.Structure;
using Emgu.CV;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Drawing.Imaging;
using WebComCation;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Diagnostics;



namespace Yugamiru
{
    public partial class IDD_BALANCELABO_DIALOG : Form
    {
        //Bitmap m_btnMeasurement;

        // m_btnAnalysis;
        //CBitmapButton m_btnClose;
        //CBitmapButton m_btnSetting;
        JointEditDoc GetDocument = new JointEditDoc();
        Bitmap m_hBitmapQRCode;
        public byte[] m_pbyteQRCodeBitmapBits;
        int m_iQRCodeBitmapWidth;
        int m_iQRCodeBitmapHeight;
        PictureBox picturebox1 = new PictureBox();
        Image imgQRcode;
        Bitmap bmBack1;
        IDD_MeasurementDlg m_MeasurementDlg;
        IDD_MEASUREMENT_START_VIEW m_MEASUREMENT_START;
        JointEditView m_JointEditView;
        SideJointEditView m_SideJointEditView;
        IDD_BALANCELABO m_BALANCELABO;
        SettingView m_SettingView;
        ResultView m_ResultView;
        OpenResult m_OpenResult;
        MyData m_BalanceLabData = new MyData();
        MyImage m_BalanceLaboImg = new MyImage();
        Process process = new Process();//Added by sumit for progress bar
        BackgroundWorker m_oWorker;
        // Point location;
        public static DateTime qlsGetDate; //Added By Suhana


        void ImportScoresheetImagesintoFolder(object sender, DoWorkEventArgs e)
        {
            CDatabase DB = new CDatabase();
            string Specific_Folder = Constants.path + Yugamiru.Properties.Resources.CURRENT_LANGUAGE;
            if (Directory.GetFiles(Specific_Folder).Length <= 1)
            {
                DB.RetrieveImage("select ImageFileName,ImageBlob " +
                "from ScoreSheetImages_" + Yugamiru.Properties.Resources.CURRENT_LANGUAGE);
            }


        }
        public IDD_BALANCELABO_DIALOG()
        {
            //Added By Sumit for Load Progress Bar-----START
            process.Exited += Process_Exited;
            process.StartInfo.FileName = Application.StartupPath + "\\yugamiruPGB.exe";
            process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            process.Start();
            process.CloseMainWindow();
            //Added by sumit load progress bar---------END
            

            #region OtherMainCode
            InitializeComponent();
            //RegistryKey key = Registry.CurrentUser;
            //key = key.OpenSubKey(Application.ProductName);
            //if (key == null)
            //{
            //    return;
            //}
            //else
            //{
            //    this.Width = Convert.ToInt32(key.GetValue("W"));
            //    this.Height = Convert.ToInt32(key.GetValue("H"));
            //}

            //Added By Suhana For GSP 866
            //if (System.IO.File.Exists(Application.StartupPath + @"\\" + "WindowsState.txt"))
            //{

            //    // string values = System.IO.File.ReadAllText(Application.StartupPath + @"\\" + "WindowsState.txt");
            //    string line1 = System.IO.File.ReadLines(Application.StartupPath + @"\\" + "WindowsState.txt").First();
            //    string line2 = System.IO.File.ReadLines(Application.StartupPath + @"\\" + "WindowsState.txt").Last();
            //     line1 = line1.Replace('{', ' ').Replace('}', ' ').Trim();
            //    line1 = line1.Replace('=', ' ').Trim();
            //    line1 = line1.Replace('X', ' ').Replace('Y', ' ').Trim();
            //  //  line1 = line1.Replace('X', ' ').TrimStart();
            //    //string points = line1.Replace(line1.Replace('{', '').Replace('}', '').Trim());
            //    string[] coords = line1.Split(',');
            //        Point point = new Point(int.Parse(coords[0]), int.Parse(coords[1]));

            //   // System.ComponentModel.TypeConverter converter =
            //    //   System.ComponentModel.TypeDescriptor.GetConverter(typeof(Point));
            // //   Point point1 = (Point)converter.ConvertFromString(coords);
            //    if (line2 == "Maximized")
            //    //  {
            //    {
            //        this.WindowState = FormWindowState.Maximized;
            //        this.Location = point;

            //    }
            //    else if (line2 == "Minimized")
            //    { 
            //        this.WindowState = FormWindowState.Minimized;
            //        this.Location = point;
            //    }
            //    else
            //    {

            //        this.WindowState = FormWindowState.Normal;
            //        this.Location = point;

            //        if (this.Location.X < 0 && this.Location.Y < 0)
            //        {
            //            this.Location = new Point(0, 0);
            //        }

            //        else
            //        {
            //            windowsSize();
            //            this.Location = new Point(43, 21);
            //            Properties.Settings.Default.WindowLocation = this.Location;
            //        }
            //        // string[] coords = line1.Split(',');
            //        // System.ComponentModel.TypeConverter converter =
            //        //System.ComponentModel.TypeDescriptor.GetConverter(typeof(Point));
            //        // Point point1 = (Point)converter.ConvertFromString(line1);
            //        //  Point point = new Point(int.Parse(coords[0]), int.Parse(coords[1]));
            //        //this.Location = point;
            //    }
            //}
            //else
            //{ 
            //    this.WindowState = FormWindowState.Normal;
            //   Properties.Settings.Default.WindowLocation = this.Location;
            //}//  this.Location = point;

            CheckSQLiteFileExistorNot();// Check for SQLite file
            m_oWorker = new BackgroundWorker();

            // Create a background worker thread 
            // Hook up the appropriate events.
            m_oWorker.DoWork += new DoWorkEventHandler(ImportScoresheetImagesintoFolder);
            m_oWorker.RunWorkerAsync();

            this.qRCODEToolStripMenuItem.Enabled = false;
            this.pORTSETTINGToolStripMenuItem.Enabled = false;
            GetDocument.OnNewDocument();
            GetDocument.SetMainScreen(this);
            this.eXPORTYGAFILEToolStripMenuItem.Enabled = false;
            if (GetDocument.GetLanguageMenu() == 1)
            {
                if (!File.Exists(Constants.programdata_path + "Languageconfig.txt"))
                {
                    File.Copy(@"Resources\Languageconfig.txt",
                            Constants.programdata_path + "Languageconfig.txt");
                }
            }


            WebComCation.LicenseValidator.sCurrent_Language = Yugamiru.Properties.Resources.CURRENT_LANGUAGE; //--Added by Rajnish For GSP-880 and GSP-881--//

            // no larger than screen size
            if (Properties.Settings.Default.WindowSize.Width == 300 && Properties.Settings.Default.WindowSize.Height == 300)
            {
                this.MaximumSize = new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);// (int)SystemParameter.PrimaryScreenHeight);
                this.Size = new Size(this.MaximumSize.Width, this.MaximumSize.Height);
            }
            else
            {
                this.Size = Properties.Settings.Default.WindowSize;
            }


            this.Location = Properties.Settings.Default.WindowLocation;
            if (this.Location.X < 0 && this.Location.Y < 0)
                this.Location = new Point(0, 0);

            menuStrip1.BackColor = Color.White;

            string host; // ƒ|[ƒg”Ô†‚ÍÅ‘å5Œ…+ƒRƒƒ“+‹ó”’•¶Žš.
            string hostName = Dns.GetHostName(); // Retrive the Name of HOST                                               
            string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();// Get the IP
            // theApp.GetMyIPAddress(host, sizeof(host));
            int szPort;
            //wsprintf(szPort, ":%d", theApp.GetServerPort());
            szPort = Constants.DEFAULT_PORT_NUMBER;
            //strcat(host, szPort.ToString);
            host = myIP + ":" + szPort.ToString();
            byte[] host_toBytes = Encoding.ASCII.GetBytes(host);
            /*   
             *  QRCode generation using QRCoder in C#*/
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeGenerator.QRCode qrCode = qrGenerator.CreateQrCode(host, QRCodeGenerator.ECCLevel.Q);

            Bitmap QRCodeBitmap1 = qrCode.GetGraphic(20);
            GetDocument.SetQRCodeImage(QRCodeBitmap1);

            panel2.Height = ClientSize.Height;
            panel2.Width = ClientSize.Width;
            panel2.AutoScroll = false;
            m_BALANCELABO = new IDD_BALANCELABO(GetDocument);
            m_BALANCELABO.Width = panel2.Width;
            m_BALANCELABO.Height = panel2.Height;
            m_BALANCELABO.TopLevel = false;
            //m_BALANCELABO.Location = new Point(0, 40);
            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_BALANCELABO);


            // to capture the event and to call back the function M_BALANCELABO_closeForm - step 4
            GetDocument.SetInitialScreen(m_BALANCELABO);
            m_BALANCELABO.closeForm += M_BALANCELABO_closeForm;
            m_BALANCELABO.OpenSettingScreen += M_BALANCELABO_OpenSettingScreen;
            //RefreshMenuStrip(true);

            menuStrip1.Items[1].Enabled = true;
            if (GetDocument.GetImportYGAFileMenuFlag() == 1) // Added for GSP-415
                this.iMPORTYGAFILEToolStripMenuItem.Enabled = true;
            else
                this.iMPORTYGAFILEToolStripMenuItem.Enabled = false;




            #endregion
            button1.TabStop = false;
            button1.FlatStyle = FlatStyle.Flat;
            button1.FlatAppearance.BorderSize = 0;
            button1.Visible = true;
            ToolTip tooltips = new ToolTip();
            tooltips.SetToolTip(button1, "Shopping cart");

             this.trialExpiryBox.Visible = false;
     
        }

       public void showStatus()
        {
            if(trialExpiryBox.Visible == false)
            trialExpiryBox.Visible = false;
           
            }


        
    
        private void Process_Exited(object sender, EventArgs e)
        {
            //throw new NotImplementedException();



            //GrantAccess(@"Resources\\Languageconfig.txt");

        }


        private void GrantAccess(string fullPath)
        {
            DirectoryInfo dInfo = new DirectoryInfo(fullPath);
            DirectorySecurity dSecurity = dInfo.GetAccessControl();
            dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
            dInfo.SetAccessControl(dSecurity);
        }
        void CheckSQLiteFileExistorNot()
        {
            //Enable CreateSQLiteFile functionality if you want to create new SQLite file for fresh installation -- by Meena
            //CreateSQLiteFile(Constants.default_database);
            CreateSQLiteFile(Constants.db_file);//--Added by Rajnish To handle GSP-893--//
            //MoveSQLiteFile(Constants.db_file);
        }
        void MoveSQLiteFile(string path)
        {
            if (!File.Exists(path))
            {
                File.Copy(Constants.default_database, Constants.db_file);
            }
        }
        public void CreateSQLiteFile(string path)
        {
            string Query = string.Empty;
            CDatabase DB;
            
            if (!File.Exists(path))
            {
                DB = new CDatabase(path);
                DB = new CDatabase();

                // create patientdetails table
                Query = "CREATE TABLE PatientDetails" +
                    "(UniqueId INTEGER PRIMARY KEY NOT NULL DEFAULT(null), " +
                    "PatientId VARCHAR DEFAULT(null), " +
                    "Name VARCHAR, Gender VARCHAR, Year VARCHAR DEFAULT(null), Month VARCHAR, " +
                    "Date VARCHAR, Height INTEGER, DOB varchar DEFAULT(null), MeasurementTime varchar DEFAULT(null), " +
                    "Comment VARCHAR, BenchmarkDistance INTEGER"+ 
                    ",lut varchar default(null)" + //Added by Sumit GSP-858 (Cloud Backup)
                    ")";
                DB.ExecuteQuery(Query);

                // Create FrontBodyPositionKneedown table
                Query = "CREATE TABLE FrontBodyPositionKneedown(UniqueId integer NOT NULL, " +
                "KneePositionDetected BOOL DEFAULT(null), " +
                "UnderBodyPositionDetected BOOL DEFAULT(null), " +
                "UpperBodyPositionDetected BOOL DEFAULT(null), " +
                "ChinX INTEGER, ChinY INTEGER, GlabellaX INTEGER, GlabellaY INTEGER, " +
                "LeftAnkleX INTEGER, LeftAnkleY INTEGER, LeftBeltX INTEGER, " +
                "LeftBeltY INTEGER, LeftEarX INTEGER, LeftEarY INTEGER, " +
                "LeftHipX INTEGER, LeftHipY INTEGER, LeftKneeX INTEGER, " +
                "LeftKneeY INTEGER, LeftShoulderX INTEGER, LeftShoulderY INTEGER, " +
                "RightAnkleX INTEGER, RightAnkleY INTEGER, RightBeltX INTEGER, " +
                "RightBeltY INTEGER, RightEarX INTEGER, RightEarY INTEGER, " +
                "RightHipX INTEGER DEFAULT(null), RightHipY INTEGER, RightKneeX INTEGER, " +
                "RightKneeY INTEGER, RightShoulderX INTEGER, RightShoulderY INTEGER, " +
                "ImageBytes VARCHAR, FOREIGN KEY(UniqueId) REFERENCES PatientDetails(UniqueId) )";

                DB.ExecuteQuery(Query);

                // Create  FrontBodyPositionStanding table
                Query = "CREATE TABLE FrontBodyPositionStanding(UniqueId integer NOT NULL, " +
                    "KneePositionDetected BOOL DEFAULT(null), " +
                    "UnderBodyPositionDetected BOOL DEFAULT(null), " +
                    "UpperBodyPositionDetected BOOL DEFAULT(null), " +
                    "ChinX INTEGER, ChinY INTEGER, GlabellaX INTEGER, " +
                    "GlabellaY INTEGER, LeftAnkleX INTEGER, LeftAnkleY INTEGER, " +
                    "LeftBeltX INTEGER, LeftBeltY INTEGER, LeftEarX INTEGER, " +
                    "LeftEarY INTEGER, LeftHipX INTEGER, LeftHipY INTEGER, " +
                    "LeftKneeX INTEGER, LeftKneeY INTEGER, LeftShoulderX INTEGER, " +
                    "LeftShoulderY INTEGER, RightAnkleX INTEGER, RightAnkleY INTEGER, " +
                    "RightBeltX INTEGER, RightBeltY INTEGER, RightEarX INTEGER, " +
                    "RightEarY INTEGER, RightHipX INTEGER DEFAULT(null), RightHipY INTEGER, " +
                    "RightKneeX INTEGER, RightKneeY INTEGER, RightShoulderX INTEGER, " +
                    "RightShoulderY INTEGER, ImageBytes VARCHAR, " +
                    "FOREIGN KEY(UniqueId) REFERENCES PatientDetails(UniqueId))";

                DB.ExecuteQuery(Query);

                //Create SideBodyPosition Table
                Query = "CREATE TABLE SideBodyPosition(UniqueId integer NOT NULL, " +
                   "AnkleX INTEGER, AnkleY INTEGER, AnkleLeftBeltX INTEGER, " +
                   "AnkleLeftBeltY INTEGER, AnkleRightBeltX INTEGER, " +
                   "AnkleRightBeltY INTEGER DEFAULT(null), BenchMark1X INTEGER, " +
                   "BenchMark1Y INTEGER, BenchMark2X INTEGER, BenchMark2Y INTEGER, " +
                   "ChinX INTEGER, ChinY INTEGER, EarX INTEGER, EarY INTEGER, " +
                   "GlabellaX INTEGER, GlabellaY INTEGER, HipX INTEGER, HipY INTEGER, " +
                   "KneeX INTEGER, KneeY INTEGER, KneeLeftBeltX INTEGER, " +
                   "KneeLeftBeltY INTEGER, KneeRightBeltX INTEGER, " +
                   "KneeRightBeltY INTEGER, LeftBeltX INTEGER, LeftBeltY INTEGER, " +
                   "RightBeltX INTEGER, RightBeltY INTEGER, ShoulderX INTEGER, " +
                   "ShoulderY INTEGER, ImageBytes VARCHAR, " +
                   "FOREIGN KEY(UniqueId) REFERENCES PatientDetails(UniqueId))";
                DB.ExecuteQuery(Query);

                Query = "CREATE TABLE [ScoreSheetImages_English] (" +
                "[ImageStore_Id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "[ImageFileName] NVARCHAR(20)  NULL, [ImageBlob] BLOB NULL)";
                DB.ExecuteQuery(Query);
                Query = "CREATE TABLE [ScoreSheetImages_Japanese] (" +
                "[ImageStore_Id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "[ImageFileName] NVARCHAR(20)  NULL, [ImageBlob] BLOB NULL)";
                DB.ExecuteQuery(Query);
               /* Query = "CREATE TABLE [ScoreSheetImages_Chinese] (" +
                "[ImageStore_Id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "[ImageFileName] NVARCHAR(20)  NULL, [ImageBlob] BLOB NULL)";
                DB.ExecuteQuery(Query);
                Query = "CREATE TABLE [ScoreSheetImages_Korean] (" +
                "[ImageStore_Id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "[ImageFileName] NVARCHAR(20)  NULL, [ImageBlob] BLOB NULL)";
                DB.ExecuteQuery(Query);
                Query = "CREATE TABLE [ScoreSheetImages_Thai] (" +
                "[ImageStore_Id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "[ImageFileName] NVARCHAR(20)  NULL, [ImageBlob] BLOB NULL)";
                DB.ExecuteQuery(Query);*/

                ImportScoreSheet ImportImages = new ImportScoreSheet();
                ImportImages.ImportScoresheetImagesintoDB();


            }
        }
        //used below functionality to open final screen from initial_screen.
        private void M_BALANCELABO_OpenSettingScreen(object sender, EventArgs e)
        {
            RefreshMenuStrip(false);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();
            GetDocument.CalcNewTrainingIDs();
            m_BALANCELABO.Visible = false;
            ResultView m_ResultView = new ResultView(GetDocument, m_BalanceLabData);

            m_ResultView.Width = panel2.Width;
            m_ResultView.Height = panel2.Height;
            //m_ResultView.AutoScroll = true;

            m_ResultView.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_ResultView);

            GetDocument.SetFinalScreenMode(Constants.FINAL_SCREEN_MODE_TRUE);
            m_ResultView.Show();


            m_ResultView.EventToEditID += M_ResultView_EventToEditID;
            m_ResultView.EventToChange += M_MeasurementDlg_EventToStartNextScreen;
            m_ResultView.EventToCheckPosition += M_MeasurementDlg_Closeformtostartnextscreen;
            m_ResultView.EventToInitialScreen += M_MeasurementDlg_closeForm;
            m_ResultView.EventToRestart += M_ResultView_EventToEditID;

        }

        private void SettingView_EventToGoInitialScreen(object sender, EventArgs e)
        {

            // panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();

            panel2.Controls.Clear();
            RefreshMenuStrip(true);
            IDD_BALANCELABO m_Balancelabo_Dialog = new IDD_BALANCELABO(GetDocument);
            m_Balancelabo_Dialog.Width = panel2.Width;
            m_Balancelabo_Dialog.Height = panel2.Height;
            //m_Balancelabo_Dialog.AutoScroll = true;

            m_Balancelabo_Dialog.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_Balancelabo_Dialog);
            panel2.Show();

            m_Balancelabo_Dialog.Show();
            m_Balancelabo_Dialog.closeForm += M_BALANCELABO_closeForm;
            m_Balancelabo_Dialog.OpenSettingScreen += M_BALANCELABO_OpenSettingScreen;
        }

        private void M_BALANCELABO_closeForm(object sender, EventArgs e)
        {
            RefreshMenuStrip(false);
            // Close the IDD Form
            //GetDocument.SetInputMode(Constants.INPUTMODE_NEW);
            //GetDocument.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_INITIALIZE);

            //DisposeForms();
            //IDD_MeasurementDlg m_MeasurementDlg = new IDD_MeasurementDlg(GetDocument);
            //m_MeasurementDlg.Width = panel2.Width;
            //m_MeasurementDlg.Height = panel2.Height;


            // m_MeasurementDlg.TopLevel = false;
            // panel2.BorderStyle = BorderStyle.FixedSingle;
            // panel2.Controls.Add(m_MeasurementDlg);
            //panel2.Show();

            //m_MeasurementDlg.Show();

            if (GetDocument.GetLanguage() != "English")
                m_MeasurementDlg.RefreshForm();
            //if(m_MeasurementDlg.Visible == false)
            m_MeasurementDlg.Visible = true;
            m_MeasurementDlg.closeForm += M_MeasurementDlg_closeForm;
            m_MeasurementDlg.EventToStartNextScreen += M_MeasurementDlg_EventToStartNextScreen;

        }

        private void M_MeasurementDlg_EventToStartNextScreen(object sender, EventArgs e)
        {
            RefreshMenuStrip(false);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            //  DisposeForms();
            /* IDD_MEASUREMENT_START_VIEW m_MEASUREMENT_START = new IDD_MEASUREMENT_START_VIEW(GetDocument);

             m_MEASUREMENT_START.Width = panel2.Width;
             m_MEASUREMENT_START.Height = panel2.Height;
             //m_MEASUREMENT_START.AutoScroll = true;

             m_MEASUREMENT_START.TopLevel = false;

             panel2.BorderStyle = BorderStyle.Fixed3D;
             panel2.Controls.Add(m_MEASUREMENT_START);
             panel2.Show();

             m_MEASUREMENT_START.Show();*/

            m_MEASUREMENT_START.Visible = true;

            m_MEASUREMENT_START.closeForm += M_BALANCELABO_closeForm;//M_MeasurementDlg_closeForm;
            m_MEASUREMENT_START.EventToStartNextScreen += M_MeasurementDlg_Closeformtostartnextscreen;
            m_MEASUREMENT_START.EventfromCrouchedViewtoResultView += M_SideJointEditView_EventToChangeResultView;

        }

        private void M_MeasurementDlg_closeForm(object sender, EventArgs e)
        {
            this.qRCODEToolStripMenuItem.Enabled = true;
            this.pORTSETTINGToolStripMenuItem.Enabled = true;
            GetDocument.SetInputMode(Constants.INPUTMODE_NEW);
            //GetDocument.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_INITIALIZE);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            /* DisposeForms();
             IDD_BALANCELABO m_BalanceLabo = new IDD_BALANCELABO(GetDocument);
             m_BalanceLabo.Width = panel2.Width;
             m_BalanceLabo.Height = panel2.Height;
             //m_BalanceLabo.AutoScroll = true;

             m_BalanceLabo.TopLevel = false;
             panel2.BorderStyle = BorderStyle.FixedSingle;
             panel2.Controls.Add(m_BalanceLabo);
             panel2.Show();


             m_BalanceLabo.Show();*/
            m_BALANCELABO.Visible = true;
            m_BALANCELABO.closeForm += M_BALANCELABO_closeForm;
            m_BALANCELABO.OpenSettingScreen += M_BALANCELABO_OpenSettingScreen;

        }



        private void M_MeasurementDlg_Closeformtostartnextscreen(object sender, EventArgs e)
        {
            RefreshMenuStrip(false);

            //   DisposeForms();

            /*  JointEditView m_JointEditView = new JointEditView(GetDocument);

              m_JointEditView.Width = panel2.Width;
              m_JointEditView.Height = panel2.Height;


              m_JointEditView.TopLevel = false;

              panel2.BorderStyle = BorderStyle.Fixed3D;
              panel2.Controls.Add(m_JointEditView);
              panel2.Show();

              m_JointEditView.Show();*/
            m_JointEditView.Visible = true;
            m_JointEditView.closeBeltAnkleForm += M_JointEditView_closeBeltAnkleForm;
            m_JointEditView.EventToStartSideJointEditView += M_JointEditView_EventToStartSideJointEditView;

        }

        private void M_JointEditView_EventToStartSideJointEditView(object sender, EventArgs e)
        {
            RefreshMenuStrip(false);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            //DisposeForms();

            /* SideJointEditView m_SideJointEditView = new SideJointEditView(GetDocument);

             m_SideJointEditView.Width = panel2.Width;
             m_SideJointEditView.Height = panel2.Height;
             //m_SideJointEditView.AutoScroll = true;

             m_SideJointEditView.TopLevel = false;

             panel2.BorderStyle = BorderStyle.Fixed3D;
             panel2.Controls.Add(m_SideJointEditView);
             panel2.Show();
             GetDocument.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_SIDE);
             m_SideJointEditView.Show();
             */
            m_SideJointEditView.Visible = true;
            m_SideJointEditView.EventToChangeResultView += M_SideJointEditView_EventToChangeResultView;
            m_SideJointEditView.GoBackToJointEditView += M_SideJointEditView_GoBackToJointEditView;

        }

        private void M_SideJointEditView_GoBackToJointEditView(object sender, EventArgs e)
        {
            RefreshMenuStrip(false);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();

            JointEditView m_JointEditView = new JointEditView(GetDocument);

            m_JointEditView.Width = panel2.Width;
            m_JointEditView.Height = panel2.Height;
            //m_JointEditView.AutoScroll = true;

            m_JointEditView.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_JointEditView);
            panel2.Show();

            m_JointEditView.Show();
            m_JointEditView.closeBeltAnkleForm += M_JointEditView_closeBeltAnkleForm;
            m_JointEditView.EventToStartSideJointEditView += M_JointEditView_EventToStartSideJointEditView;
        }


        private void M_SideJointEditView_EventToChangeResultView(object sender, EventArgs e)
        {
            RefreshMenuStrip(false);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();
            GetDocument.CalcNewTrainingIDs();
            ResultView m_ResultView = new ResultView(GetDocument, m_BalanceLabData);

            m_ResultView.Width = panel2.Width;
            m_ResultView.Height = panel2.Height;
            //m_ResultView.AutoScroll = true;

            m_ResultView.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_ResultView);

            GetDocument.SetFinalScreenMode(Constants.FINAL_SCREEN_MODE_TRUE);
            m_ResultView.Show();


            m_ResultView.EventToEditID += M_ResultView_EventToEditID;
            m_ResultView.EventToChange += M_MeasurementDlg_EventToStartNextScreen;
            m_ResultView.EventToCheckPosition += M_MeasurementDlg_Closeformtostartnextscreen;
            m_ResultView.EventToInitialScreen += M_MeasurementDlg_closeForm;
            m_ResultView.EventToRestart += M_ResultView_EventToEditID;

        }

        private void M_ResultView_EventToInitialScreen(object sender, EventArgs e)
        {
            /*this.qRCODEToolStripMenuItem.Enabled = true;
            this.pORTSETTINGToolStripMenuItem.Enabled = true;*/
            RefreshMenuStrip(true);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();

            IDD_BALANCELABO m_BalanceLabo = new IDD_BALANCELABO(GetDocument);

            m_BalanceLabo.Width = panel2.Width;
            m_BalanceLabo.Height = panel2.Height - 40;
            //m_BalanceLabo.AutoScroll = true;

            m_BalanceLabo.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_BalanceLabo);
            panel2.Show();

            m_BalanceLabo.Show();
            m_BalanceLabo.closeForm += M_BALANCELABO_closeForm;
            m_BalanceLabo.OpenSettingScreen += M_BALANCELABO_OpenSettingScreen;


        }

        private void M_ResultView_EventToEditID(object sender, EventArgs e)
        {
            RefreshMenuStrip(false);
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            DisposeForms();

            IDD_MeasurementDlg m_MeasurementView = new IDD_MeasurementDlg(GetDocument);

            m_MeasurementView.Width = panel2.Width;
            m_MeasurementView.Height = panel2.Height;
            //m_MeasurementView.AutoScroll = true;

            m_MeasurementView.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_MeasurementView);
            panel2.Show();

            m_MeasurementView.Show();
            m_MeasurementView.EventfromMeasurementViewtoResultView += M_SideJointEditView_EventToChangeResultView;
            m_MeasurementView.closeForm += M_BALANCELABO_closeForm;//M_MeasurementDlg_closeForm;
            m_MeasurementView.EventToStartNextScreen += M_MeasurementDlg_EventToStartNextScreen;
        }



        private void M_JointEditView_closeBeltAnkleForm(object sender, EventArgs e)
        {
            DisposeForms();
            //panel2.Controls.RemoveAt(0);
            //GetDocument.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN);
            IDD_MEASUREMENT_START_VIEW m_MEASUREMENT_START = new IDD_MEASUREMENT_START_VIEW(GetDocument);
            m_MEASUREMENT_START.Width = panel2.Width;
            m_MEASUREMENT_START.Height = panel2.Height;
            //m_MEASUREMENT_START.AutoScroll = true;

            m_MEASUREMENT_START.TopLevel = false;

            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_MEASUREMENT_START);
            panel2.Show();

            m_MEASUREMENT_START.Show();
            m_MEASUREMENT_START.closeForm += M_BALANCELABO_closeForm;//M_MeasurementDlg_closeForm;
            m_MEASUREMENT_START.EventToStartNextScreen += M_MeasurementDlg_Closeformtostartnextscreen;
        }

        private void IDC_MeasurementBtn_Click(object sender, EventArgs e)
        {
            /*  if (!(GetDocument()->ProRingCheck(FALSE)))
               {
                   return;
               }*/
            GetDocument.SetInputMode(Constants.INPUTMODE_NEW);
            GetDocument.ClearMakerTouchFlag();
            GetDocument.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_INITIALIZE);
            GetDocument.InitBodyBalance();
            GetDocument.ClearStandingImage();
            GetDocument.ClearKneedownImage();
            GetDocument.ClearSideImage();
            GetDocument.SetDataMeasurementTime("");
            GetDocument.SetSaveFilePath("");
            GetDocument.ChangeToMeasurementView();


            /*  panel2.Height = this.Size.Height - 100 ;
              panel2.Width = this.Size.Width - 30 ;

              IDD_MeasurementDlg m_MeasurementDlg = new IDD_MeasurementDlg();
              m_MeasurementDlg.Width = panel2.Width;
              m_MeasurementDlg.Height = panel2.Height;
              m_MeasurementDlg.AutoScroll = true;

              m_MeasurementDlg.TopLevel = false;
              //panel1.Controls.RemoveAt(0);
              panel2.BorderStyle = BorderStyle.FixedSingle;
              panel2.Controls.Add(m_MeasurementDlg);
              panel2.Show();

              m_MeasurementDlg.Show();*/
            //picturebox1.Dispose();
            //IDC_MeasurementBtn.Dispose();
            //IDC_AnalysisBtn.Dispose();
            //IDC_CloseBtn.Dispose();
            //IDC_SETTING_BTN.Dispose();

        }

        private void IDD_BALANCELABO_DIALOG_SizeChanged(object sender, EventArgs e)
        {

            var t = panel2.Controls;
            this.MinimumSize = new Size(984, 0); // Fix for JIRA # 138 ( to set minimum size of main form)


            foreach (var control in panel2.Controls)
            {
                var controlType = control.GetType();

                if (controlType.Name == "IDD_BALANCELABO")
                {
                    var tt = control as IDD_BALANCELABO;


                    tt.Width = panel2.Width;
                    tt.Height = panel2.Height;

                    tt.IDD_BALANCELABO_SizeChanged(sender, e);
                    tt.Refresh();
                }
                else if (controlType.Name == "IDD_MeasurementDlg")
                {
                    var tt = control as IDD_MeasurementDlg;
                    tt.Width = panel2.Width;
                    tt.Height = panel2.Height;
                    tt.IDD_MeasurementDlg_SizeChanged(sender, e);
                    //tt.Refresh();
                }
                else if (controlType.Name == "IDD_MEASUREMENT_START_VIEW")
                {
                    var tt = control as IDD_MEASUREMENT_START_VIEW;
                    tt.Width = panel2.Width;
                    tt.Height = panel2.Height;
                    tt.IDD_MEASUREMENT_START_VIEW_SizeChanged(sender, e);
                    //tt.Refresh();
                }
                else if (controlType.Name == "JointEditView")
                {
                    var tt = control as JointEditView;
                    tt.Width = panel2.Width;
                    tt.Height = panel2.Height;
                    tt.JointEditView_SizeChanged(sender, e);
                    //tt.Refresh();

                }
                else if (controlType.Name == "SideJointEditView")
                {
                    var tt = control as SideJointEditView;
                    tt.Width = panel2.Width;
                    tt.Height = panel2.Height;
                    tt.SideJointEditView_SizeChanged(sender, e);
                    //tt.Refresh();
                    //tt.Show();

                }
                else if (controlType.Name == "ResultView")
                {
                    var tt = control as ResultView;
                    tt.Width = panel2.Width;
                    tt.Height = panel2.Height;
                    tt.ResultView_SizeChanged(sender, e);

                }
                else if (controlType.Name == "SettingView")
                {
                    var tt = control as SettingView;
                    tt.Width = panel2.Width;
                    tt.Height = panel2.Height;
                    tt.SettingView_SizeChanged(sender, e);

                }
                else if (controlType.Name == "OpenResult")
                {
                    var tt = control as OpenResult;
                    tt.Width = panel2.Width;
                    tt.Height = panel2.Height;
                    tt.OpenResult_SizeChanged(sender, e);
                }

            }
            //if(control.GetType)


            // MessageBox.Show("hi");

            // MessageBox.Show("hello");
            //  struct CLayoutInfo;
            //		{
            //			uint m_uiID; // �R���g���[���̂h�c.
            //			int m_iLeft;
            //			int m_iTop;
            //			int m_iWidth;
            //			int m_iHeight;
            //		};

            //C++ TO C# CONVERTER NOTE: This static local variable declaration (not allowed in C#) has been moved just prior to the method:
            /*     aSymbolAnbdIDPair[0] = new CSymbolAndIDPair { iID = BODYPOSITIONTYPEID_STANDING, pchSymbol = "Standing" };
                 CLayoutInfo[] s_aLayoutInfo = new CLayoutInfo[100];

                 s_aLayoutInfo[0] = new CLayoutInfo
                 {
                     m_uiID = IDC_MeasurementBtn,

                 }
                   {
                              { 1, -237-91, 214, 112, 42},
                              { IDC_AnalysisBtn, -56-91, 214, 112, 42}, 
                              { IDC_CloseBtn, 126-91, 214, 112, 42}, 
                              { IDC_SETTING_BTN, 308-91, 214, 112, 42}, 
                              { IDC_STATIC, 0, 0, 0, 0}
                    };

                       Rectangle rcSelf;*/

            /* this.IDC_MeasurementBtn.Size = new Size(112, 42);
             this.IDC_MeasurementBtn.Location = new Point(400, 614);

             this.IDC_AnalysisBtn.Size = new Size(112,42);
             this.IDC_AnalysisBtn.Location = new Point(550,614);

             this.IDC_CloseBtn.Size = new Size(112, 42);
             this.IDC_CloseBtn.Location = new Point(700, 614);

             this.IDC_SETTING_BTN.Size = new Size(112, 42);
             this.IDC_SETTING_BTN.Location = new Point(850, 614);*/

            //--to centre the picture box while resizing the form
            //picturebox1.Left = (this.ClientSize.Width - picturebox1.Width) / 2;
            //picturebox1.Top = (this.ClientSize.Height - picturebox1.Height) / 2;
            //picturebox1.Top = 25;
            //--end




            //GetClientRect(rcSelf);

            //C++ TO C# CONVERTER TODO TASK: Pointer arithmetic is detected on this variable, so pointers on this variable are left unchanged:
            /*   CLayoutInfo pLayoutInfo = OnSize_s_aLayoutInfo[0];
               while (pLayoutInfo.m_uiID != IDC_STATIC)
               {
                   Rectangle rcControlInWindowCoordinate;
               CWnd pWnd = GetDlgItem(pLayoutInfo.m_uiID);
                   if (pWnd != null)
                   {
                       pWnd.MoveWindow(pLayoutInfo.m_iLeft + rcSelf.Width() / 2, 
                       pLayoutInfo.m_iTop + rcSelf.Height() / 2, 
                       pLayoutInfo.m_iWidth, pLayoutInfo.m_iHeight, 1);
                   }
           pLayoutInfo++;
               }
               }*/

            /*   IDC_MeasurementBtn.Left = this.Width / 2 - IDC_MeasurementBtn.Left;
               IDC_AnalysisBtn.Left = IDC_MeasurementBtn.Left + IDC_AnalysisBtn.Width + 90;
               IDC_CloseBtn.Left = IDC_AnalysisBtn.Left + IDC_CloseBtn.Width + 90; 
               IDC_SETTING_BTN.Left = IDC_CloseBtn.Left + IDC_SETTING_BTN.Width + 90;*/


            /*  IDC_MeasurementBtn.Size = new Size(112, 42);
               IDC_MeasurementBtn.Location = new Point(
                   this.ClientSize.Width / 2 - this.IDC_MeasurementBtn.Location.X  ,
                   this.ClientSize.Height / 2 - this.IDC_MeasurementBtn.Location.Y);*/


        }

        private void IDD_BALANCELABO_DIALOG_Paint(object sender, PaintEventArgs e)
        {
            // e.Graphics.DrawRectangle(new Pen(Color.Black, 3),
            //   this.Location.X + 10, this.Location.Y +10, this.Width - 50, this.Height - 50);
            //e.Graphics.DrawLine(new Pen(Color.Black, 2), 0, 24, this.Width, 24);
            // this.DisplayRectangle);
            // trialExpiryBox.Visible = false;
            // this.Update()


        }
        

        private void qRCODEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Image QRCode_Bitmap = ConvertToBitmap(m_pbyteQRCodeBitmapBits);
            if (qRCODEToolStripMenuItem.Checked)
            {
                GetDocument.SetQRCodeFlag(true);
            }
            else
            {
                GetDocument.SetQRCodeFlag(false);

            }
            //panel2.Controls.RemoveAt(0); //step 5 ( removing the controls from panel2)
            /*  DisposeForms();
                  IDD_BALANCELABO m_Balancelabo_Dialog = new IDD_BALANCELABO(GetDocument);

                  m_Balancelabo_Dialog.Width = panel2.Width;
                  m_Balancelabo_Dialog.Height = panel2.Height;
                 // m_Balancelabo_Dialog.AutoScroll = true;

                  m_Balancelabo_Dialog.TopLevel = false;

                  panel2.BorderStyle = BorderStyle.Fixed3D;
                  panel2.Controls.Add(m_Balancelabo_Dialog);
                  panel2.Show();

                  m_Balancelabo_Dialog.Show();
                  m_Balancelabo_Dialog.closeForm += M_BALANCELABO_closeForm;
                  m_Balancelabo_Dialog.OpenSettingScreen += M_BALANCELABO_OpenSettingScreen;
                  */
            GetDocument.GetInitialScreen().RefreshForms();



        }
        /// <summary>
        /// Converts to bitmap.
        /// </summary>
        /// <param name="imagesSource">The images source.</param>
        /// <returns>The bitmap.</returns>
        public Image ConvertToBitmap(byte[] imagesSource)
        {
            Image returnImage;
            //try
            {
                if (imagesSource == null)
                {
                    //MessageBox.Show("no imge");
                }
                MemoryStream ms = new MemoryStream(imagesSource, 0, imagesSource.Length);
                ms.Write(imagesSource, 0, imagesSource.Length);
                returnImage = Image.FromStream(ms, true);

            }
            //catch { }
            return returnImage;
        }

        private void qRCODEToolStripMenuItem_CheckStateChanged(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void panel2_SizeChanged(object sender, EventArgs e)
        {
            // MessageBox.Show("for test");
            //panel2.Refresh();
        }

        private void lANGUAGESETTINGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var Language_form = new LanguageSetting(GetDocument);
            Language_form.Show();
            /*
                        if (GetDocument.GetMeasurementViewMode() == Constants.MEASUREMENTVIEWMODE_INITIALIZE)
                            Language_form.EventToLangChange += M_BALANCELABO_closeForm;
                        else if (GetDocument.GetMeasurementStartViewMode() == Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN ||
                            GetDocument.GetMeasurementStartViewMode() == Constants.MEASUREMENTSTARTVIEWMODE_FRONT_STANDING ||
                            GetDocument.GetMeasurementStartViewMode() == Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING)
                            Language_form.EventToLangChange += M_MeasurementDlg_EventToStartNextScreen;
                        else if (GetDocument.GetJointEditViewMode() == Constants.JOINTEDITVIEWMODE_ANKLE_AND_HIP ||
                            GetDocument.GetJointEditViewMode() == Constants.JOINTEDITVIEWMODE_KNEE ||
                            GetDocument.GetJointEditViewMode() == Constants.JOINTEDITVIEWMODE_UPPERBODY)
                            Language_form.EventToLangChange += M_MeasurementDlg_Closeformtostartnextscreen;
                        else if (GetDocument.GetJointEditViewMode() == Constants.JOINTEDITVIEWMODE_SIDE)
                            Language_form.EventToLangChange += M_JointEditView_EventToStartSideJointEditView;
                        else if (GetDocument.GetFinalScreenMode() == Constants.FINAL_SCREEN_MODE_TRUE)
                            Language_form.EventToLangChange += M_SideJointEditView_EventToChangeResultView;
                        else if (GetDocument.GetSettingMode() == Constants.SETTING_SCREEN_MODE_TRUE)
                            Language_form.EventToLangChange += M_BALANCELABO_OpenSettingScreen;
                        else if (GetDocument.GetLanguage() != null)
                            Language_form.EventToLangChange += SettingView_EventToGoInitialScreen;     
                            */
            if (GetDocument.GetFinalScreenMode() == Constants.FINAL_SCREEN_MODE_TRUE)
                Language_form.EventToLangChange += M_SideJointEditView_EventToChangeResultView;


        }

        private void pORTSETTINGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PortSetting port = new PortSetting();
            port.Show();
        }

        private void versionInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IDD_ABOUTBOX version = new IDD_ABOUTBOX();
            version.Show();

        }
        protected override bool ProcessCmdKey(ref Message msg, System.Windows.Forms.Keys keyData)
        {


            if (keyData == System.Windows.Forms.Keys.A &&
                GetDocument.GetMeasurementDlg().Visible == false &&
                GetDocument.GetOpenResult().Visible == false &&
                Application.OpenForms.OfType<ResultView>().Count() == 0)
            {

                IDD_ABOUTBOX version = new IDD_ABOUTBOX();
                version.Show();

                //write code for your shortcut action
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
        public void RefreshMenuStrip(bool EnableFlag)
        {
            Text = Properties.Resources.YUGAMIRU_TITLE;
            menuStrip1.Items[0].Text = Yugamiru.Properties.Resources.KEYWORD_SETTING;
            menuStrip1.Items[1].Text = Yugamiru.Properties.Resources.KEYWORD_TOOLS;
            menuStrip1.Items[2].Text = Yugamiru.Properties.Resources.KEYWORD_HELP;
            menuStrip1.Items[2].ToolTipText = "Help(ALT + H)";

            this.qRCODEToolStripMenuItem.Text = Yugamiru.Properties.Resources.KEYWORD_QR;
            this.pORTSETTINGToolStripMenuItem.Text = Yugamiru.Properties.Resources.KEYWORD_PORT;
            this.versionInfoToolStripMenuItem.Text = Yugamiru.Properties.Resources.KEYWORD_VERSION;

            this.iMPORTYGAFILEToolStripMenuItem.Text = Yugamiru.Properties.Resources.KEYWORD_IMPORTYGA;
            this.eXPORTYGAFILEToolStripMenuItem.Text = Yugamiru.Properties.Resources.KEYWORD_EXPORTYGA;

            //this.qRCODEToolStripMenuItem.Enabled = EnableFlag;
            //this.pORTSETTINGToolStripMenuItem.Enabled = EnableFlag;

            // Beta Testing

            if (GetDocument.GetFinalScreenMode() == Constants.FINAL_SCREEN_MODE_TRUE)
            {
                menuStrip1.Items[1].Enabled = true;
                this.iMPORTYGAFILEToolStripMenuItem.Enabled = false;
                this.eXPORTYGAFILEToolStripMenuItem.Enabled = false;
            }
            else if (GetDocument.GetInitialScreen().Visible)
            {
                menuStrip1.Items[1].Enabled = true;
                if (GetDocument.GetImportYGAFileMenuFlag() == 1) // Added for GSP-415
                    this.iMPORTYGAFILEToolStripMenuItem.Enabled = true;
                else
                    this.iMPORTYGAFILEToolStripMenuItem.Enabled = false;
                this.eXPORTYGAFILEToolStripMenuItem.Enabled = false;

                //Added By suhana For GSP 868
                #region TrialBox,Shopping Cart
                string comp_key = "";
                string license_key = "";

                string computerID = WebComCation.keyRequest.GetComputerID();
                QlmLicenseLib.QlmLicense qls = new QlmLicenseLib.QlmLicense();
                qls.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                   , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                qls.PublicKey = ProductInfo.ProductEncryptionKey;
                qls.ReadKeys(ref license_key, ref comp_key);

                qls.ValidateLicenseEx(comp_key, keyRequest.GetComputerID());
                qls.GetStatus();
                qlsGetDate = qls.ExpiryDate;
                int dRenewDate = ValidateDates();
                int ReadRenewDates = dRenewDate;
                if (ReadRenewDates <= 30)
                {
                    if (qls.DaysLeft <= 15)
                    {
                        this.trialExpiryBox.Text = qls.DaysLeft.ToString() + " " + "days left";
                        this.trialExpiryBox.Visible = true;
                       // this.button1.Visible = true;
                    }
                    else
                    {
                        this.trialExpiryBox.Visible = false;
                       // this.button1.Visible = false;
                    }

                }
                else
                {
                    this.trialExpiryBox.Visible = false;
                  //  this.button1.Visible = false;

                }



                #endregion
                //Added By suhana For GSP 868
            }
            else
                menuStrip1.Items[1].Enabled = false;


            //Added By Sumit GSP-339, 346--------- START: ATTENTION required
            this.toolStripMenuItem_License.Text = Yugamiru.Properties.Resources.KEYWORD_LICENSE;
            this.toolStripMenuItem_View_License.Text = Yugamiru.Properties.Resources.KEYWORD_VIEW;
            this.toolStripMenuItem_Release_License.Text = Yugamiru.Properties.Resources.KEYWORD_RELEASE;
            //Added By Sumit GSP-339, 346--------- END

            if (GetDocument.GetLanguageMenu() == 1) // Added for GSP-416
            {
                this.lANGUAGESETTINGToolStripMenuItem.Enabled = true;

            }
            else
                this.lANGUAGESETTINGToolStripMenuItem.Enabled = false;

        }
        public int ValidateDates()
        {
            string renewDate = WebComCation.LicenseValidator.showRenewDate;
            if (renewDate != null)
            {
                DateTime ddRenew = DateTime.Parse(renewDate).Date;
                DateTime GetQLSExpiryDays = qlsGetDate;
                TimeSpan leftTotalDays = (GetQLSExpiryDays - ddRenew);
                int getD = (int)leftTotalDays.TotalDays;



                return getD;
            }
            else
            {

                string showActivationDate = Utility.GetSavedStamp();
                if (showActivationDate == "")
                {
                    return 0;
                }
                else
                {
                    DateTime ddRenew = DateTime.Parse(showActivationDate).Date;
                    DateTime GetQLSExpiryDays = qlsGetDate;
                    TimeSpan leftTotalDays = (GetQLSExpiryDays - ddRenew);
                    int getD = (int)leftTotalDays.TotalDays;

                    return getD;
                }
            }
        }
        public void DisposeForms()
        {
            List<Form> openForms = new List<Form>();

            foreach (Form f in Application.OpenForms)
                openForms.Add(f);

            foreach (Form f in openForms)
            {
                /*if (f.Name != "IDD_BALANCELABO_DIALOG" && f.Name != "LanguageSetting")
                {
                    f.Controls.Clear();
                    f.Close();
                    ///test commit
                }*/
                if (f.Name == "ResultView")
                {
                    f.Controls.Clear();
                    f.Close();
                }

            }





        }

        public void IDD_BALANCELABO_DIALOG_Load(object sender, EventArgs e)
        {

            // Added By Sumit on 30-Jan-18
            try
            {
                //Added by Sumit-------START 3
                Cursor.Current = Cursors.WaitCursor;
                //Added by Sumit-------END 3

                //Added by Sumit GSP-346----START
                //Added by Sumit Hide progress bar for some time (during license checking)---------START
                //process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                //Added by Sumit Hide progress bar for some time (during license checking)---------END

                ReCheck:
                LicenseStatus s = LicenseValidator.VerifyPreActivated();
                
                LicenseValidator.IsFirstTimeCheck = true;//--Added by Rajnish To Handle GSP-771--//
                if (s != LicenseStatus.Valid)
                {
                    //DialogResult deci= MessageBox.Show("Do you want to provide a valid license?", "gsport", MessageBoxButtons.YesNo);
                    LicenseValidator.IsKVFromLaunch = true;//--Added by Rajnish For GSP-782--//
                    //if (deci == DialogResult.Yes)
                    {
                        try
                        {
                            //Close ProcessBar if running
                            Process[] ps = Process.GetProcesses();
                            foreach (Process kp in ps)
                            {
                                if (kp.ProcessName.ToUpper().Contains("YUGAMIRUPGB"))
                                {
                                    kp.CloseMainWindow();
                                }
                            }
                            //


                            KeyValidator kv = new KeyValidator();
                            #region NotInUse
                            //Added by sumit load progress bar---------START
                            //try
                            //{
                            //    //object top = process.HasExited;
                            //    if (process.HasExited)
                            //    {
                            //        Environment.Exit(1);
                            //    }
                            //    else
                            //    {
                            //        process.CloseMainWindow();
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    MessageBox.Show("Error in Loader" + Environment.NewLine + ex.Message);
                            //}


                            //Added by sumit load progress bar---------END
                            #endregion
                            //Edited By Suhana for GSP-450
                            kv.label1.Text = Yugamiru.Properties.Resources.ENTER_KEY;
                            kv.label2.Text = Yugamiru.Properties.Resources.ACTIVATION_RESULT;
                            kv.btnClose.Text = Yugamiru.Properties.Resources.BUTTON_CLOSE;
                            kv.button1.Text = Yugamiru.Properties.Resources.BUTTON_ACTIVATE;
                            //Edited By Suhana for GSP-450
                            kv.ShowDialog();
                            goto ReCheck;
                        }
                        finally
                        {

                        }
                    }
                    //else
                    //  Environment.Exit(0);
                }
                //Added by Sumit GSP-821 on 25-Sep-2018-----START
                DateTemperingHandler.SavePresentTimeStamp();
                //Added by Sumit GSP-821 on 25-Sep-2018-----END


                #region Merging Cloud
                //Added by Sumit on 10-Oct-18 Merging Cloud GSP-858.=================START
                CloudManager.GlobalItems.SetValues(keyRequest.GetComputerID(), LicenseValidator.ActivationKey);
                //===========Added by sumit GSP-556 (cloud backup)
                try
                {
                    //take backup from sever if there is no data avilable in local machine.
                    if (CloudManager.AppDataReader.GetRecordCount() == 0)
                    {
                        CloudManager.RestoreFromServer.Start();
                    }
                }
                catch (Exception ex)
                {
                    //exception at taking cloud backup
                    MessageBox.Show(ex.Message);
                }
                //==========End GSP-556
                //Added by Sumit on 10-Oct-18 Merging Cloud GSP-858.=================END
                #endregion



            }
            catch (Exception ex)
            {
                //Added Sumit GSP-775 on 28-Aug-18 START
                WebComCation.FaultManager.LogIt(ex);
                //Added Sumit GSP-775 on 28-Aug-18 END
            }
            finally
            {
                //Added by Sumit---Restoring the progress back to visible
                process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            }
            #region LoadCode
            m_BALANCELABO.Visible = true;

            // Initialize Measurement dialog screen
            GetDocument.SetInputMode(Constants.INPUTMODE_NEW);
            GetDocument.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_INITIALIZE);
            m_MeasurementDlg = new IDD_MeasurementDlg(GetDocument);
            m_MeasurementDlg.Width = panel2.Width;
            m_MeasurementDlg.Height = panel2.Height;
            m_MeasurementDlg.TopLevel = false;
            panel2.BorderStyle = BorderStyle.FixedSingle;
            panel2.Controls.Add(m_MeasurementDlg);
            //m_MeasurementDlg.Show();
            m_MeasurementDlg.Visible = false;
            GetDocument.SetMeasurementDlg(m_MeasurementDlg); // se the object of measrement dialog
            m_MeasurementDlg.EventfromMeasurementViewtoResultView += M_SideJointEditView_EventToChangeResultView;


            //Initialize measurement start view with side View screen
            GetDocument.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING);
            m_MEASUREMENT_START = new IDD_MEASUREMENT_START_VIEW(GetDocument);
            m_MEASUREMENT_START.Width = panel2.Width;
            m_MEASUREMENT_START.Height = panel2.Height;
            m_MEASUREMENT_START.TopLevel = false;
            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_MEASUREMENT_START);
            //m_MEASUREMENT_START.Show();
            m_MEASUREMENT_START.Visible = false;
            m_MEASUREMENT_START.EventfromCrouchedViewtoResultView += M_SideJointEditView_EventToChangeResultView;
            GetDocument.SetMeasurementStart(m_MEASUREMENT_START);// set the object of Measurement View Dialog


            //Initialize Joint Edit view screen with BELT-Ankle mode
            GetDocument.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_ANKLE_AND_HIP);
            m_JointEditView = new JointEditView(GetDocument);
            m_JointEditView.Width = panel2.Width;
            m_JointEditView.Height = panel2.Height;
            m_JointEditView.TopLevel = false;
            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_JointEditView);
            //m_JointEditView.Show();
            m_JointEditView.Visible = false;
            GetDocument.SetJointEditView(m_JointEditView);

            //intialize Side Joint Edit View Screen
            m_SideJointEditView = new SideJointEditView(GetDocument);
            m_SideJointEditView.Width = panel2.Width;
            m_SideJointEditView.Height = panel2.Height;
            m_SideJointEditView.TopLevel = false;
            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_SideJointEditView);
            GetDocument.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_SIDE);
            //m_SideJointEditView.Show();
            m_SideJointEditView.Visible = false;
            m_SideJointEditView.EventToChangeResultView += M_SideJointEditView_EventToChangeResultView;

            GetDocument.SetSideJointEditView(m_SideJointEditView);

            //initialize setting view screen
            m_SettingView = new SettingView(GetDocument);
            m_SettingView.Width = panel2.Width;
            m_SettingView.Height = panel2.Height;
            m_SettingView.TopLevel = false;
            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_SettingView);
            //GetDocument.(Constants.JOINTEDITVIEWMODE_SIDE);
            //m_SideJointEditView.Show();
            m_SettingView.Visible = false;
            GetDocument.SetSettingView(m_SettingView);

            GetDocument.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_NONE);
            GetDocument.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_NONE);
            GetDocument.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_NONE);

            //initialize Open Result screen
            m_OpenResult = new OpenResult(GetDocument);


            m_OpenResult.Width = panel2.Width;
            m_OpenResult.Height = panel2.Height;
            m_OpenResult.TopLevel = false;
            panel2.BorderStyle = BorderStyle.Fixed3D;
            panel2.Controls.Add(m_OpenResult);

            m_OpenResult.Visible = false;
            m_OpenResult.EventToChangeResultView += M_SideJointEditView_EventToChangeResultView;
            GetDocument.SetOpenResult(m_OpenResult);
            #endregion

            //Added by sumit load progress bar---------START
            try
            {

                //if (process.HasExited)
                //{
                //    Environment.Exit(1);
                //}
                //else
                //{
                //    process.CloseMainWindow();
                //}
                Process[] allp = Process.GetProcesses();
                int proCnt = 0;
                foreach (Process p in allp)
                {
                    if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                    {
                        proCnt++;
                        p.CloseMainWindow();
                    }
                }
                if (proCnt == 0)
                {
                    Environment.Exit(1);
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in Loader" + Environment.NewLine + ex.Message);
            }


            //Added by sumit load progress bar---------END

            //Refresh Menu Added by sumit on 22_Mar_18---------
            RefreshMenuStrip(true);

        }

        private void IDD_BALANCELABO_DIALOG_Shown(object sender, EventArgs e)
        {



        }

        private void iMPORTYGAFILEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Constants.data_path;
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;
            openFileDialog.Title = "Import YGA file";
            openFileDialog.FileName = "*.yga";
            openFileDialog.Filter = "YGA Files|*.yga";

            // image filters
            //openFileDialog.Filter = "*.yga";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string File = openFileDialog.FileName;
                int iCurrentVersion = 2;
                LoadStatementData(File, ref iCurrentVersion);
                //MessageBox.Show(iCurrentVersion.ToString());
                if (iCurrentVersion == 1)
                {
                    // ‹Œƒo[ƒWƒ‡ƒ“‚Ìê‡‚ÍA•Gƒxƒ‹ƒg‚Ì—¼’[À•WA‘«Žñƒxƒ‹ƒg‚Ì—¼’[À•W‚ªÝ’è‚³‚ê‚Ä‚¢‚È‚¢‚Ì‚Å
                    // ‹­§“I‚ÉÝ’è‚·‚é.
                    SideBodyPosition SideBodyPosition = new SideBodyPosition();
                    GetDocument.m_BalanceLabData.GetSideBodyPosition(ref SideBodyPosition);

                    Point ptKnee = new Point();
                    SideBodyPosition.GetKneePosition(ref ptKnee);
                    SideBodyPosition.SetKneeRightBeltPosition(ptKnee);
                    SideBodyPosition.SetKneeLeftBeltPosition(ptKnee);
                    Point ptAnkle = new Point();
                    SideBodyPosition.GetAnklePosition(ref ptAnkle);
                    SideBodyPosition.SetAnkleRightBeltPosition(ptAnkle);
                    SideBodyPosition.SetAnkleLeftBeltPosition(ptAnkle);
                    GetDocument.m_BalanceLabData.SetSideBodyPosition(SideBodyPosition);
                }

                m_BALANCELABO.Visible = false;
                GetDocument.CalcNewTrainingIDs();

                ResultView m_ResultView = new ResultView(GetDocument, GetDocument.m_BalanceLabData);
                m_ResultView.ImportYGAFileFlag = true;

                m_ResultView.Width = panel2.Width;
                m_ResultView.Height = panel2.Height;
                //m_ResultView.AutoScroll = true;

                m_ResultView.TopLevel = false;

                panel2.BorderStyle = BorderStyle.Fixed3D;
                panel2.Controls.Add(m_ResultView);

                GetDocument.SetFinalScreenMode(Constants.FINAL_SCREEN_MODE_TRUE);
                RefreshMenuStrip(false);
                m_ResultView.Show();


                m_ResultView.EventToEditID += M_ResultView_EventToEditID;
                m_ResultView.EventToChange += M_MeasurementDlg_EventToStartNextScreen;
                m_ResultView.EventToCheckPosition += M_MeasurementDlg_Closeformtostartnextscreen;
                m_ResultView.EventToInitialScreen += M_MeasurementDlg_closeForm;
                m_ResultView.EventToRestart += M_ResultView_EventToEditID;


            }

        }
        public bool LoadStatementData(string pchFileName, ref int iCurrentVersion)
        {

            int iFileHeaderLength = 0;
            int i = 0;
            for (i = 0; i < Constants.ms_achFileHeader.Length; i++)
            {
                iFileHeaderLength++;
            }
            int iFileVersionLength = 0;
            for (i = 0; i < Constants.ms_achFileVersion.Length; i++)
            {
                iFileVersionLength++;
            }

            //FILE* fp = fopen(pchFileName, "rb");
            StreamReader fp = new StreamReader(pchFileName,
                Encoding.GetEncoding("iso-8859-1"));
            if (fp == null)
            {
                return false;
            }

            long lFileSize = new System.IO.FileInfo(pchFileName).Length;
            if (lFileSize < 0)
            {
                fp.Close();
                return false;
            }

            char[] pbyteIn = new char[lFileSize];
            if (pbyteIn == null)
            {
                if (fp != null)
                {

                    fp.Close();
                    fp = null;
                }
                return false;
            }
            if (fp.Read(pbyteIn, 0, (int)lFileSize) < lFileSize - 1)
            {
                fp.Close();
                //fp = null;
                return false;
            }

            /*  if (fp != null)
              {

                  fp.Close();
                  fp = null;
              }*/

            int iOffset = 0;
            for (i = 0; i < iFileHeaderLength; i++)
            {
                if (pbyteIn[iOffset] != Constants.ms_achFileHeader[i])
                {
                    // ƒtƒ@ƒCƒ‹ƒwƒbƒ_[ˆÙí.
                    if (pbyteIn != null)
                    {

                        pbyteIn = null;
                    }
                    return false;
                }
                iOffset++;
            }
            //int iCurrentVersion = 2;
            int iOffsetVersionHead = iOffset;
            for (i = 0; i < iFileVersionLength; i++)
            {
                if (pbyteIn[iOffset] != Constants.ms_achFileVersion[i])
                {
                    // ƒtƒ@ƒCƒ‹ƒo[ƒWƒ‡ƒ“ˆÙí.
                    iCurrentVersion = 0;
                    break;
                }
                iOffset++;
            }
            // ‹Œƒo[ƒWƒ‡ƒ“‚Ö‚ÌƒTƒ|[ƒg.
            if (iCurrentVersion == 0)
            {
                iOffset = iOffsetVersionHead;
                string s_achOldFileVersion = "1000";
                for (i = 0; i < iFileVersionLength; i++)
                {
                    if (pbyteIn[iOffset] != s_achOldFileVersion[i])
                    {
                        // ƒtƒ@ƒCƒ‹ƒo[ƒWƒ‡ƒ“ˆÙí.
                        if (pbyteIn != null)
                        {

                            pbyteIn = null;
                        }
                        return false;
                    }
                    iOffset++;
                }
                iCurrentVersion = 1;
            }
            while (iOffset < lFileSize)
            {

                int iStatementSize = 0;
                if (iOffset + 4 >= lFileSize)
                    return true;
                iOffset += ReadIntDataFromMemory(pbyteIn, ref iStatementSize, iOffset);
                if (iOffset + 1 >= lFileSize)
                    return true;
                int iOpeCode = 0;
                iOffset += ReadUnsignedCharDataFromMemory(pbyteIn, ref iOpeCode, iOffset);
                if (iOffset + 1 >= lFileSize)
                    return true;
                int iSymbolSize = 0;
                iOffset += ReadUnsignedCharDataFromMemory(pbyteIn, ref iSymbolSize, iOffset);

                string pchSymbol = new string(pbyteIn, iOffset, iSymbolSize);
                iOffset += iSymbolSize;
                if (iOpeCode == 1)
                {
                    // StringAssignmentStatement.
                    int iValueSize = 0;
                    if (iOffset + 2 >= lFileSize)
                        return true;
                    iOffset += ReadUnsignedShortDataFromMemory(pbyteIn, ref iValueSize, iOffset);
                    //const char* pchValue = ( const char* )(pbyteIn + iOffset);
                    string pchValue = new string(pbyteIn, iOffset, iValueSize);
                    iOffset += iValueSize;
                    ExecuteStringAssignmentStatement(pchSymbol, iSymbolSize, pchValue, iValueSize);
                }
                else if (iOpeCode == 2)
                {
                    // ShortAssignmentStatement.
                    int iValue = 0;
                    if (iOffset + 2 >= lFileSize)
                        return true;
                    iOffset += ReadShortDataFromMemory(pbyteIn, ref iValue, iOffset);
                    ExecuteIntegerAssignmentStatement(pchSymbol, iSymbolSize, iValue);
                }
                else if (iOpeCode == 3)
                {
                    // ImageAssignmentStatement.
                    int iImageSize = 0;
                    if (iOffset + 4 >= lFileSize)
                        return true;
                    iOffset += ReadIntDataFromMemory(pbyteIn, ref iImageSize, iOffset);


                    //int imageValueCount = iImageSize - iOffset;
                    byte[] pbyteImage = new byte[iImageSize];


                    for (i = 0; i < iImageSize; i++)
                    {
                        if (iOffset == pbyteIn.Length)
                        {
                            ExecuteImageAssignmentStatement(pchSymbol, iSymbolSize, pbyteImage, iImageSize);
                            return true;
                        }
                        pbyteImage[i] = Convert.ToByte(pbyteIn[iOffset]);
                        iOffset = iOffset + 1;
                    }

                    //iOffset += iImageSize;
                    /*---- for testing
                    System.IO.File.WriteAllText
                       (@"C: \Users\Meena\Desktop\WriteLines.txt", pbyteImage.ToString());

                    using (Stream file = File.OpenWrite(@"C: \Users\Meena\Desktop\WriteLines.txt"))
                    {
                        file.Write(pbyteImage, 0, pbyteImage.Length);
                    }****/
                    ExecuteImageAssignmentStatement(pchSymbol, iSymbolSize, pbyteImage, iImageSize);
                }
            }
            /*MessageBox.Show(iCurrentVersion.ToString());
            if (iCurrentVersion == 1)
            {
                // ‹Œƒo[ƒWƒ‡ƒ“‚Ìê‡‚ÍA•Gƒxƒ‹ƒg‚Ì—¼’[À•WA‘«Žñƒxƒ‹ƒg‚Ì—¼’[À•W‚ªÝ’è‚³‚ê‚Ä‚¢‚È‚¢‚Ì‚Å
                // ‹­§“I‚ÉÝ’è‚·‚é.
                SideBodyPosition SideBodyPosition = new SideBodyPosition();
                GetDocument.m_BalanceLabData.GetSideBodyPosition(ref SideBodyPosition);
                
                Point ptKnee = new Point();
                SideBodyPosition.GetKneePosition(ref ptKnee);
                SideBodyPosition.SetKneeRightBeltPosition(ptKnee);
                SideBodyPosition.SetKneeLeftBeltPosition(ptKnee);
                Point ptAnkle = new Point();
                SideBodyPosition.GetAnklePosition(ref ptAnkle);
                SideBodyPosition.SetAnkleRightBeltPosition(ptAnkle);
                SideBodyPosition.SetAnkleLeftBeltPosition(ptAnkle);
                GetDocument.m_BalanceLabData.SetSideBodyPosition(SideBodyPosition);
            }*/
            if (pbyteIn != null)
            {
                //delete[] pbyteIn;
                pbyteIn = null;
            }
            return true;
        }
        int ReadIntDataFromMemory(char[] pbyteIn, ref int iValue, int ioffset)
        {
            iValue = pbyteIn[0 + ioffset] & 0xFF;
            iValue |= ((pbyteIn[1 + ioffset] << 8) & 0x0000FF00);
            iValue |= ((pbyteIn[2 + ioffset] << 16) & 0x00FF0000);

            iValue |= (int)((pbyteIn[3 + ioffset] << 24) & 0xFF000000);
            return 4;
        }
        int ReadUnsignedCharDataFromMemory(char[] pbyteIn, ref int iValue, int ioffset)
        {

            iValue = pbyteIn[0 + ioffset] & 0xFF;
            return 1;
        }
        int ReadShortDataFromMemory(char[] pbyteIn, ref int iValue, int ioffset)
        {
            iValue = pbyteIn[0 + ioffset] & 0xFF;
            iValue |= ((pbyteIn[1 + ioffset] << 8) & 0x0000FF00);
            if ((iValue & 0x00001000) == 1)
            {
                iValue |= 0xFFFF;
            }
            return 2;
        }
        int ReadUnsignedShortDataFromMemory(char[] pbyteIn, ref int iValue, int ioffset)
        {

            iValue = pbyteIn[0 + ioffset] & 0xFF;
            iValue |= ((pbyteIn[1 + ioffset] << 8) & 0x0000FF00);
            return 2;
        }
        int ExecuteIntegerAssignmentStatement(string pchSymbolName, int iSymbolNameLength, int iValue)
        {
            if (GetDocument.m_BalanceLabData.ExecuteIntegerAssignmentStatement(pchSymbolName, iSymbolNameLength, iValue) == 1)
            {
                return 1;
            }
            if (m_BalanceLaboImg.ExecuteIntegerAssignmentStatement(pchSymbolName, iSymbolNameLength, iValue) == 1)
            {
                return 1;
            }
            return 0;
        }

        int ExecuteStringAssignmentStatement(string pchSymbolName, int iSymbolNameLength, string pchValue, int iValueLength)
        {
            if (GetDocument.m_BalanceLabData.ExecuteStringAssignmentStatement(pchSymbolName, iSymbolNameLength, pchValue, iValueLength) == 1)
            {
                return 1;
            }
            if (m_BalanceLaboImg.ExecuteStringAssignmentStatement(pchSymbolName, iSymbolNameLength, pchValue, iValueLength) == 1)
            {
                return 1;
            }
            return 0;
        }
        int ExecuteImageAssignmentStatement(string pchSymbolName,
            int iSymbolNameLength, byte[] pbyteImage, int iImageSize)
        {
            Image temp;
            if (pchSymbolName == "StandingImage")
            {

                MemoryStream ms = new MemoryStream(pbyteImage, 0, pbyteImage.Length);
                ms.Write(pbyteImage, 0, pbyteImage.Length);
                temp = Image.FromStream(ms, true);



                Image<Bgr, byte> Emgu_stand_image =
                    new Image<Bgr, byte>(SwapRedandBlueChannels(temp));
                Emgu_stand_image = Emgu_stand_image.Resize(1024, 1280,
                    Emgu.CV.CvEnum.Inter.Linear);


                GetDocument.AllocStandingImage(Emgu_stand_image.Bytes);
                return 1;
            }
            if (pchSymbolName == "KneedownImage")
            {
                MemoryStream ms = new MemoryStream(pbyteImage, 0, pbyteImage.Length);
                ms.Write(pbyteImage, 0, pbyteImage.Length);
                temp = Image.FromStream(ms, true);

                Image<Bgr, byte> Emgu_Kneedown_image =
                    new Image<Bgr, byte>(SwapRedandBlueChannels(temp));
                Emgu_Kneedown_image = Emgu_Kneedown_image.Resize(1024, 1280,
                    Emgu.CV.CvEnum.Inter.Linear);

                GetDocument.AllocKneedownImage(Emgu_Kneedown_image.Bytes);

                return 1;
            }
            if (pchSymbolName == "SideImage")
            {
                MemoryStream ms = new MemoryStream(pbyteImage, 0, pbyteImage.Length);
                ms.Write(pbyteImage, 0, pbyteImage.Length);
                temp = Image.FromStream(ms, true);

                Image<Bgr, byte> Emgu_Side_image =
                    new Image<Bgr, byte>(SwapRedandBlueChannels(temp));
                Emgu_Side_image = Emgu_Side_image.Resize(1024, 1280,
                    Emgu.CV.CvEnum.Inter.Linear);

                GetDocument.AllocSideImage(Emgu_Side_image.Bytes);
                return 1;
            }
            /* if (m_BalanceLaboImg.ExecuteImageAssignmentStatement(pchSymbolName, iSymbolNameLength, pbyteImage, iImageSize) == 1)
             {
                 return 1;
             }*/
            return 1;
        }
        private Bitmap SwapRedandBlueChannels(Image OriginalImage)
        {
            Bitmap bitmap = new Bitmap(OriginalImage);
            var imageAttr = new ImageAttributes();
            imageAttr.SetColorMatrix(new ColorMatrix(
                                         new[]
                                             {
                                                 new[] {0.0F, 0.0F, 1.0F, 0.0F, 0.0F},
                                                 new[] {0.0F, 1.0F, 0.0F, 0.0F, 0.0F},
                                                 new[] {1.0F, 0.0F, 0.0F, 0.0F, 0.0F},
                                                 new[] {0.0F, 0.0F, 0.0F, 1.0F, 0.0F},
                                                 new[] {0.0F, 0.0F, 0.0F, 0.0F, 1.0F}
                                             }
                                         ));
            var temp = new Bitmap(bitmap.Width, bitmap.Height);
            GraphicsUnit pixel = GraphicsUnit.Pixel;
            using (Graphics g = Graphics.FromImage(temp))
            {
                g.DrawImage(bitmap, Rectangle.Round(bitmap.GetBounds(ref pixel)),
                    0, 0, bitmap.Width, bitmap.Height,
                            GraphicsUnit.Pixel, imageAttr);
            }
            return temp;
        }

        private void eXPORTYGAFILEToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog SaveFileDialog1 = new SaveFileDialog();
            SaveFileDialog1.Title = "Save File";
            SaveFileDialog1.Filter = "YGA Files|*.yga";
            SaveFileDialog1.FileName = "Yugamiru" + DateTime.Now.ToString("hmmsstt") + ".yga";
            string strMeasurementTime = string.Empty;
            strMeasurementTime = GetDocument.GetDataMeasurementTime();
            // CTime timeCurrent = CTime::GetCurrentTime();

            if (strMeasurementTime == null)
            {
                GetDocument.SetDataMeasurementTime(DateTime.Now.ToString("yy-MM-dd HH:mm"));
            }
            //SaveFileDialog1.ShowDialog();
            string m_FileName = string.Empty;
            if (SaveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //m_FileName = SaveFileDialog1.FileName;
                /*   using (StreamWriter sw = new StreamWriter(SaveFileDialog1.FileName))
                       sw.WriteLine("Hello World!");*/
                SaveStatementData(SaveFileDialog1.FileName);
            }
        }
        bool SaveStatementData(string pchFileName)
        {
            int iFileHeaderLength = 0;
            int i = 0;
            for (i = 0; i < Constants.ms_achFileHeader.Length; i++)
            {
                iFileHeaderLength++;
            }
            int iFileVersionLength = 0;
            for (i = 0; i < Constants.ms_achFileVersion.Length; i++)
            {
                iFileVersionLength++;
            }
            int iStatementBlockSize = CalcDataSizeOfStatementBlock() + iFileHeaderLength + iFileVersionLength;
            char[] pbyteOut = new char[iStatementBlockSize];


            int iOffset = 0;
            for (i = 0; i < iFileHeaderLength; i++)
            {
                pbyteOut[iOffset++] = Constants.ms_achFileHeader[i];
            }
            for (i = 0; i < iFileVersionLength; i++)
            {
                pbyteOut[iOffset++] = Constants.ms_achFileVersion[i];
            }
            WriteStatementBlock(pbyteOut, iOffset, iStatementBlockSize);
            StreamWriter fp = new StreamWriter(pchFileName,
                true, Encoding.GetEncoding("iso-8859-1"));
            fp.WriteLine(pbyteOut);

            return true;
        }
        int WriteStatementBlock(char[] pbyteOut, int iOffset, int iSize)
        {
            if (iOffset < iSize)
                iOffset = GetDocument.m_BalanceLabData.WriteStatementBlock(pbyteOut, iOffset, iSize);

            if (iOffset < iSize)
                iOffset = GetDocument.m_BalanceLabData.WriteStatementBlock(pbyteOut, iOffset,
                    iSize, GetDocument);

            // ASSERT(iOffset<iSize );
            return iOffset;
        }

        public int CalcDataSizeOfStatementBlock()
        {

            int iRet = 0;
            iRet += m_BalanceLabData.CalcDataSizeOfStatementBlock();
            iRet += m_BalanceLabData.CalcDataSizeOfStatementBlock(GetDocument);
            return iRet;
        }

        private void IDD_BALANCELABO_DIALOG_MaximumSizeChanged(object sender, EventArgs e)
        {
            panel2.AutoScroll = true;
        }

        private void IDD_BALANCELABO_DIALOG_MinimumSizeChanged(object sender, EventArgs e)
        {
            panel2.AutoScroll = true;

        }

        private void IDD_BALANCELABO_DIALOG_Resize(object sender, EventArgs e)
        {
            
            if (WindowState == FormWindowState.Maximized)
            {
                panel2.AutoScroll = true;

            }
            if (WindowState == FormWindowState.Minimized)
            {
                panel2.AutoScroll = false;

            }

            if (WindowState == FormWindowState.Normal)
            {
                panel2.AutoScroll = false;

            }
            this.Height = Screen.PrimaryScreen.Bounds.Height;
        }

        private void IDD_BALANCELABO_DIALOG_KeyDown(object sender, KeyEventArgs e)
        {
            /*   var t = panel2.Controls;

               foreach (var control in panel2.Controls)
               {
                   var controlType = control.GetType();

                   if (controlType.Name == "IDD_BALANCELABO")
                   {
                       var tt = control as IDD_BALANCELABO;
                       //tt.Width = panel2.Width;
                       //tt.Height = panel2.Height;
                       tt.IDD_BALANCELABO_KeyDown(sender, e);
                       //tt.Refresh();
                   }


               }*/
        }


        /*  int ExecuteImageAssignmentStatement(string pchSymbolName, int iSymbolNameLength, string pbyteImage, int iImageSize)
          {
              if (m_BalanceLabData.ExecuteImageAssignmentStatement(pchSymbolName, iSymbolNameLength, pbyteImage, iImageSize) == 1)
              {
                  return 1;
              }
              if (m_BalanceLaboImg.ExecuteImageAssignmentStatement(pchSymbolName, iSymbolNameLength, pbyteImage, iImageSize))
              {
                  return 1;
              }
              return 0;
          }
          */

        //Added By Sumit GSP-365----------START
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == NativeMethods.WM_SHOWME)
            {
                ShowMe();
            }
            base.WndProc(ref m);
        }
        private void ShowMe()
        {
            if (WindowState == FormWindowState.Minimized)
            {
                WindowState = FormWindowState.Normal;
            }
            // hold our current "TopMost" value 
            bool top = this.TopMost;
            // make our form jump to the top of everything
            this.TopMost = true;
            // set it back to whatever it was
            this.TopMost = top;
        }


        //GSP-365------------END

        //event added by sumit GSP-346------START
        private void toolStripMenuItem_View_License_Click(object sender, EventArgs e)
        {

            //Attention Reuired By SUMIT GSP-346:    Update Relevant Resource Files for menu items


            WebComCation.frmViewLicense vl = new frmViewLicense();
            vl.label1.Text = Yugamiru.Properties.Resources.ACTIVATION_KEY;
            vl.label2.Text = Yugamiru.Properties.Resources.KEYWORD_LICENSE;
            vl.label5.Text = Yugamiru.Properties.Resources.EXPIRY_DATE;
            vl.label6.Text = Yugamiru.Properties.Resources.DAYS_LEFT;
            vl.txtExpiryDate.Text = Yugamiru.Properties.Resources.EXPIRY_DATE;
            vl.txtActivationDate.Text = Yugamiru.Properties.Resources.EXPIRY_DATE;
            vl.btnClose.Text = Yugamiru.Properties.Resources.BUTTON_CLOSE;
            vl.btnRefresh.Text = Yugamiru.Properties.Resources.BUTTON_REFRESH;
            vl.ShowDialog();
        }



        //event added by sumit GSP-346------START
        private void toolStripMenuItem_Release_License_Click(object sender, EventArgs e)
        {

            //Attention Reuired By SUMIT GSP-346:     Update Relevant Resource Files for menu items

            WebComCation.frmReleaseLicense release = new frmReleaseLicense();
            release.btnCancel.Text = Yugamiru.Properties.Resources.BUTTON_CANCEL;
            release.btnRelease.Text = Yugamiru.Properties.Resources.BUTTON_RELEASE;
            release.msgReleaseLicense = Yugamiru.Properties.Resources.LICENSE_RELEASED;
            release.msgReleaseNetProblem = Yugamiru.Properties.Resources.INTERNET_NOT;
            release.ShowDialog();
        }

        //event by sumit GSP-346------END

        //event by sumit GSP-346------END
        //public void windowsSize()
        //{
        //    Properties.Settings.Default.WindowSize = this.Size;
        //    Properties.Settings.Default.Save();
        //}
        private void IDD_BALANCELABO_DIALOG_FormClosing(object sender, FormClosingEventArgs e)
        {
            //RegistryKey key = Registry.CurrentUser;
            //key = key.CreateSubKey(Application.ProductName);
            //key.SetValue("W", this.Width);
            //key.SetValue("H", this.Height);
            //Added By Suhana For GSP 866
            //this.WindowState = this.WindowState;
            //Properties.Settings.Default.WindowSize = this.Size;
            ////Properties.Settings.Default.WindowLocation = this.Location;
            //location = this.Location;
            //windowsSize(ref Properties.Settings.Default.WindowSize);

            //System.IO.File.WriteAllText(Application.StartupPath + @"\\" + "WindowsState.txt", location.ToString() + Environment.NewLine + this.WindowState.ToString());
            //Added By Suhana For GSP 866

            Properties.Settings.Default.WindowLocation = this.Location;
            Properties.Settings.Default.WindowSize = this.Size;
            Properties.Settings.Default.Save();

            //DeleteScoresheetImagesInFolder();

        }
        void DeleteScoresheetImagesInFolder()
        {
            
            
            string Specific_Folder = Constants.path + Yugamiru.Properties.Resources.CURRENT_LANGUAGE;

            System.IO.DirectoryInfo di = new DirectoryInfo(Specific_Folder);
            try
            {
                
                Array.ForEach(Directory.GetFiles(Specific_Folder), File.Delete);
                if (!File.Exists(Specific_Folder + "temp.txt"))
                {
                    File.CreateText(Specific_Folder + "\\temp.txt").Dispose();
                }
            }
            catch (IOException ex)
            {
                //MessageBox.Show(ex.Message);
                System.Threading.Thread.Sleep(1000);
                Array.ForEach(Directory.GetFiles(Specific_Folder), File.Delete);
                if (!File.Exists(Specific_Folder + "temp.txt"))
                {
                    File.CreateText(Specific_Folder + "\\temp.txt").Dispose();
                }

                //file is currently locked
            }
        }

        private void IDD_BALANCELABO_DIALOG_FormClosed(object sender, FormClosedEventArgs e)
        {
            foreach (var control in panel2.Controls)
            {
                int count = panel2.Controls.Count;
                var controlType = control.GetType();

                if (controlType.Name == "IDD_BALANCELABO")
                {
                    var tt = control as IDD_BALANCELABO;

                    tt.Dispose();
                   
                    
                }
                else if (controlType.Name == "IDD_MeasurementDlg")
                {
                    var tt = control as IDD_MeasurementDlg;
                    tt.Dispose();
                    
                }
                else if (controlType.Name == "IDD_MEASUREMENT_START_VIEW")
                {
                    var tt = control as IDD_MEASUREMENT_START_VIEW;
                    tt.Dispose();
                }
                else if (controlType.Name == "JointEditView")
                {
                    var tt = control as JointEditView;
                    tt.Dispose();

                }
                else if (controlType.Name == "SideJointEditView")
                {
                    var tt = control as SideJointEditView;
                    tt.Dispose();

                }
                else if (controlType.Name == "ResultView")
                {
                    var tt = control as ResultView;
                    tt.ResultView_FormClosed(sender, e);
                    tt.Dispose();

                }
                else if (controlType.Name == "SettingView")
                {
                    var tt = control as SettingView;
                    tt.Dispose();

                }
                else if (controlType.Name == "OpenResult")
                {
                    var tt = control as OpenResult;
                    tt.Dispose();
                }

            }


            DeleteScoresheetImagesInFolder();

           // MessageBox.Show("hi");

        }
        //Added By Suhana For GSP 868
        private void trialExpiryBox_Click(object sender, EventArgs e)
        {
            GetDefaultBrowserPath();

            ProcessStartInfo sInfo = new ProcessStartInfo(@"https://yugamiru.net/");
            Process.Start(sInfo);
        }
        //Added By Suhana For GSP 858
        #region DefaultbrowserCode
        public static string GetDefaultBrowserPath()
        {
            string urlAssociation = @"Software\Microsoft\Windows\Shell\Associations\UrlAssociations\http";
            string browserPathKey = @"$BROWSER$\shell\open\command";

            Microsoft.Win32.RegistryKey userChoiceKey = null;
            string browserPath = "";

            try
            {
                //Read default browser path from userChoiceLKey
                userChoiceKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(urlAssociation + @"\UserChoice", false);

                //If user choice was not found, try machine default
                if (userChoiceKey == null)
                {
                    //Read default browser path from Win XP registry key
                    var browserKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(@"HTTP\shell\open\command", false);

                    //If browser path wasn’t found, try Win Vista (and newer) registry key
                    if (browserKey == null)
                    {
                        browserKey =
                        Microsoft.Win32.Registry.CurrentUser.OpenSubKey(
                        urlAssociation, false);
                    }
                    var path = CleanifyBrowserPath(browserKey.GetValue(null) as string);
                    browserKey.Close();
                    return path.ToString();
                }
                else
                {
                    // user defined browser choice was found
                    string progId = (userChoiceKey.GetValue("ProgId").ToString());
                    userChoiceKey.Close();

                    // now look up the path of the executable
                    string concreteBrowserKey = browserPathKey.Replace("$BROWSER$", progId);
                    var kp = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(concreteBrowserKey, false);
                    browserPath = CleanifyBrowserPath(kp.GetValue(null) as string).ToString();
                    kp.Close();
                    return browserPath;
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        //Added By Suhana For GSP 868
        private static object CleanifyBrowserPath(string v)
        {
            throw new NotImplementedException();
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            GetDefaultBrowserPath();

            ProcessStartInfo sInfo = new ProcessStartInfo(@"https://yugamiru.net");
            Process.Start(sInfo);
        }

        public void IDD_BALANCELABO_DIALOG_ParentChanged(object sender, EventArgs e)
        {
            this.trialExpiryBox.Visible = false;
            this.Update();
        }
    }


}

