﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace CloudManager
{
    public class GetBsonFromServer
    {
        //create BSON request.Replace # with actual UniqueId
        public static string initReqPatientJson = @"[{""TableName"":""" + Tables.PatientDetails.ToString() + @""",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""#""}]";
        public static string initReqKneeDownJson = @"[{""TableName"":""" + Tables.FrontBodyPositionKneeDown.ToString() + @""",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""#""}]";
        public static string initReqStandingJson = @"[{""TableName"":""" + Tables.FrontBodyPositionStanding.ToString() + @""",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""#""}]";
        public static string initReqsideJson = @"[{""TableName"":"""+Tables.SideBodyPosition.ToString()+@""",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""#""}]";

        static GetBsonFromServer()
        {
            initReqPatientJson = initReqPatientJson.Replace("ComputerIdhgjyfjhg", GlobalItems.ComputerID).Replace("activationKeykgjyftyfjvjhg", GlobalItems.ActivationKey);

            initReqKneeDownJson = initReqKneeDownJson.Replace("ComputerIdhgjyfjhg", GlobalItems.ComputerID).Replace("activationKeykgjyftyfjvjhg", GlobalItems.ActivationKey);
            initReqStandingJson = initReqStandingJson.Replace("ComputerIdhgjyfjhg", GlobalItems.ComputerID).Replace("activationKeykgjyftyfjvjhg", GlobalItems.ActivationKey);
            initReqsideJson = initReqsideJson.Replace("ComputerIdhgjyfjhg", GlobalItems.ComputerID).Replace("activationKeykgjyftyfjvjhg", GlobalItems.ActivationKey);
        }
        public static string getBsonFromServer(string jsonQuery, string uniqueID,string url)
        {
            var queryString = jsonQuery;
            queryString = queryString.Replace("#", uniqueID);
            #region sample JSON
            //var studentObject = queryString;//Newtonsoft.Json.JsonConvert.DeserializeObject(@"[{""TableName"":""PatientDetails"",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""1""}]");//queryString);
            #endregion
            //3.
            var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
            JsonSerializer jsonSerializer = new JsonSerializer();
            //4.
            MemoryStream objBsonMemoryStream = new MemoryStream();
            //5.
            Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
            //6.
            jsonSerializer.Serialize(bsonWriterObject, studentObject); //studentObject);
           
            byte[] requestByte = objBsonMemoryStream.ToArray();
            
            WebRequest webRequest = WebRequest.Create(url);//@"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/downloadpatient_data");//CloudUrls.FrontBodyPositionKneeDownSendToUrl);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            webRequest.ContentLength = requestByte.Length;

            // create our stram to send
            Stream webDataStream = null;
            try
            {
                webDataStream = webRequest.GetRequestStream();
                webDataStream.Write(requestByte, 0, requestByte.Length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            string ed = webDataStream.ToString();

            // get the response from our stream

            WebResponse webResponse = webRequest.GetResponse();
            
            string tp = webResponse.ContentType;
            try
            {
                webDataStream = webResponse.GetResponseStream();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // convert the result into a String
            StreamReader webResponseSReader = new StreamReader(webDataStream);
            String responseFromServer = webResponseSReader.ReadToEnd();
            //=======================================================================
            //string datafromAdars = File.ReadAllText(@"C:\Users\JMA_Sumit\Downloads\response1");
            //byte[] data = Convert.FromBase64String(datafromAdars);//(webResponseSReader.ReadToEnd());// "MQAAAAJOYW1lAA8AAABNb3ZpZSBQcmVtaWVyZQAJU3RhcnREYXRlAMDgKWE8AQAAAA==");





            //MemoryStream ms = new MemoryStream(data);
            //using (BsonReader reader = new BsonReader(ms))
            //{
            //    JsonSerializer serializer = new JsonSerializer();

            //    //Event e = serializer.Deserialize<Event>(reader);
            //    var e = serializer.Deserialize(reader);

            //    //Console.WriteLine(e.Name);
            //    // Movie Premiere
            //}
            //========================================================================

            return responseFromServer;
        }



    }
}
