﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace CloudManager
{
    /// <summary>
    /// This will initiate the datarestoration in local system. This will be called very rarely even in most cases never.
    /// </summary>
    public static class RestoreFromServer
    {
        public static void Start()
        {
            bool insertStatus = false;
            //start infinite loop for receiving unknown amount of data. and break it once done.
            for (int uniqueId=1; true; uniqueId++)
            {
                ////===First table patientdetails.
                string bsonPatient = GetBsonFromServer.getBsonFromServer(GetBsonFromServer.initReqPatientJson, uniqueId.ToString(), CloudUrls.PatientDetailsGetFromUrl);
                if(bsonPatient.Contains("Data Doesnot exist"))
                {
                    break;
                }

                if (bsonPatient.Length > 0)//--Added by Rajnish for GSP-676--//
                {
                    DataTable dtPat = BsonParser.SpliterPatientDetails(bsonPatient);//PatientDetailsBSONtoDT(bsonPatient);

                    //Added by Sumit on 06-Dec-2018 GSP-944---------------START
                    try
                    {
                        foreach (DataRow dr in dtPat.Rows)
                        {
                            //PatientId column
                            dr["PatientId"] = SplCharHandler.GetNormalString(dr["PatientId"].ToString());
                            if (dr["PatientId"].ToString().Contains("'"))
                            {
                                dr["PatientId"] = dr["PatientId"].ToString().Replace("'", @"''");
                            }
                            //Name column
                            dr["Name"] = SplCharHandler.GetNormalString(dr["Name"].ToString());
                            if (dr["Name"].ToString().Contains("'"))
                            {
                                dr["Name"] = dr["Name"].ToString().Replace("'", @"''");
                            }
                            //Comment Column
                            dr["Comment"] = SplCharHandler.GetNormalString(dr["Comment"].ToString());
                            if (dr["Comment"].ToString().Contains("'"))
                            {
                                dr["Comment"] = dr["Comment"].ToString().Replace("'", @"''");
                            }

                        }
                    }
                    catch (Exception e1)
                    { }
                    //Added by Sumit on 06-Dec-2018 GSP-944---------------END




                    string pdqr = DBHandler.QryConverter(dtPat, Tables.PatientDetails.ToString());
                    ////===insert into database 
                    insertStatus = DBHandler.ExecQry(pdqr, uniqueId.ToString(), Tables.PatientDetails);

                    try
                    {
                        // if(insertStatus)
                        {
                            string bsonkneedown = GetBsonFromServer.getBsonFromServer(GetBsonFromServer.initReqKneeDownJson, uniqueId.ToString(), CloudUrls.FrontBodyPositionKneeDownGetFromUrl);
                            DataTable dtknee = BsonParser.SpliterFrontBodyPositionKneedown(bsonkneedown);
                            ////===convert table to qry
                            string q = DBHandler.QryConverter(dtknee, Tables.FrontBodyPositionKneeDown.ToString());
                            ////===insert into database 
                            insertStatus = DBHandler.ExecQry(q, uniqueId.ToString(), Tables.FrontBodyPositionKneeDown);


                            string bsonstanding = GetBsonFromServer.getBsonFromServer(GetBsonFromServer.initReqStandingJson, uniqueId.ToString(), CloudUrls.FrontBodyPositionStandingGetFromUrl);
                            //DataTable dtStanding = BsonParser.ColumnNamesFrontBodyPositionStanding(bsonstanding);
                            DataTable dtStanding = BsonParser.SpliterFrontBodyPositionStanding(bsonstanding);
                            string r = DBHandler.QryConverter(dtStanding, Tables.FrontBodyPositionStanding.ToString());
                            //insert into database 
                            //**************
                            insertStatus = DBHandler.ExecQry(r, uniqueId.ToString(), Tables.FrontBodyPositionStanding);//**************


                            string bsonSideBody = GetBsonFromServer.getBsonFromServer(GetBsonFromServer.initReqsideJson, uniqueId.ToString(), CloudUrls.SideBodyPositionGetFromUrl);
                            //DataTable dtSideBody = BsonParser.PatientDetailsBSONtoDT(bsonSideBody);
                            DataTable dtSideBody = BsonParser.SpliterSideBodyPosition(bsonSideBody);
                            string s = DBHandler.QryConverter(dtSideBody, Tables.SideBodyPosition.ToString());
                            //insert into database 
                            insertStatus = DBHandler.ExecQry(s, uniqueId.ToString(), Tables.SideBodyPosition);

                        }
                    }
                    catch(Exception e2)
                    { }
                }
                //else
                //{
                //    throw new Exception("sqlite db insert/update error");
                //}
                //Save 
            }
        }


    }
}
